#include "Background.h"
#include "Rendering/Rendering.h"
#include "Media/MediaManager.h"

bool Background::Initialize(int slices, float radius)
{
    // Load background shader.
    NGE::Media::MediaManager::GetInstance().GetShaderManager().LoadProgram("backgroundShader", "background.xml");

    // Initialize the sphere.
    NGE::Geometry::Basic::Sphere::Initialize(slices, radius, NGE::Media::MediaManager::GetInstance().GetShaderManager().GetProgram("backgroundShader"));

    // Create the background cubemap from XML description file.
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file("config/backgroundCubemap.xml");
    pugi::xml_node cubeMap = doc.child("TextureCubeMap");
    NGE::Media::MediaManager::GetInstance().GetTextureManager().LoadTexture(cubeMap);

    this->texture = NGE::Media::MediaManager::GetInstance().GetTextureManager().GetTexture("backgroundCubemap");

    // Bind cubemap to the shader.
    shader->BindShader();
    {
        texture->Activate(0);
        shader->SendUniform("u_cubemap", 0);
    }
    shader->UnbindShader();

    return true;
}

void Background::Render()
{
    // Clear color and depth buffers.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Set visibility to the back of the sphere. As we use sphere as a skydome,
    // we want to see the back of it.
    glFrontFace(GL_CW);

    shader->BindShader();
    {
        // Send modelview and projection matrix to the shader.
        shader->SendUniform4x4("modelviewMatrix", NGE::Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(NGE::MODELVIEW_MATRIX));
        shader->SendUniform4x4("projectionMatrix", NGE::Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(NGE::PROJECTION_MATRIX));

        //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        
        // Render the sphere.
        glBindVertexArray(vertexArray);
        {
            glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
        }
        glBindVertexArray(0);
        
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
    shader->UnbindShader();
}


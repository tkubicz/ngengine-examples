#include "Examples/FrameBufferExample.h"
#include "Media/MediaManager.h"

using namespace NGE::Examples;

FrameBufferExample::FrameBufferExample() 
{
    modelAngle = 0.0f;
    boxAngle = 0.0f;
}

FrameBufferExample::~FrameBufferExample() { }

bool FrameBufferExample::Init()
{
    Media::MediaManager::GetInstance().GetShaderManager().LoadProgram("floorShader", "floorShader.xml");
    Media::MediaManager::GetInstance().GetShaderManager().LoadProgram("obj_shader", "obj.xml");
    Media::MediaManager::GetInstance().GetShaderManager().LoadProgram("colorShader", "colorShader.xml");

    camera.Set(-35.0f, 10.0f, 0.0f, 50.0f, 10.0f, 22.0f, 0.0f, 1.0f, 0.0f);
    //camera.Set(0.0f, 0.0f, 10.0f, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    floor.Initialize(50.f, 4.f, Media::MediaManager::GetInstance().GetShaderManager().GetProgram("floorShader"));

    box.Initialize(10.0f);
    box.SetShader(Media::MediaManager::GetInstance().GetShaderManager().GetProgram("colorShader"));

    modelShader = Media::MediaManager::GetInstance().GetShaderManager().GetProgram("obj_shader");
    model.Initialize("data/models/cow2.obj");
    model.LoadObjects();

    fbo = PrepareFBO(1024, 1024, 1);
    if (fbo == 0)
    {
        log_error("Error in FBO creation");
        return false;
    }

    modelShader->BindShader();
    modelShader->SendUniform("u_color", Math::vec4f(0.0f, 0.0f, 1.0f, 1.0f));
    modelShader->UnbindShader();

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    BuildVBO();

    return true;
}

void FrameBufferExample::Prepare(float dt)
{
    camera.Update(dt);
    
    modelAngle += 45 * dt;
    boxAngle += 30 * dt;
    
    if (modelAngle > 360.0f)
        modelAngle -= 360.0f;
    if (boxAngle > 360.0f)
        boxAngle -= 360.0f;
}

void FrameBufferExample::Render()
{
    // Bind framebuffer object
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);

    glClearColor(0.4f, 0.4f, 0.8f, 1.0f);

    // Set Drawing buffers
    GLuint attachments[1] = {GL_COLOR_ATTACHMENT0};
    glDrawBuffers(1, attachments);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glViewport(0, 0, 1024, 1024);

    Math::mat4f projection;
    projection.SetPerspectiveProjection(52.0f, 1024.0f / 1024.0f, 0.1f, 2000.0f);

    matrixStack.SetMatrixMode(PROJECTION_MATRIX);
    matrixStack.Identity();
    matrixStack.Multiple(projection);

    Math::mat4f lookAt;
    lookAt.LookAt(0.0f, 0.0f, 15.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

    matrixStack.SetMatrixMode(MODELVIEW_MATRIX);
    matrixStack.Identity();
    matrixStack.Multiple(lookAt);

    matrixStack.Rotate(modelAngle, 0.0f, 1.0f, 0.0f);
    
    modelShader->BindShader();
    modelShader->SendUniform4x4("u_modelviewMatrix", matrixStack.GetMatrix(MODELVIEW_MATRIX));
    modelShader->SendUniform4x4("u_projectionMatrix", matrixStack.GetMatrix(PROJECTION_MATRIX));
    
    glBindVertexArray(modelVao);
    glDrawArrays(GL_TRIANGLES, 0, vertexSize);
    glBindVertexArray(0);

    modelShader->UnbindShader();

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

    Application::OnResize(window->GetWidth(), window->GetHeight());
    glClearColor(0.9f, 0.95f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    Rendering::Renderer::GetInstance().GetMatrixStack().Identity();

    camera.Look();
    floor.Render();

    matrixStack.Translate(0, 5, 0);
    //matrixStack.Rotate(boxAngle, 0.0f, 1.0f, 0.0f);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texFBO[0]);
    box.Render();
}

void FrameBufferExample::OnKeyPressed(NGE::Events::KeyboardEvent& event)
{
    camera.SetKeyboardInput(event);

    if (event.GetAction() == Events::PRESSED)
    {
        if (event.GetKeyId() == Events::KEY_Z)
        {
            if (camera.IsMouseLocked())
            {
                window->EnableMouseCursor(true);
            }
            else
            {
                window->EnableMouseCursor(false);
            }

            camera.LockMouse(!camera.IsMouseLocked());
        }
    }
}

void FrameBufferExample::OnMouse(NGE::Events::MouseEvent& event)
{
    camera.SetMouseInfo(event.GetX(), event.GetY());
}

void FrameBufferExample::Shutdown() { }

void FrameBufferExample::BuildVBO()
{
    std::vector<Math::vec3f> vertices;
    std::vector<Math::vec3f> normals;

    std::map<std::string, Geometry::Mesh*> meshObjects = model.GetObjects();
    unsigned int size = 0;

    for (std::map<std::string, Geometry::Mesh*>::iterator it = meshObjects.begin(); it != meshObjects.end(); ++it)
        size += it->second->GetVertices().size();

    vertices.reserve(size);
    normals.reserve(size);

    for (std::map<std::string, Geometry::Mesh*>::iterator it = meshObjects.begin(); it != meshObjects.end(); ++it)
    {
        vertices.insert(vertices.end(), it->second->GetVertices().begin(), it->second->GetVertices().end());
        normals.insert(normals.end(), it->second->GetNormals().begin(), it->second->GetNormals().end());
    }

    GLuint vboVertex, vboNormal;

    glGenBuffers(1, &vboVertex);
    glBindBuffer(GL_ARRAY_BUFFER, vboVertex);
    glBufferData(GL_ARRAY_BUFFER, sizeof (GLfloat) * 3 * vertices.size(), &vertices[0], GL_STATIC_DRAW);

    glGenBuffers(1, &vboNormal);
    glBindBuffer(GL_ARRAY_BUFFER, vboNormal);
    glBufferData(GL_ARRAY_BUFFER, sizeof (GLfloat) * 3 * normals.size(), &normals[0], GL_STATIC_DRAW);


    glGenVertexArrays(1, &modelVao);
    glBindVertexArray(modelVao);

    glBindBuffer(GL_ARRAY_BUFFER, vboVertex);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, vboNormal);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);

    glBindVertexArray(0);

    vertexSize = vertices.size();
}

GLuint FrameBufferExample::CreateRGBATexture(int w, int h)
{
    GLuint tex;
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

    glBindTexture(GL_TEXTURE_2D, 0);
    return tex;
}

GLuint FrameBufferExample::CreateDepthTexture(int w, int h)
{
    GLuint tex;
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, w, h, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

    glBindTexture(GL_TEXTURE_2D, 0);
    return tex;
}

GLuint FrameBufferExample::PrepareFBO(int w, int h, int colorCount)
{
    GLuint fbo;

    // Generate one framebuffer
    glGenFramebuffers(1, &fbo);

    // Bind it
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);

    // Attach textures for colors
    for (int i = 0; i < colorCount; ++i)
    {
        texFBO[i] = CreateRGBATexture(w, h);
        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, texFBO[i], 0);
    }

    // Attach depth texture
    GLuint depthFBO;
    depthFBO = CreateDepthTexture(w, h);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthFBO, 0);

    // Check if everything is ok
    GLenum e = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
    switch (e)
    {
        case GL_FRAMEBUFFER_UNDEFINED:
            log_error("FBO Undefined");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
            log_error("FBO Incomplete Attachment");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
            log_error("FBO Missing Attachment");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
            log_error("FBO Incomplete Draw Buffer");
            break;
        case GL_FRAMEBUFFER_UNSUPPORTED:
            log_error("FBO Unsupported");
            break;
        case GL_FRAMEBUFFER_COMPLETE:
            log_info("FBO OK");
            break;
        default:
            log_error("FBO Problem?");
    }

    if (e != GL_FRAMEBUFFER_COMPLETE)
        return 0;

    // Unbind FBO
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

    return fbo;
}
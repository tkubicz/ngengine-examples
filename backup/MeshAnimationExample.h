/* 
 * File:   MeshAnimationExample.h
 * Author: tku
 *
 * Created on 18 marzec 2014, 16:14
 */

#ifndef MESHANIMATIONEXAMPLE_H
#define	MESHANIMATIONEXAMPLE_H

#include "Windows/Application.h"
#include "Geometry/Basic/Floor.h"
#include "Rendering/Camera/Camera.h"
#include "RagdollExample.h"
#include "Appearance/Scene/Scene.h"
#include "Appearance/Scene/NodeAnim.h"

namespace NGE
{
    namespace Examples
    {

        /*
        class Mesh
        {
          public:
            Mesh();
            ~Mesh();

            bool LoadMesh(const std::string& filename);
            void Render();

            unsigned int NumBones() const
            {
                return numBones;
            }

            void BoneTransform(float timeInSeconds, std::vector<Math::mat4f>& transforms);

          private:
#define NUM_BONES_PER_VERTEX 4

            struct BoneInfo
            {
                Math::mat4f boneOffset;
                Math::mat4f finalTransformation;

                BoneInfo() {
                    // SetZero to matrices
                }
            };

            struct VertexBoneData
            {
                unsigned int IDs[NUM_BONES_PER_VERTEX];
                float Weights[NUM_BONES_PER_VERTEX];

                VertexBoneData()
                {
                    Reset();
                }

                void Reset() {
                    // TODO: ZERO_MEM
                }

                void AddBoneData(unsigned int boneID, float weights);
            };

            void CalcInterpolatedScaling(Math::vec3f& out, float animationTime, const Appearance::Scene::NodeAnim* nodeAnim);
            void CalcInterpolatedRotation(Math::quaternionf& out, float animationTime, const Appearance::Scene::NodeAnim* nodeAnim);
            void CalcInterpolatedPosition(Math::vec3f& out, float animationTime, const Appearance::Scene::NodeAnim* nodeAnim);

            unsigned int FindScaling(float animationTime, const Appearance::Scene::NodeAnim* nodeAnim);
            unsigned int FindRotation(float animationTime, const Appearance::Scene::NodeAnim* nodeAnim);
            unsigned int FindPosition(float animationTime, const Appearance::Scene::NodeAnim* nodeAnim);

            const Appearance::Scene::NodeAnim* FindNodeAnim(const Appearance::Scene::Animation* animation, const std::string nodeName);
            void RenderNodeHierarchy(float animationTime, const Appearance::Scene::NodeAnim* node, const Math::mat4f& parentTransform);
            bool InitFromScene(const Appearance::Scene::Scene* scene, const std::string& filename);
            void InitFromMesh(unsigned int meshIndex, const Geometry::Mesh* mesh, std::vector<Math::vec3f>& positions,
                              std::vector<Math::vec3f>& normals, std::vector<Math::vec2f>& texCoords, std::vector<VertexBoneData>& bones,
                              std::vector<unsigned int>& indices);
            void LoadBones(unsigned int meshIndex, const Geometry::Mesh* mesh, std::vector<VertexBoneData>& bones);
            bool InitMaterials(const Appearance::Scene::Scene* scene, const std::string& filename);
            
            void Clear();
            
#define INVALID_MATERIAL 0xFFFFFFFF
            
            enum VB_TYPES { 
                INDEX_BUFFER,
                POS_VB,
                NORMAL_VB,
                TEXCOORD_VB,
                BONE_VB,
                NUM_VBs
            };
            
            GLuint VAO;
            GLuint buffers[NUM_VBs];
            
            struct MeshEntry
            {
                MeshEntry()
                {
                    numIndices = baseIndex = baseVertex = 0;
                    materialIndex = INVALID_MATERIAL;
                }
                
                unsigned int numIndices;
                unsigned int baseVertex;
                unsigned int baseIndex;
                unsigned int materialIndex;
            };
            
            std::vector<MeshEntry> entries;
            std::vector<Media::Images::Texture*> textures;
            
            /**
             * Map a bone name to its index.
             */
             /*
            std::map<string, unsigned int> boneMapping;
            unsigned int numBones;
            std::vector<BoneInfo> boneInfo;
            Math::mat4f globalInverseTransformation;
            
            const Appearance::Scene::Scene* scene;
        };
        */

        class MeshAnimationExample : public Windows::Application
        {
          public:
            MeshAnimationExample();
            virtual ~MeshAnimationExample();

            virtual bool Init();
            virtual void Prepare(float dt);
            virtual void Render();
            virtual void Shutdown();

            virtual void OnKeyPressed(Events::KeyboardEvent& event);
            virtual void OnMouse(Events::MouseEvent& event);

          protected:
            Geometry::Basic::Floor* floor;
            Rendering::Camera::Camera* camera;
        };
    }
}

#endif	/* MESHANIMATIONEXAMPLE_H */


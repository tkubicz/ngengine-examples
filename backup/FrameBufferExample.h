/* 
 * File:   FrameBufferExample.h
 * Author: tku
 *
 * Created on 12 grudzień 2013, 03:48
 */

#ifndef FRAMEBUFFEREXAMPLE_H
#define	FRAMEBUFFEREXAMPLE_H

#include "Windows/Windows.h"
#include "Events/KeyboardEvent.h"
#include "Events/MouseEvent.h"
#include "Rendering/Rendering.h"
#include "Geometry/Basic/Floor.h"
#include "Geometry/Models/ObjModel.h"
#include "Geometry/Basic/Box.h"

namespace NGE
{
    namespace Examples
    {

        class FrameBufferExample : public Windows::Application
        {
          public:
            FrameBufferExample();
            virtual ~FrameBufferExample();

            bool Init();
            void Prepare(float dt);
            void Render();
            void Shutdown();

            void OnKeyPressed(Events::KeyboardEvent& event);
            void OnMouse(Events::MouseEvent& event);

            //void OnResize(int width, int height);

          private:
            Rendering::Camera::Camera camera;
            Geometry::Basic::Floor floor;

            Geometry::Basic::Box box;
            float boxAngle;

            Geometry::ObjModel model;
            Geometry::Mesh* modelMesh;
            Media::Shaders::GLSLProgram* modelShader;
            GLuint modelVao;
            GLuint vertexSize;
            float modelAngle;
            void BuildVBO();


            GLuint fbo; // handle for the framebuffer object
            GLuint texFBO[2]; // texture handlers
            GLuint rb[2]; // renderbuffer handlers

            GLuint CreateRGBATexture(int w, int h);
            GLuint CreateDepthTexture(int w, int h);
            GLuint PrepareFBO(int w, int h, int colorCount);
        };
    }
}

#endif	/* FRAMEBUFFEREXAMPLE_H */


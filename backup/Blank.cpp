#include "Examples/Blank.h"
#include "Rendering/Renderer.h"
#include "Parsers/pugixml.hpp"
#include "GUI/GUI.h"
#include "Media/Media.h"
#include "Tools/Tools.h"
using namespace NGE::Examples;

Blank::Blank()
{
}

Blank::~Blank()
{
}

bool Blank::Init()
{
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file("data/configs/testGui.xml");
    pugi::xml_node panelxml = doc.child("Panel");

    //gui = new GUI::GUIFrame();
    //gui->LoadXMLSettings(panelxml);
    //gui->SetGUIEventListener(this);
    //gui->ForceUpdate(true);
    
    return true;
}

void Blank::Prepare(float dt)
{
    /*gui->Update(dt);
    
    GUI::GUILabel* label = (GUI::GUILabel*)gui->GetWidgetByCallbackString("labelFPS");
    label->SetLabelString("FPS: " + to_string(int(Tools::Timing::Get().fps / 1000.0)));

    label = (GUI::GUILabel*)gui->GetWidgetByCallbackString("labelShaderCount");
    label->SetLabelString("Shader count: " + to_string(Media::MediaManager::GetShaderManager().GetProgramCount()));

    label = (GUI::GUILabel*)gui->GetWidgetByCallbackString("labelTextureCount");
    label->SetLabelString("Texture count: " + to_string(Media::MediaManager::GetTextureManager().GetTextureCount()));

    label = (GUI::GUILabel*)gui->GetWidgetByCallbackString("labelMediaCount");
    label->SetLabelString("Media path count: " + to_string(Media::MediaManager::GetMediaPathManager().GetMediaPathCount()));

    label = (GUI::GUILabel*)gui->GetWidgetByCallbackString("labelFontCount");
    label->SetLabelString("Font count: " + to_string(Media::MediaManager::GetFontManager().GetFontCount()));*/
}

void Blank::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.9f, 0.95f, 1.0f, 1.0f);
    
    Rendering::Renderer::GetInstance().GetMatrixStack().Identity();
    
    //Rendering::Renderer::Enter2DMode();
    //gui->Render();
    //Rendering::Renderer::Exit2DMode();
}

void Blank::Shutdown()
{
    //delete gui;
}

void Blank::OnMouse(NGE::Events::MouseEvent& event)
{
    //gui->CheckMouseEvents(event);
}

void Blank::OnKeyPressed(NGE::Events::KeyboardEvent& event)
{
    //gui->CheckKeyboardEvents(event);
}

void Blank::OnResize(int width, int height)
{
    Application::OnResize(width, height);
    
    //gui->SetDimensions(width, height);
    //gui->ForceUpdate(true);
}

void Blank::ActionPerformed(NGE::GUI::GUIEvent& event)
{
}
/* 
 * File:   CubeMapExample.h
 * Author: tku
 *
 * Created on 1 listopad 2013, 04:37
 */

#ifndef CUBEMAPEXAMPLE_H
#define	CUBEMAPEXAMPLE_H

#include "Windows/Windows.h"
#include "Events/Events.h"
#include "Geometry/Basic/Floor.h"
#include "Geometry/Basic/Box.h"
#include "Geometry/Basic/Sphere.h"
#include "Rendering/Rendering.h"

namespace NGE
{
    namespace Examples
    {

        class Cube : public Geometry::Basic::Box
        {
          public:
            
            
            void Render()
            {
                if(shader == NULL)
                    return;

                shader->BindShader();

                texture->Activate();

                shader->SendUniform4x4("modelviewMatrix", Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(MODELVIEW_MATRIX));
                shader->SendUniform4x4("projectionMatrix", Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(PROJECTION_MATRIX));
                
                Math::mat3f normalMatrix = Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(MODELVIEW_MATRIX).ExtractMatrix3();
                shader->SendUniform3x3("normalMatrix", normalMatrix);
                shader->SendUniform3x3("inverseViewMatrix", normalMatrix, true);
                
                shader->SendUniform("cubemapTexture", 0);

                glBindVertexArray(vertexArray);
                glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
                glBindVertexArray(0);

                shader->UnbindShader();
            }
        };

        class CubeMapExample : public Windows::Application
        {
          public:
            CubeMapExample();
            virtual ~CubeMapExample();

            bool Init();
            void Prepare(float dt);
            void Render();
            void Shutdown();

            void OnKeyPressed(Events::KeyboardEvent& event);
            void OnMouse(Events::MouseEvent& event);

            void OnResize(int width, int height);

          private:
            Rendering::Camera::Camera camera;
            Geometry::Basic::Floor floor;
            Cube box;
            
            float angle;
        };
    }
}

#endif	/* CUBEMAPEXAMPLE_H */


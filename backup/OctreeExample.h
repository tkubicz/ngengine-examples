/* 
 * File:   OctreeExample.h
 * Author: tku
 *
 * Created on 26 grudzień 2013, 18:11
 */

#ifndef OCTREEEXAMPLE_H
#define	OCTREEEXAMPLE_H

#include "Windows/Windows.h"
#include "Events/MouseEvent.h"
#include "Events/KeyboardEvent.h"
#include "Rendering/Rendering.h"
#include "Geometry/Basic/Floor.h"
#include "Geometry/Basic/Sphere.h"
#include "Media/Images/Texture.h"
#include "Tools/Random.h"
#include "Rendering/Scene/Octree.h"

namespace NGE
{
    namespace Examples
    {
        class OctreeExample : public Windows::Application
        {
          public:
            OctreeExample();
            virtual ~OctreeExample();
            
            bool Init();
            void Prepare(float dt);
            void Render();
            void Shutdown();
            
            void OnKeyPressed(Events::KeyboardEvent& event);
            void OnMouse(Events::MouseEvent& event);
            
          private:
            Tools::Random random;
            Rendering::Camera::Camera camera;
            Media::Images::Texture* texture;
            Geometry::Basic::Floor floor;
            std::vector<Math::vec4f> spherePositions;
            Geometry::Basic::Sphere sphere;
            
            Rendering::Scene::Octree<Math::vec4f>* octree;
        };
    }
}

#endif	/* OCTREEEXAMPLE_H */


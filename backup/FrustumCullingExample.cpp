#include "Examples/FrustumCullingExample.h"
#include "Geometry/Models/ColladaModel.h"
#include "Math/Algorithms/Sort.h"
#include "Math/Quaternion.h"

using namespace NGE::Examples;

FrustumCullingExample::FrustumCullingExample() { }

FrustumCullingExample::~FrustumCullingExample() { }

bool FrustumCullingExample::Init()
{
    shaderManager = &NGE::Media::MediaManager::GetInstance().GetShaderManager();
    textureManager = &NGE::Media::MediaManager::GetInstance().GetTextureManager();

    // Load shaders
    shaderManager->LoadProgram("floorShader", "floorShader.xml");
    shaderManager->LoadProgram("colorShader", "colorShader.xml");
    shaderManager->LoadProgram("solidColorShader", "colorShader.xml");
    shaderManager->LoadProgram("planeShader", "basicGeometryShaders.xml");
    shaderManager->LoadProgram("point_light_per_pixel", "diffuseShader.xml");

    camera = new Rendering::Camera::Camera(0.0f, 10.0f, 30.0f, 0.0f, 0.0f, 0.0f);

    frustum = new Rendering::Camera::Frustum<real>();
    frustum->SetCameraInternals(52.f, 800.f / 600.f, 0.1f, 1000.f);
    frustum->SetCameraDefinition(camera->GetViewerPosition(), camera->GetFocusPosition(), camera->GetUpDirection());

    octree = new Rendering::Scene::Octree<real>(Math::vec3f(0, 0, 0), Math::vec3f(20, 20, 20));

    floor = new Geometry::Basic::Floor();
    floor->Initialize(200.f, 4.f, shaderManager->GetProgram("floorShader"));
    floor->SetColor(Math::vec4f(131.f / 255.f, 139.f / 255.f, 131.f / 255.f, 1.0f));

    box = new Geometry::Basic::Box();
    box->Initialize(2.0f);
    box->SetShader(shaderManager->GetProgram("colorShader"));

    planeTga = new Plane();
    planeTga->Initialize();
    planeTga->shader = shaderManager->GetProgram("planeShader");

    planeJpeg = new Plane();
    planeJpeg->Initialize();
    planeJpeg->shader = shaderManager->GetProgram("planeShader");

    planePng = new Plane();
    planePng->Initialize();
    planePng->shader = shaderManager->GetProgram("planeShader");

    jpeg.Load("data/textures/jpeg/wall3.jpg");
    tga.Load("data/textures/jpeg/test2.tga");
    png.Load("data/textures/jpeg/test.png");

    jpeg.Load("data/models/x/body.jpg");
    Media::Images::Texture* tempTex = new Media::Images::Texture();
    tempTex->Load2DImage(jpeg, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR_MIPMAP_LINEAR, GL_RGB, GL_RGB, true);
    textureManager->AddTexture("body.jpg", tempTex);

    jpeg.Load("data/models/x/face.jpg");
    tempTex = new Media::Images::Texture();
    tempTex->Load2DImage(jpeg, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR_MIPMAP_LINEAR, GL_RGB, GL_RGB, true);
    textureManager->AddTexture("face.jpg", tempTex);

    jpeg.Load("data/models/x/helmet.jpg");
    tempTex = new Media::Images::Texture();
    tempTex->Load2DImage(jpeg, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR_MIPMAP_LINEAR, GL_RGB, GL_RGB, true);
    textureManager->AddTexture("helmet.jpg", tempTex);

    jpeg.Load("data/models/x/pulserifle.jpg");
    tempTex = new Media::Images::Texture();
    tempTex->Load2DImage(jpeg, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR_MIPMAP_LINEAR, GL_RGB, GL_RGB, true);
    textureManager->AddTexture("pulserifle.jpg", tempTex);

    //tex.Load2DImage(jpeg);
    //tex.Load2DImage(tga, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR_MIPMAP_LINEAR, GL_RGBA, GL_RGBA, true);

    texTga.Load2DImage(tga, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR_MIPMAP_LINEAR, GL_RGBA, GL_RGBA, true);
    planeTga->texture = &texTga;

    texJpeg.Load2DImage(jpeg, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR_MIPMAP_LINEAR, GL_RGB, GL_RGB, true);
    planeJpeg->texture = &texJpeg;

    texPng.Load2DImage(png, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR_MIPMAP_LINEAR, GL_RGBA, GL_RGBA, true);
    planePng->texture = &texPng;

    random = new Tools::Random();
    boxes = new BoxStruct[boxesCount];
    GenerateBoxes();

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    xmesh.shader = shaderManager->GetProgram("point_light_per_pixel");
    xmesh.xmodel = new Geometry::Models::XModel();
    xmesh.xmodel->Initialise("data/models/x/soldier.X");
    Geometry::Models::XModel::Scene* scene = xmesh.xmodel->GetScene();

    xmesh.Sort();
    xmesh.GenerateVBO();
    xmesh.GenerateVBA();

    xmesh2.xmodel = new Geometry::Models::XModel();
    xmesh2.xmodel->Initialise("data/models/x/soldier2.X");

    /*std::cout << "NumAnimations: " << xmesh2.xmodel->GetGenericScene()->GetNumAnimations() << std::endl;
    for (unsigned int i = 0; i < xmesh2.xmodel->GetGenericScene()->GetNumAnimations(); ++i)
    {
        std::cout << "Animation: " << xmesh2.xmodel->GetGenericScene()->GetAnimations()[i]->GetName() << std::endl;
        std::cout << "TicksPerSecond: " << xmesh2.xmodel->GetGenericScene()->GetAnimations()[i]->GetTicksPerSecond() << std::endl;
        std::cout << "NumChannels :" << xmesh2.xmodel->GetGenericScene()->GetAnimations()[i]->GetNumChannels() << std::endl;
        std::cout << "Duration: " << xmesh2.xmodel->GetGenericScene()->GetAnimations()[i]->GetDuration() << std::endl;
        for (unsigned int j = 0; j < xmesh2.xmodel->GetGenericScene()->GetAnimations()[i]->GetNumChannels(); ++j)
        {
            std::cout << "Node: " << xmesh2.xmodel->GetGenericScene()->GetAnimations()[i]->GetChannels()[j]->GetNodeName() << std::endl;
            std::cout << "NumPositionKeys: " << xmesh2.xmodel->GetGenericScene()->GetAnimations()[i]->GetChannels()[j]->GetNumPositionKeys() << std::endl;
            std::cout << "NumRotationKeys: " << xmesh2.xmodel->GetGenericScene()->GetAnimations()[i]->GetChannels()[j]->GetNumRotationKeys() << std::endl;
            std::cout << "NumScalingKeys: " << xmesh2.xmodel->GetGenericScene()->GetAnimations()[i]->GetChannels()[j]->GetNumScalingKeys() << std::endl;
            for (unsigned int k = 0; k < xmesh2.xmodel->GetGenericScene()->GetAnimations()[i]->GetChannels()[j]->GetNumPositionKeys(); ++k)
            {
                std::cout << "PositionKeys time: " << xmesh2.xmodel->GetGenericScene()->GetAnimations()[i]->GetChannels()[j]->GetPositionKeys()[k].GetTime() << std::endl;
                std::cout << "PositionKeys value: " << xmesh2.xmodel->GetGenericScene()->GetAnimations()[i]->GetChannels()[j]->GetPositionKeys()[k].GetValue() << std::endl;
            }
        }

        std::cout << std::endl;
    }*/

    return true;
}

void FrustumCullingExample::Shutdown() {
    //delete camera;
    //delete frustum;
    //delete octree;

    //delete planeTga;
    //delete planeJpeg;
    //delete planePng;

    //delete floor;
    //delete box;

    //delete[] boxes;
    //delete random;
}

void FrustumCullingExample::Prepare(float dt)
{
    camera->Update(dt);
    frustum->SetCameraDefinition(camera->GetViewerPosition(), camera->GetFocusPosition(), camera->GetUpDirection());
}

void FrustumCullingExample::Render()
{
    Rendering::Renderer::GetInstance().ClearBuffers();
    Rendering::Renderer::GetInstance().ClearColor(0.9f, 0.95f, 1.f, 1.f);
    matrixStack.Identity();

    camera->Look();
    floor->Render();
    //box->Render();

    matrixStack.PushMatrix();
    matrixStack.Translate(0, 1, -20);

    matrixStack.PushMatrix();
    matrixStack.Translate(-20, 0, 0);
    planeTga->Render();
    matrixStack.PopMatrix();

    planeJpeg->Render();

    matrixStack.PushMatrix();
    matrixStack.Translate(20, 0, 0);
    planePng->Render();
    matrixStack.PopMatrix();

    matrixStack.PopMatrix();

    xmesh.RenderX();

    Math::Objects3D::aaboxf aabox;
    int rendered = 0;
    for (int i = 0; i < boxesCount; ++i)
    {
        aabox.SetBox(boxes[i].position, boxes[i].halfSize);
        if (frustum->AABoxInFrustum(aabox) != 0)
        {
            matrixStack.PushMatrix();
            matrixStack.Translate(boxes[i].position);
            matrixStack.Scale(boxes[i].halfSize);
            box->Render();
            matrixStack.PopMatrix();
            rendered++;
        }
    }
    //std::cout << rendered << "/256" << std::endl;
    window->SetWindowTitle(std::string(to_string(rendered) + "/256"));

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    frustum->Render();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void FrustumCullingExample::OnResize(int width, int height)
{
    Application::OnResize(width, height);

    frustum->SetCameraInternals(52.f, (float) width / float(height), 0.1f, 1000.f);
}

void FrustumCullingExample::OnKeyPressed(Events::KeyboardEvent& event)
{
    if (event.GetAction() == Events::PRESSED)
    {
        switch (event.GetKeyId())
        {
            case Events::KEY_Z:
                if (camera->IsMouseLocked())
                    window->EnableMouseCursor(true);
                else
                    window->EnableMouseCursor(false);
                camera->LockMouse(!camera->IsMouseLocked());
                break;

            case Events::KEY_C:
                CheckIfInFrustum();
                break;
        }
    }

    camera->SetKeyboardInput(event);
}

void FrustumCullingExample::OnMouse(Events::MouseEvent& event)
{
    camera->SetMouseInfo(event.GetX(), event.GetY());
}

void FrustumCullingExample::CheckIfInFrustum()
{
    Math::Objects3D::aaboxf aabox;
    int count = 0;
    for (int i = 0; i < boxesCount; ++i)
    {
        aabox.SetBox(boxes[i].position, boxes[i].halfSize);
        if (frustum->AABoxInFrustum(aabox) != 0)
            count++;
    }
    std::cout << "In frustum: " << count << std::endl;
}

void FrustumCullingExample::GenerateBoxes()
{

    static const float minBound = -200;
    static const float maxBound = 200;
    static const float minHeight = 0;
    static const float maxHeight = 50;

    for (int i = 0; i < boxesCount; ++i)
    {
        boxes[i].position = random->RandomVector(Math::vec3f(minBound, minHeight, minBound), Math::vec3f(maxBound, maxHeight, maxBound));
        float halfSize = random->RandomFloat(0.5f, 5.0f);
        boxes[i].halfSize = Math::vec3f(halfSize, halfSize, halfSize);
    }
}

void FrustumCullingExample::XMesh::Sort()
{
    Geometry::Models::XModel::Mesh* mesh = xmodel->GetScene()->rootNode->meshes[0];

    std::cout << mesh->faceMaterials.size() << std::endl;
    std::cout << mesh->posFaces.size() << std::endl;
    std::cout << mesh->texCoords[0].size() << std::endl;
    std::cout << mesh->normFaces.size() << std::endl;
    std::cout << mesh->normals.size() << std::endl;

    indices.reserve(xmodel->GetScene()->rootNode->meshes[0]->posFaces.size() * 3);
    for (int i = 0; i < xmodel->GetScene()->rootNode->meshes[0]->posFaces.size(); ++i)
    {
        for (int j = 0; j < xmodel->GetScene()->rootNode->meshes[0]->posFaces[i].indices.size(); ++j)
        {
            indices.push_back(xmodel->GetScene()->rootNode->meshes[0]->posFaces[i].indices[j]);
        }
    }

    // Sort indices, materials and normals by meterials
    size_t j = 0;
    for (size_t i = 1; i < mesh->faceMaterials.size(); ++i)
    {
        j = i;
        while ((j > 0) && (mesh->faceMaterials[j - 1] > mesh->faceMaterials[j]))
        {
            std::swap(mesh->faceMaterials[j - 1], mesh->faceMaterials[j]);
            std::swap(mesh->posFaces[j - 1], mesh->posFaces[j]);
            std::swap(mesh->normFaces[j - 1], mesh->normFaces[j]);
            j--;
        }
    }

    // Find out, wher every material starts and ends
    MaterialStartEnd mat;
    mat.materialIndex = mesh->faceMaterials[0];
    mat.start = 0;
    mat.end = -1;
    for (size_t i = 1; i < mesh->faceMaterials.size(); ++i)
    {
        if (mesh->faceMaterials[i] != mat.materialIndex || (i + 1 == mesh->faceMaterials.size()))
        {
            mat.end = (i - 1) * 3 + 3;
            materials.push_back(mat);

            mat.materialIndex = mesh->faceMaterials[i];
            mat.start = (i * 3);
        }
    }

    // Add textures
    for (size_t i = 0; i < materials.size(); ++i)
    {
        std::cout << "material " << i << ": " << materials[i].materialIndex << std::endl;
        std::cout << "start: " << materials[i].start << std::endl;
        std::cout << "end: " << materials[i].end << std::endl;
        std::cout << "name: " << xmodel->GetScene()->globalMaterials[materials[i].materialIndex].textures[0].name << std::endl;
        std::cout << "ref: " << xmodel->GetScene()->globalMaterials[materials[i].materialIndex].isReference << std::endl;
        std::cout << "ref2: " << mesh->materials[i].isReference << std::endl << std::endl;

        materials[i].texture = NGE::Media::MediaManager::GetInstance().GetTextureManager().GetTexture(xmodel->GetScene()->globalMaterials[materials[i].materialIndex].textures[0].name);
    }
}

void FrustumCullingExample::XMesh::GenerateVBO()
{
    glGenBuffers(4, buffers);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof (GLfloat) * 3 * xmodel->GetScene()->rootNode->meshes[0]->positions.size(), &xmodel->GetScene()->rootNode->meshes[0]->positions[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof (GLfloat) * 2 * xmodel->GetScene()->rootNode->meshes[0]->texCoords[0].size(), &xmodel->GetScene()->rootNode->meshes[0]->texCoords[0][0], GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[2]);
    glBufferData(GL_ARRAY_BUFFER, sizeof (GLfloat) * 3 * xmodel->GetScene()->rootNode->meshes[0]->normals.size(), &xmodel->GetScene()->rootNode->meshes[0]->normals[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[3]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof (GLuint) * indices.size(), &indices[0], GL_STATIC_DRAW);
}

void FrustumCullingExample::XMesh::GenerateVBA()
{
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[2]);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(2);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[3]);

    glBindVertexArray(0);
}

void FrustumCullingExample::XMesh::RenderX()
{
    shader->BindShader();

    shader->SendUniform4x4("modelview_matrix", matrixStack.GetMatrix(MODELVIEW_MATRIX));
    shader->SendUniform4x4("projection_matrix", matrixStack.GetMatrix(PROJECTION_MATRIX));
    shader->SendUniform3x3("normal_matrix", &Rendering::Renderer::GetInstance().CalculateNormalMatrix(matrixStack.GetMatrix(MODELVIEW_MATRIX))[0]);

    shader->SendUniform("light0.position", Math::vec4f(0.0f, 5.0f, -1380.0f));
    shader->SendUniform("light0.diffuse", Math::vec4f(0.8f, 0.8f, 0.8f, 1.0f));
    shader->SendUniform("light0.specular", Math::vec4f(0.6f, 0.6f, 0.6f, 1.0f));
    shader->SendUniform("light0.ambient", Math::vec4f(0.7f, 0.7f, 0.7f, 1.0f));
    shader->SendUniform("light0.constant_attenuation", 0.03f);
    shader->SendUniform("light0.linear_attenuation", 0.03f);
    shader->SendUniform("light0.quadratic_attenuation", 0.03f);

    //shader->SendUniform("color", Math::vec4f(1.0f, 0.0f, 0.0f, 1.0f));

    //Geometry::Models::XModel::Mesh* mesh = xmodel->GetScene()->globalMaterials

    glBindVertexArray(vao);

    for (int i = 0; i < materials.size(); ++i)
    {
        shader->SendUniform("material0.ambient", Math::vec4f(0.5f, 0.5f, 0.5f, 1.0f));
        shader->SendUniform("material0.diffuse", xmodel->GetScene()->globalMaterials[materials[i].materialIndex].diffuse);
        shader->SendUniform("material0.specular", xmodel->GetScene()->globalMaterials[materials[i].materialIndex].specular);
        shader->SendUniform("material0.emissive", xmodel->GetScene()->globalMaterials[materials[i].materialIndex].emissive);
        shader->SendUniform("shininess", xmodel->GetScene()->globalMaterials[materials[i].materialIndex].specularExponenet);

        materials[i].texture->Activate();
        shader->SendUniform("texture0", 0);

        glDrawRangeElements(GL_TRIANGLES, indices[0], indices[indices.size() - 1], materials[i].end - materials[i].start, GL_UNSIGNED_INT, (void*) (materials[i].start * sizeof (GLuint)));
    }

    glBindVertexArray(0);

    shader->UnbindShader();
}

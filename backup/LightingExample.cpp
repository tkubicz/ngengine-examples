#include "Examples/LightingExample.h"
#include "Tools/Timing.h"
#include "GUI/GUILabel.h"
#include "Media/MediaManager.h"
#include "Math/Matrix3.h"
#include <GL/glfw.h>

using namespace NGE::Examples;

LightingExample::LightingExample() { }

LightingExample::~LightingExample() { }

bool LightingExample::Init()
{
    Media::MediaManager::GetInstance().GetShaderManager().LoadProgram("only_vertex4_shader", "basicShader.xml");
    Media::MediaManager::GetInstance().GetShaderManager().LoadProgram("floorShader", "floorShader.xml");
    Media::MediaManager::GetInstance().GetShaderManager().LoadProgram("basicShader", "shaders.xml");
    Media::MediaManager::GetInstance().GetShaderManager().LoadProgram("skyDomeShader", "skydome.xml");

    camera.Set(-35.0f, 10.0f, 0.0f, 50.0f, 10.0f, 22.0f, 0.0f, 1.0f, 0.0f);
    floor.Initialize(300.f, 4.f, Media::MediaManager::GetInstance().GetShaderManager().GetProgram("floorShader"));

    pugi::xml_document terrainInfo;
    pugi::xml_parse_result terrainResult = terrainInfo.load_file("data/configs/terrain.xml");
    pugi::xml_node terrrainNode = terrainInfo.child("Terrain");

    if (!terrain.LoadXMLSettings(terrrainNode))
    {
        Tools::Logger::WriteErrorLog("Could not load terrain information from xml file");
    }

    sphere.Initialize(15, 15, 1, Media::MediaManager::GetInstance().GetShaderManager().GetProgram("basicShader"));
    sphere.SetTexture(Media::MediaManager::GetInstance().GetTextureManager().GetTexture("grass"));
    spherePosition.Set(20.0f, 30.0f, 20.0f);

    rotationAngle = 0.0f;
    rotationSpeed = 14.0f;

    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file("data/configs/testGui.xml");
    pugi::xml_node panelxml = doc.child("Panel");

    gui = new GUI::GUIFrame();
    gui->LoadXMLSettings(panelxml);
    gui->SetGUIEventListener(this);

    pugi::xml_document textureInfo;
    pugi::xml_parse_result textureResult = textureInfo.load_file("data/configs/textures.xml");
    //pugi::xml_node currentTexture = textureInfo.child("Texture2D");

    for (pugi::xml_node currentTexture = textureInfo.child("Texture2D"); currentTexture != NULL; currentTexture = currentTexture.next_sibling())
    {
        Media::MediaManager::GetInstance().GetTextureManager().LoadTexture(currentTexture);
    }

    skyPlane.GenerateSkyPlane(15, 2000, 800, 3, 3, 0.85f, false);
    skyPlane.SetShader(Media::MediaManager::GetInstance().GetShaderManager().GetProgram("basicShader"));
    skyPlane.SetTexture(Media::MediaManager::GetInstance().GetTextureManager().GetTexture("sky"));
    
    latitude = 51.12f;
    longitude = 17.13f;
    time = 18.0f;
    day = 275.0f;
    turbidity = 3.0f;
    
    skyDome.Initialize(1600, 96, 64, false, 1.0);
    skyDome.SetShader(Media::MediaManager::GetInstance().GetShaderManager().GetProgram("skyDomeShader"));
    skyDome.SetTexture(Media::MediaManager::GetInstance().GetTextureManager().GetTexture("sky2"));
    skyDome.SetCamera(&camera);
    skyDome.SetCoordinates(latitude, longitude, time, day, turbidity);
    skyDome.Update(0);
    
    mesh = new Geometry::Skeleton::SkinnedMesh(Geometry::Skeleton::SkinnedMesh::QUAD);
    mesh->SetShader(Media::MediaManager::GetInstance().GetShaderManager().GetProgram("only_vertex4_shader"));
    mesh->LoadMesh("data/HumanModel.txt");
    mesh->SetColor(1.0, 0.5, 0.0);
    
    skeleton = new Geometry::Skeleton::Skeleton();
    skeleton->LoadSkeleton("data/Skeleton.txt");
    mesh->AttachSkeleton(skeleton);
    mesh->Preprocess();
    
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    return true;
}

void LightingExample::Prepare(float dt)
{
    rotationAngle += dt * rotationSpeed;
    if (rotationAngle >= 360.0f)
        rotationAngle -= 360;

    skyDome.Update(dt);
    gui->Update(dt);
    camera.Update(dt);
    terrain.Update(dt);
    
    float height = terrain.GetHeightAt(spherePosition);
    spherePosition.y = height + 0.5f;

    GUI::GUILabel* label = (GUI::GUILabel*)gui->GetWidgetByCallbackString("labelFPS");
    label->SetLabelString("FPS: " + to_string(int(Tools::Timing::Get().fps / 1000.0)));

    label = (GUI::GUILabel*)gui->GetWidgetByCallbackString("labelShaderCount");
    label->SetLabelString("Shader count: " + to_string(Media::MediaManager::GetInstance().GetShaderManager().GetProgramCount()));

    label = (GUI::GUILabel*)gui->GetWidgetByCallbackString("labelTextureCount");
    label->SetLabelString("Texture count: " + to_string(Media::MediaManager::GetInstance().GetTextureManager().GetTextureCount()));

    label = (GUI::GUILabel*)gui->GetWidgetByCallbackString("labelMediaCount");
    label->SetLabelString("Media path count: " + to_string(Media::MediaManager::GetInstance().GetMediaPathManager().GetMediaPathCount()));

    label = (GUI::GUILabel*)gui->GetWidgetByCallbackString("labelFontCount");
    label->SetLabelString("Font count: " + to_string(Media::MediaManager::GetInstance().GetFontManager().GetFontCount()));
    
    skeleton->Rotate(2, -70, 0, 75);
	skeleton->Rotate(3, 30, 0, -75);
	skeleton->Rotate(4, 0, 40, 0);
	skeleton->Rotate(5, 0, -10, 0);
	skeleton->Rotate(7, -20, 0, 5);
	skeleton->Rotate(8, -20, 0, -5);
	skeleton->Rotate(9, 20, 0, 0);
    skeleton->Rotate(10, 50, 0, 0);
    
    static float angle = 0.0f;
    static bool forward = true;
    static float moveSpeed = 50.0f;
    
    if (forward)
        angle += moveSpeed * dt;
    else
        angle -= moveSpeed * dt;
    
    if ((forward && angle > 80) || !forward && angle < -80)
    {
        
        forward = !forward;
    }
    
    //skeleton->Rotate(3, angle, 0, -75);
}

void LightingExample::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.9f, 0.95f, 1.0f, 1.0f);
    Rendering::Renderer::GetInstance().GetMatrixStack().Identity();

    camera.Look();
    floor.Render();
    //terrain.Render();
    //skyPlane.Render(0.5f);
    //skyDome.Render();
    
    mesh->Render();

    Rendering::Renderer::GetInstance().Enter2DMode();
    gui->Render();
    Rendering::Renderer::GetInstance().Exit2DMode();
}

void LightingExample::Shutdown() { }

void LightingExample::OnKeyPressed(NGE::Events::KeyboardEvent& event)
{
    camera.SetKeyboardInput(event);
    gui->CheckKeyboardEvents(event);

    if (event.GetAction() == Events::PRESSED)
    {
        if (event.GetKeyId() == Events::KEY_Z)
        {
            if (camera.IsMouseLocked()) {
                window->EnableMouseCursor(true);
            }
            else {
                window->EnableMouseCursor(false);
            }

            camera.LockMouse(!camera.IsMouseLocked());
        }
        else if (event.GetKeyId() == Events::KEY_X)
        {
            terrain.SetWireframe(!terrain.GetWireframe());
        }
        else if (event.GetKeyId() == Events::KEY_C)
        {
            float height = terrain.GetHeightAt(camera.GetViewerPosition());
            Tools::Logger::WriteInfoLog("Terrain --> Viewer position: " + to_string(camera.GetViewerPosition()));
            Tools::Logger::WriteInfoLog("Terrain --> Height at viewer position: " + to_string(height));
        }
        else if (event.GetKeyId() == Events::KEY_NUM8)
        {
            spherePosition.x += 0.1f;
        }
        else if (event.GetKeyId() == Events::KEY_UP)
        {
            time += 1.0f;
            skyDome.SetCoordinates(latitude, longitude, time, day, turbidity);
        }
    }
}

void LightingExample::OnMouse(NGE::Events::MouseEvent& event)
{
    camera.SetMouseInfo(event.GetX(), event.GetY());
    gui->CheckMouseEvents(event);
}

void LightingExample::OnResize(int width, int height)
{
    Application::OnResize(width, height);

    gui->SetDimensions(width, height);
    gui->ForceUpdate(true);
}

void LightingExample::ActionPerformed(NGE::GUI::GUIEvent& event) { }


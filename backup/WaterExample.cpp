#include "Examples/WaterExample.h"
#include "Media/MediaManager.h"
#include <GL/gl.h>
using namespace NGE::Examples;

const int WaterExample::WATER_PLANE_LENGTH = 128;
const int WaterExample::NUMBERWAVES = 4;

WaterExample::WaterExample()
{
    passedTime = 0;
}

WaterExample::~WaterExample() { }

bool WaterExample::Init()
{
    Media::Shaders::ShaderManager* shaderManager = &Media::MediaManager::GetInstance().GetShaderManager();
    Media::Images::TextureManager* textureManager = &Media::MediaManager::GetInstance().GetTextureManager();

    shaderManager->LoadProgram("waterShader", "water.xml");
    shaderManager->LoadProgram("waterTextureShader", "water.xml");
    shaderManager->LoadProgram("floorShader", "floorShader.xml");

    camera.Set(-35.0f, 10.0f, 0.0f, 50.0f, 10.0f, 22.0f, 0.0f, 1.0f, 0.0f);
    floor.Initialize(64.f, 4.0f, shaderManager->GetProgram("floorShader"));

    waterShader = shaderManager->GetProgram("waterShader");
    waterTexture.Initialize(128, shaderManager->GetProgram("waterTextureShader"));

    float* points = new float[WATER_PLANE_LENGTH * WATER_PLANE_LENGTH * 4 * sizeof (GLfloat)];
    unsigned int* indices = new unsigned int[WATER_PLANE_LENGTH * (WATER_PLANE_LENGTH - 1) * 2 * sizeof (GLuint)];

    for (int z = 0; z < WATER_PLANE_LENGTH; ++z)
    {
        for (int x = 0; x < WATER_PLANE_LENGTH; ++x)
        {
            points[(x + z * (WATER_PLANE_LENGTH)) * 4 + 0] = -(float) WATER_PLANE_LENGTH / 2 + 0.5f + (float) x;
            points[(x + z * (WATER_PLANE_LENGTH)) * 4 + 1] = 0.0f;
            points[(x + z * (WATER_PLANE_LENGTH)) * 4 + 2] = (float) WATER_PLANE_LENGTH / 2 - 0.5f - (float) z;
            points[(x + z * (WATER_PLANE_LENGTH)) * 4 + 3] = 1.0f;
        }
    }

    for (int k = 0; k < WATER_PLANE_LENGTH - 1; ++k)
    {
        for (int i = 0; i < WATER_PLANE_LENGTH; ++i)
        {
            if (k % 2 == 0)
            {
                indices[(i + k * (WATER_PLANE_LENGTH)) * 2 + 0] = i + (k + 1) * WATER_PLANE_LENGTH;
                indices[(i + k * (WATER_PLANE_LENGTH)) * 2 + 1] = i + k * WATER_PLANE_LENGTH;
            }
            else
            {
                indices[(i + k * (WATER_PLANE_LENGTH)) * 2 + 0] = WATER_PLANE_LENGTH - 1 - i + k * WATER_PLANE_LENGTH;
                indices[(i + k * (WATER_PLANE_LENGTH)) * 2 + 1] = WATER_PLANE_LENGTH - 1 - i + (k + 1) * WATER_PLANE_LENGTH;
            }
        }
    }

    glGenBuffers(1, &verticesVBO);
    glBindBuffer(GL_ARRAY_BUFFER, verticesVBO);
    glBufferData(GL_ARRAY_BUFFER, WATER_PLANE_LENGTH * WATER_PLANE_LENGTH * 4 * sizeof (GLfloat), (GLfloat*) points, GL_STATIC_DRAW);

    glGenBuffers(1, &indicesVBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, WATER_PLANE_LENGTH * (WATER_PLANE_LENGTH - 1) * 2 * sizeof (GLuint), (GLuint*) indices, GL_STATIC_DRAW);

    delete points;
    delete indices;

    pugi::xml_document textureInfo;
    pugi::xml_parse_result textureResult = textureInfo.load_file("data/configs/waterExample.xml");

    for (pugi::xml_node currentTexture = textureInfo.child("Texture2D"); currentTexture != NULL; currentTexture = currentTexture.next_sibling())
        textureManager->LoadTexture(currentTexture);

    for (pugi::xml_node currentTexture = textureInfo.child("TextureCubeMap"); currentTexture != NULL; currentTexture = currentTexture.next_sibling())
        textureManager->LoadTexture(currentTexture);

    waterTextureId = waterTexture.Initialize((float)WATER_PLANE_LENGTH, shaderManager->GetProgram("waterTextureShader"));
    waterCubeMap = textureManager->GetTexture("waterExampleCube");
    
    // TODO: Do usunięcia po testach
    Media::Images::Texture* waterTexture2 = textureManager->GetTexture("waterTexture");

    waterShader->BindShader();
    
    waterShader->SendUniform("u_waterPlaneLength", (float)WATER_PLANE_LENGTH);

    waterCubeMap->Activate(0);
    waterShader->SendUniform("u_cubemap", 0);

    /*glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, waterTextureId);
    waterShader->SendUniform("u_waterTexture", 1);*/
    waterTexture2->Activate(1);
    waterShader->SendUniform("u_waterTexture", 1);

    waterShader->UnbindShader();

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, verticesVBO);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVBO);

    glBindVertexArray(0);

    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    return true;
}

void WaterExample::InverseRigidBody(NGE::Math::mat4f& mat)
{
    Math::vec3f scales = mat.GetScale();
    Math::mat4f inverseScale, inverseRotation, inverseTranslation;

    inverseScale.Scale(1.0f / (scales.x * scales.x),
                       1.0f / (scales.y * scales.y),
                       1.0f / (scales.z * scales.z));

    // Skopiowanie całej macierzy
    inverseRotation = mat;
    // Potrzebujemy tylko macierzy rotacji, więc zerujemy resztę
    inverseRotation[3] = inverseRotation[7] = inverseRotation[11] = inverseRotation[12] = inverseRotation[13] = inverseRotation[14] = inverseRotation[15] = 0.0f;

    inverseRotation.SetTranspose();

    inverseTranslation.Translate(-mat[12], -mat[13], -mat[14]);

    mat = inverseRotation * inverseTranslation;
    mat = inverseScale * mat;
}

void WaterExample::Prepare(float dt)
{
    camera.Update(dt);
    
    waterTexture.Update(dt);
    
    static float angle = 0.0f;

    //Math::mat4f inverseViewMatrix = Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(MODELVIEW_MATRIX);
    viewMatrix.LookAt(0.0f, 1.0f, 0.0f, (float) 0.5f * sin(angle), 1.0f, -(float)0.5f * cos(angle), 0.0f, 1.0f, 0.0f);
    Math::mat4f inverseViewMatrix = viewMatrix;
    
    if (!inverseViewMatrix.SetInverse())
    {
        Tools::Logger::WriteErrorLog("Could not inverse modelview matrix");
        return;
    }
    
    InverseRigidBody(inverseViewMatrix);
    inverseViewNormalMatrix = inverseViewMatrix.ExtractMatrix3();
    
    passedTime += dt;
    angle += 2.0f * (float) Math::MathUtils::PI / 120.0f * dt;
}

void WaterExample::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.9f, 0.95f, 1.0f, 1.0f);

    camera.Look();
    floor.Render();
    
    waterTexture.Render();
    RenderWater();

    //Rendering::Renderer::GetInstance().GetMatrixStack().Identity();
}

void WaterExample::Shutdown() { }

void WaterExample::OnKeyPressed(NGE::Events::KeyboardEvent& event)
{
    if (event.GetAction() == Events::PRESSED)
    {
        switch (event.GetKeyId())
        {
            case Events::KEY_Z:
                if (camera.IsMouseLocked())
                    window->EnableMouseCursor(true);
                else
                    window->EnableMouseCursor(false);
                camera.LockMouse(!camera.IsMouseLocked());
                break;
        }
    }

    camera.SetKeyboardInput(event);
}

void WaterExample::OnMouse(NGE::Events::MouseEvent& event)
{
    camera.SetMouseInfo(event.GetX(), event.GetY());
}

void WaterExample::OnResize(int width, int height)
{
    Application::OnResize(width, height);

    waterTexture.Reshape(width, height);

    waterShader->BindShader();
    waterShader->SendUniform4x4("u_projectionMatrix", Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(PROJECTION_MATRIX));
    waterShader->UnbindShader();
}

void WaterExample::RenderWater()
{
    Geometry::Nature::WaterTexture::WaveParameters waveParameters[NUMBERWAVES];
    Geometry::Nature::WaterTexture::WaveDirections waveDirections[NUMBERWAVES];

    static float overallSteepness = 0.2f;

    memset(waveParameters, 0, sizeof (waveParameters));
    memset(waveDirections, 0, sizeof (waveDirections));

    // Wave one
    waveParameters[0].speed = 1.0f;
    waveParameters[0].amplitude = 0.01f;
    waveParameters[0].wavelength = 4.0f;
    waveParameters[0].steepness = overallSteepness / (waveParameters[0].wavelength * waveParameters[0].amplitude * float(NUMBERWAVES));
    waveDirections[0].x = +1.0f;
    waveDirections[0].z = +1.0f;

    // Wave two
    waveParameters[1].speed = 0.5f;
    waveParameters[1].amplitude = 0.02f;
    waveParameters[1].wavelength = 3.0f;
    waveParameters[1].steepness = overallSteepness / (waveParameters[1].wavelength * waveParameters[1].amplitude * float(NUMBERWAVES));
    waveDirections[1].x = +1.0f;
    waveDirections[1].z = +0.0f;

    // Wave three
    waveParameters[2].speed = 0.1f;
    waveParameters[2].amplitude = 0.015f;
    waveParameters[2].wavelength = 2.0f;
    waveParameters[2].steepness = overallSteepness / (waveParameters[2].wavelength * waveParameters[2].amplitude * float(NUMBERWAVES));
    waveDirections[2].x = -0.1f;
    waveDirections[2].z = -0.2f;

    // Wave four
    waveParameters[3].speed = 1.1f;
    waveParameters[3].amplitude = 0.008f;
    waveParameters[3].wavelength = 1.0f;
    waveParameters[3].steepness = overallSteepness / (waveParameters[3].wavelength * waveParameters[3].amplitude * float(NUMBERWAVES));
    waveDirections[3].x = -0.2f;
    waveDirections[3].z = -0.1f;

    waterShader->BindShader();

    waterShader->SendUniform4x4("u_viewMatrix", viewMatrix);
    waterShader->SendUniform3x3("u_inverseViewNormalMatrix", inverseViewNormalMatrix);
    
    waterShader->SendUniform("u_passedTime", passedTime);
    
    waterShader->SendUniformArray("u_waveParameters", 4 * NUMBERWAVES, (GLfloat*) waveParameters);
    waterShader->SendUniformArray("u_waveDirections", 2 * NUMBERWAVES, (GLfloat*) waveDirections);
    
    glBindVertexArray(VAO);
    glFrontFace(GL_CCW);
    glDrawElements(GL_TRIANGLE_STRIP, WATER_PLANE_LENGTH * (WATER_PLANE_LENGTH - 1) * 2, GL_UNSIGNED_INT, 0);
    
    glBindVertexArray(0);
}
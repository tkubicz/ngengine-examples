#include "Examples/GPUParticles.h"
#include "Media/MediaManager.h"
using namespace NGE::Examples;

GPUParticles::GPUParticles()
{
    flip = 0;
}

GPUParticles::~GPUParticles() { }

bool GPUParticles::Init()
{
    Media::Shaders::ShaderManager* shaderManager = &Media::MediaManager::GetInstance().GetShaderManager();
    Media::Images::TextureManager* textureManager = &Media::MediaManager::GetInstance().GetTextureManager();

    camera.Set(0.0f, 0.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

    shaderManager->LoadProgram("floorShader", "floorShader.xml");
    shaderManager->LoadProgram("particle", "particles.xml");
    shaderManager->LoadProgram("update_points", "particles.xml");

    floor.Initialize(64.f, 4.0f, shaderManager->GetProgram("floorShader"));
    //floor.SetColor(Math::vec4f(0.803f, 0.584f, 0.047f, 1.0f));

    // Points for accessing the texel, which contains the particle data
    float points[particleTextureWidth * particleTextureWidth * 2];

    // Array containing the initial particles
    float particles[particleTextureWidth * particleTextureWidth * 4];

    for (int y = 0; y < particleTextureWidth; ++y)
    {
        for (int x = 0; x < particleTextureWidth; ++x)
        {
            particles[x * 4 + 0 + y * particleTextureWidth * 4] = 0.0f;
            particles[x * 4 + 1 + y * particleTextureWidth * 4] = 0.0f;
            particles[x * 4 + 2 + y * particleTextureWidth * 4] = 0.0f;
            particles[x * 4 + 3 + y * particleTextureWidth * 4] = -1.0f; // w < 0.0 means a dead particle. So renewed in first frame
        }
    }

    pugi::xml_document textureInfo;
    pugi::xml_parse_result textureResult = textureInfo.load_file("data/configs/gpuParticlesExample.xml");

    for (pugi::xml_node currentTexture = textureInfo.child("Texture2D"); currentTexture != NULL; currentTexture = currentTexture.next_sibling())
        textureManager->LoadTexture(currentTexture);

    glGenTextures(2, positionTexture);

    glBindTexture(GL_TEXTURE_2D, positionTexture[0]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, particleTextureWidth, particleTextureWidth, 0, GL_RGBA, GL_FLOAT, particles);

    // No interpolation
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


    glBindTexture(GL_TEXTURE_2D, positionTexture[1]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, particleTextureWidth, particleTextureWidth, 0, GL_RGBA, GL_FLOAT, particles);

    // No interpolation
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBindTexture(GL_TEXTURE_2D, 0);

    // Two framebuffers
    glGenFramebuffers(2, positionFramebuffer);

    // Attach the texture
    glBindFramebuffer(GL_FRAMEBUFFER, positionFramebuffer[0]);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, positionTexture[0], 0);

    glBindFramebuffer(GL_FRAMEBUFFER, positionFramebuffer[1]);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, positionTexture[1], 0);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glGenBuffers(1, &verticesVBO);
    glBindBuffer(GL_ARRAY_BUFFER, verticesVBO);
    glBufferData(GL_ARRAY_BUFFER, particleTextureWidth * particleTextureWidth * 2 * sizeof (float), (float*) points, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    particleShader = shaderManager->GetProgram("particle");
    updatePointShader = shaderManager->GetProgram("update_points");

    shader = shaderManager->GetProgram("particle");

    shader->BindShader();

    textureManager->GetTexture("particle")->Activate(0);
    shader->SendUniform("u_texture", 0);

    glActiveTexture(GL_TEXTURE1);
    shader->SendUniform("u_positionTexture", 1);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, verticesVBO);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    shader = shaderManager->GetProgram("update_points");
    shader->BindShader();

    shader->SendUniform("u_positionTexture", 1);
    shader->SendUniform("u_positionTextureWidth", (float) particleTextureWidth);

    glGenVertexArrays(1, &vaoUpdatePoints);
    glBindVertexArray(vaoUpdatePoints);

    glBindBuffer(GL_ARRAY_BUFFER, verticesVBO);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    //glEnable(GL_DEPTH_TEST);
    //glEnable(GL_CULL_FACE);

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    // Use gl_PointSize in the shaders
    glEnable(GL_PROGRAM_POINT_SIZE);

    // Formula to blend the particles
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);

    return true;
}

void GPUParticles::Prepare(float dt)
{
    camera.Update(dt);

    updatePointShader->BindShader();
    updatePointShader->SendUniform("u_time", dt);

    // Bind the writing buffer
    glBindFramebuffer(GL_FRAMEBUFFER, positionFramebuffer[(flip + 1) % 2]);

    // Set the viewport to the texture size
    glViewport(0, 0, particleTextureWidth, particleTextureWidth);

    // This texture contains the current particle data
    glBindTexture(GL_TEXTURE_2D, positionTexture[flip]);

    glBindVertexArray(vaoUpdatePoints);

    // Draw all particles, data is updated in the shader
    glDrawArrays(GL_POINTS, 0, particleTextureWidth * particleTextureWidth);

    // Go back to the window buffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // Reset the viewport
    glViewport(0, 0, window->GetWidth(), window->GetHeight());
}

void GPUParticles::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //glClearColor(0.9f, 0.95f, 1.0f, 1.0f); 
    Rendering::Renderer::GetInstance().GetMatrixStack().Identity();

    camera.Look();
    floor.Render();

    particleShader->BindShader();

    particleShader->SendUniform4x4("u_modelviewProjectionMatrix",
                                   Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(MODELVIEW_MATRIX) *
                                   Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(PROJECTION_MATRIX));

    glBindVertexArray(vao);

    // Use the particle data from current frame. Updated data is used in next frame
    glBindTexture(GL_TEXTURE_2D, positionTexture[flip]);

    // Draw point sprites by blending them
    glEnable(GL_BLEND);
    glDrawArrays(GL_POINTS, 0, particleTextureWidth * particleTextureWidth);
    glDisable(GL_BLEND);

    // Switch to the next buffer
    flip = (flip + 1) % 2;
}

void GPUParticles::Shutdown() { }

void GPUParticles::OnKeyPressed(NGE::Events::KeyboardEvent& event)
{
    if (event.GetAction() == Events::PRESSED)
    {
        switch (event.GetKeyId())
        {
            case Events::KEY_Z:
                if (camera.IsMouseLocked())
                    window->EnableMouseCursor(true);

                else
                    window->EnableMouseCursor(false);
                camera.LockMouse(!camera.IsMouseLocked());
                break;
        }
    }

    camera.SetKeyboardInput(event);
}

void GPUParticles::OnMouse(NGE::Events::MouseEvent& event)
{

    camera.SetMouseInfo(event.GetX(), event.GetY());
}

void GPUParticles::OnResize(int width, int height)
{
    Application::OnResize(width, height);
}

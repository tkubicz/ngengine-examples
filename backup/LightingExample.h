/* 
 * File:   LightingExample.h
 * Author: tku
 *
 * Created on 23 czerwiec 2013, 15:15
 */

#ifndef LIGHTINGEXAMPLE_H
#define	LIGHTINGEXAMPLE_H

#include "Windows/Windows.h"
#include "Events/GUIEventListener.h"
#include "Events/KeyboardEvent.h"
#include "Events/MouseEvent.h"
#include "GUI/GUIEvent.h"
#include "GUI/GUIFrame.h"
#include "Geometry/Basic/Floor.h"
#include "Geometry/Basic/Sphere.h"
#include "Rendering/Rendering.h"
#include "Media/Media.h"
#include "Geometry/Nature/Terrain.h"
#include "Geometry/Models/ObjModel.h"
#include "Appearance/Light.h"
#include "Appearance/Material.h"
#include "Geometry/Nature/SkyPlane.h"
#include "Geometry/Nature/SkyDome.h"
#include "Geometry/Skeleton/Skeleton.h"
#include "Geometry/Skeleton/SkinnedMesh.h"
#include <vector>

namespace NGE
{
    namespace Examples
    {   
        class LightingExample : public Windows::Application, public Events::GUIEventListener
        {
          public:
            LightingExample();
            virtual ~LightingExample();
            
            bool Init();
            void Prepare(float dt);
            void Render();
            void Shutdown();
            
            void OnKeyPressed(Events::KeyboardEvent& event);
            void OnMouse(Events::MouseEvent& event);
            void ActionPerformed(GUI::GUIEvent& event);
            
            void OnResize(int width, int height);
            
          private:
            GUI::GUIFrame* gui;
            
            Geometry::Basic::Floor floor;
            Geometry::Nature::Terrain terrain;
            Geometry::Nature::SkyPlane skyPlane;
            Geometry::Nature::SkyDome skyDome;
            
            Geometry::Skeleton::Skeleton* skeleton;
            Geometry::Skeleton::SkinnedMesh* mesh;
            
            float latitude, longitude, time, day, turbidity;
            
            Geometry::Basic::Sphere sphere;
            Math::vec3f spherePosition;
            
            float rotationAngle, rotationSpeed;
            Rendering::Camera::Camera camera;
        };
    }
}

#endif	/* LIGHTINGEXAMPLE_H */


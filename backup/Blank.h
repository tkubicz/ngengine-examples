/* 
 * File:   Blank.h
 * Author: tku
 *
 * Created on 15 kwiecień 2013, 15:44
 */

#ifndef BLANK_H
#define	BLANK_H

#include "Windows/Application.h"
#include "Events/GUIEventListener.h"
#include "Events/MouseEvent.h"
#include "Events/KeyboardEvent.h"
#include "GUI/GUIFrame.h"
#include "GUI/GUIEvent.h"

namespace NGE
{
    namespace Examples
    {

        class Blank : public Windows::Application, public Events::GUIEventListener
        {
          public:
            Blank();
            virtual ~Blank();

            bool Init();
            void Prepare(float dt);
            void Render();
            void Shutdown();

            void OnKeyPressed(Events::KeyboardEvent& event);
            void OnMouse(Events::MouseEvent& event);

            void OnResize(int width, int height);

            void ActionPerformed(GUI::GUIEvent& event);

          private:
            GUI::GUIFrame* gui;
        };
    }
}

#endif	/* BLANK_H */


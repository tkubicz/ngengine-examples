/* 
 * File:   Flightism.h
 * Author: tku
 *
 * Created on 9 styczeń 2014, 02:54
 */

#ifndef FLIGHTISM_H
#define	FLIGHTISM_H

#include "Windows/Application.h"
#include "Events/Events.h"
#include "Rendering/Rendering.h"
#include "Physics/RigidBody/ForceGenerator.h"
#include "Physics/RigidBody/RigidBody.h"
#include "Geometry/Basic/Floor.h"
#include "Geometry/Basic/Box.h"

namespace NGE
{
    namespace Examples
    {
        class FlightismExample : public Windows::Application
        {
          public:
            FlightismExample();
            virtual ~FlightismExample();
            
            bool Init();
            void Prepare(float dt);
            void Render();
            void Shutdown();
            
            void OnKeyPressed(Events::KeyboardEvent& event);
            void OnMouse(Events::MouseEvent& event);
            
            //void OnResize(int width, int height);
            
          protected:
            Rendering::Camera::Camera camera;
            Geometry::Basic::Floor floor;
            Geometry::Basic::Box box;
            
            Physics::RigidBody::AeroControl* leftWing;
            Physics::RigidBody::AeroControl* rightWing;
            Physics::RigidBody::AeroControl* rudder;
            Physics::RigidBody::Aero* tail;
            Physics::RigidBody::RigidBody aircraft;
            Physics::RigidBody::ForceRegistry registry;
            
            Math::vec3f windspeed;
            float leftWingControl;
            float rightWingControl;
            float rudderControl;
            
            void ResetPlane();
            void RenderPlane();
        };
    }
}

#endif	/* FLIGHTISM_H */


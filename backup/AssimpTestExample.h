/* 
 * File:   AssimpTestExample.h
 * Author: tku
 *
 * Created on 17 kwiecień 2014, 12:19
 */

#ifndef ASSIMPTESTEXAMPLE_H
#define	ASSIMPTESTEXAMPLE_H

#include <assimp/scene.h>
#include <vector>

#include "Windows/Windows.h"
#include "Math/Vector3.h"
#include "Math/Vector2.h"
#include "Events/KeyboardEvent.h"
#include "Events/MouseEvent.h"
#include "Media/Images/Texture.h"
#include "Geometry/Basic/Floor.h"
#include "Rendering/Camera/Camera.h"

namespace NGE
{
    namespace Examples
    {

        class Vertex
        {
          public:
            Math::vec3f position;
            Math::vec2f texCoords;
            Math::vec3f normal;

            Vertex() { }

            Vertex(const Math::vec3f& pos, const Math::vec2f& tex, const Math::vec3f& normal)
            {
                position = pos;
                texCoords = tex;
                this->normal = normal;
            }
        };

        class Mesh
        {
          public:
            Mesh();
            ~Mesh();

            bool LoadMesh(const std::string& filename);
            void Render();
            void SetShader(Media::Shaders::GLSLProgram* shader);

          private:
            bool InitFromScene(const aiScene* scene, const std::string& filename);
            void InitMesh(unsigned int index, const aiMesh* mesh);
            bool InitMaterials(const aiScene* scene, const std::string& filename);
            void Clear();

#define INVALID_MATERIAL 0xFFFFFFFF

            struct MeshEntry
            {
                MeshEntry();
                ~MeshEntry();

                //void Init(const std::vector<Vertex>& vertices, const std::vector<unsigned int>& indices);
                void Init(const std::vector<Math::vec3f>& vertices, const std::vector<unsigned int>& indices);

                GLuint vb;
                GLuint ib;
                GLuint vao;

                unsigned int numIndices;
                unsigned int materialIndex;
            };

            std::vector<MeshEntry> entries;
            std::vector<Media::Images::Texture*> textures;
            Media::Shaders::GLSLProgram* shader;
        };

        class AssimpTestExample : public Windows::Application
        {
          public:
            AssimpTestExample();
            virtual ~AssimpTestExample();

            virtual bool Init();
            virtual void Prepare(float dt);
            virtual void Render();
            virtual void Shutdown();

            virtual void OnKeyPressed(Events::KeyboardEvent& event);
            virtual void OnMouse(Events::MouseEvent& event);

          private:
            Geometry::Basic::Floor* floor;
            Rendering::Camera::Camera* camera;
            
            Mesh* mesh;

            // Shortcuts
            NGE::Media::Shaders::ShaderManager* shaderManager;
            NGE::Media::Images::TextureManager* textureManager;
        };
    }
}

#endif	/* ASSIMPTESTEXAMPLE_H */


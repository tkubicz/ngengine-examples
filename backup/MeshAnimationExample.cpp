#include "Examples/MeshAnimationExample.h"
using namespace NGE::Examples;

MeshAnimationExample::MeshAnimationExample() { }

MeshAnimationExample::~MeshAnimationExample() { }

bool MeshAnimationExample::Init()
{
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    
    return true;
}

void MeshAnimationExample::Prepare(float dt) { }

void MeshAnimationExample::Render()
{
    Rendering::Renderer::GetInstance().ClearBuffers();
    Rendering::Renderer::GetInstance().ClearColor(0.9f, 0.95f, 1.f, 1.f);
    Rendering::Renderer::GetInstance().GetMatrixStack().Identity();
}

void MeshAnimationExample::Shutdown() { }

void MeshAnimationExample::OnMouse(Events::MouseEvent& event) { }

void MeshAnimationExample::OnKeyPressed(Events::KeyboardEvent& event) { }
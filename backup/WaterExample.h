/* 
 * File:   WaterExample.h
 * Author: tku
 *
 * Created on 21 październik 2013, 15:36
 */

#ifndef WATEREXAMPLE_H
#define	WATEREXAMPLE_H

#include "Windows/Windows.h"
#include "Events/Events.h"
#include "Geometry/Basic/Floor.h"
#include "Geometry/Nature/WaterTexture.h"
#include "Rendering/Rendering.h"
#include "Media/Images/Texture.h"

namespace NGE
{
    namespace Examples
    {        
        class WaterExample : public Windows::Application
        {
          public:
            WaterExample();
            virtual ~WaterExample();

            bool Init();
            void Prepare(float dt);
            void Render();
            void Shutdown();

            void OnKeyPressed(Events::KeyboardEvent& event);
            void OnMouse(Events::MouseEvent& event);

            void OnResize(int width, int height);
            
          private:
            void InverseRigidBody(Math::mat4f& mat);
            
            void RenderWater();
            
            static const int WATER_PLANE_LENGTH;
            static const int NUMBERWAVES;
            
            int waterTextureId;
            
            Media::Shaders::GLSLProgram* waterShader;
            Media::Images::Texture* waterCubeMap;
            
            GLuint verticesVBO, indicesVBO, VAO;
            
            Rendering::Camera::Camera camera;
            Geometry::Basic::Floor floor;
            Geometry::Nature::WaterTexture waterTexture;
            
            Math::mat3f inverseViewNormalMatrix;
            Math::mat4f viewMatrix;
            float passedTime;
        };
    }
}

#endif	/* WATEREXAMPLE_H */


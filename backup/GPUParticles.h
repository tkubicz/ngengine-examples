/* 
 * File:   GPUParticles.h
 * Author: tku
 *
 * Created on 7 grudzień 2013, 04:02
 */

#ifndef GPUPARTICLES_H
#define	GPUPARTICLES_H

#include "Windows/Application.h"
#include "Events/Events.h"
#include "Rendering/Rendering.h"
#include "Geometry/Basic/Floor.h"

namespace NGE
{
    namespace Examples
    {

        class GPUParticles : public Windows::Application
        {
          public:
            GPUParticles();
            ~GPUParticles();

            bool Init();
            void Prepare(float dt);
            void Render();
            void Shutdown();

            void OnKeyPressed(Events::KeyboardEvent& event);
            void OnMouse(Events::MouseEvent& event);

            void OnResize(int width, int height);

          private:
            Rendering::Camera::Camera camera;
            Geometry::Basic::Floor floor;

            Media::Shaders::GLSLProgram* shader;
            Media::Shaders::GLSLProgram* particleShader;
            Media::Shaders::GLSLProgram* updatePointShader;

            GLuint verticesVBO, vaoUpdatePoints, vao;
            GLuint positionFramebuffer[2];
            GLuint positionTexture[2];

            GLint flip;
            
            const static int particleTextureWidth = 16;
        };
    }
}

#endif	/* GPUPARTICLES_H */


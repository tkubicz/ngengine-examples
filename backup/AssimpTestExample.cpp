#include "Examples/AssimpTestExample.h"
#include "Rendering/Renderer.h"
#include "assimp/Importer.hpp"
#include "assimp/postprocess.h"
#include "Appearance/Scene/Scene.h"
using namespace NGE::Examples;

Mesh::MeshEntry::MeshEntry()
{
    vb = ib = 0;
    numIndices = 0;
    materialIndex = INVALID_MATERIAL;
}

Mesh::MeshEntry::~MeshEntry()
{
    if (vb != 0)
        glDeleteBuffers(1, &vb);
    if (ib != 0)
        glDeleteBuffers(1, &ib);
}

//void Mesh::MeshEntry::Init(const std::vector<Vertex>& vertices, const std::vector<unsigned int>& indices)
void Mesh::MeshEntry::Init(const std::vector<Math::vec3f>& vertices, const std::vector<unsigned int>& indices)
{
    numIndices = indices.size();

    glGenBuffers(1, &vb);
    glBindBuffer(GL_ARRAY_BUFFER, vb);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * vertices.size(), &vertices[0], GL_STATIC_DRAW);

    glGenBuffers(1, &ib);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof (unsigned int) * numIndices, &indices[0], GL_STATIC_DRAW);
    
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    
    glBindBuffer(GL_ARRAY_BUFFER, vb);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
    
    glBindVertexArray(0);
}

Mesh::Mesh() { }

Mesh::~Mesh()
{
    Clear();
}

void Mesh::Clear()
{
    for (unsigned int i = 0; i < textures.size(); ++i)
    {
        delete textures[i];
    }
}

bool Mesh::LoadMesh(const std::string& filename)
{
    // Release the previously loaded mesh (if it exists)
    Clear();

    bool Ret = false;
    Assimp::Importer importer;

    const aiScene* scene = importer.ReadFile(filename.c_str(), aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs);

    if (scene)
    {
        Ret = InitFromScene(scene, filename);
    }
    else
    {
        log_error("Error parsing: " + to_string(importer.GetErrorString()));
    }

    return Ret;
}

bool Mesh::InitFromScene(const aiScene* scene, const std::string& filename)
{
    entries.resize(scene->mNumMeshes);
    textures.resize(scene->mNumMaterials);

    // Initialize the meshes in the scene one by one
    for (unsigned int i = 0; i < entries.size(); ++i)
    {
        const aiMesh* mesh = scene->mMeshes[i];
        InitMesh(i, mesh);
    }

    return InitMaterials(scene, filename);
}

void Mesh::InitMesh(unsigned int index, const aiMesh* mesh)
{
    entries[index].materialIndex = mesh->mMaterialIndex;

    //std::vector<Vertex> vertices;
    std::vector<Math::vec3f> vertices;
    std::vector<unsigned int> indices;

    const aiVector3D zero3D(0.0f, 0.0f, 0.0f);

    for (unsigned int i = 0; i < mesh->mNumVertices; ++i)
    {
        const aiVector3D* pos = &(mesh->mVertices[i]);
        const aiVector3D* normal = &(mesh->mNormals[i]);
        const aiVector3D* texCoord = mesh->HasTextureCoords(0) ? &(mesh->mTextureCoords[0][i]) : &zero3D;

        /*Vertex v(Math::vec3f(pos->x, pos->y, pos->z),
                 Math::vec2f(texCoord->x, texCoord->y),
                 Math::vec3f(normal->x, normal->y, normal->z));*/
        Math::vec3f v(pos->x, pos->y, pos->z);

        vertices.push_back(v);
    }

    for (unsigned int i = 0; i < mesh->mNumFaces; ++i)
    {
        const aiFace& face = mesh->mFaces[i];
        assert(face.mNumIndices == 3);
        indices.push_back(face.mIndices[0]);
        indices.push_back(face.mIndices[1]);
        indices.push_back(face.mIndices[2]);
    }

    entries[index].Init(vertices, indices);
}

bool Mesh::InitMaterials(const aiScene* scene, const std::string& filename)
{
    // Extract the directory part from the file name
    std::string::size_type slashIndex = filename.find_last_of("/");
    std::string dir;

    if (slashIndex == std::string::npos)
        dir = ".";
    else if (slashIndex == 0)
        dir = "/";
    else
        dir = filename.substr(0, slashIndex);

    bool ret = true;

    // Initialize the materials
    for (unsigned int i = 0; i < scene->mNumMaterials; ++i)
    {
        const aiMaterial* material = scene->mMaterials[i];

        textures[i] = NULL;

        if (material->GetTextureCount(aiTextureType_DIFFUSE) > 0)
        {
            aiString path;

            if (material->GetTexture(aiTextureType_DIFFUSE, 0, &path, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS)
            {
                std::string fullPath = dir + "/" + path.data;
                // TODO: Naprawić to -> poprawić wczytywanie tekstur.
                //textures[i] = new Media::Images::Texture();
                //textures[i]->Load2DImage()
            }
        }

        // Load a white texture in case the model does not include its own texture
        if (textures[i])
        {
            // TODO: Fix that.
        }
    }

    return ret;
}

void Mesh::Render()
{
    /*glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    for (unsigned int i = 0; i < entries.size(); ++i)
    {
        glBindBuffer(GL_ARRAY_BUFFER, entries[i].vb);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof (Vertex), 0);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof (Vertex), (const GLvoid*) 12);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof (Vertex), (const GLvoid*) 20);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, entries[i].ib);

        const unsigned int materialIndex = entries[i].materialIndex;

        if (materialIndex < textures.size() && textures[materialIndex])
        {
            // TODO: Fix that
        }

        glDrawElements(GL_TRIANGLES, entries[i].numIndices, GL_UNSIGNED_INT, 0);
    }

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);*/
    
    shader->BindShader();
    
    shader->SendUniform4x4("modelview_matrix", Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(MODELVIEW_MATRIX));
    shader->SendUniform4x4("projection_matrix", Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(PROJECTION_MATRIX));
    shader->SendUniform("color", 1.0, 0.0, 0.0, 1.0);
    
    for (unsigned int i = 0; i < entries.size(); ++i)
    {
        glBindVertexArray(entries[i].vao);
        glDrawElements(GL_TRIANGLES, entries[i].numIndices, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);
    }
    
    shader->UnbindShader();
    
    
}

void Mesh::SetShader(Media::Shaders::GLSLProgram* shader)
{
    this->shader = shader;
}

AssimpTestExample::AssimpTestExample() { }

AssimpTestExample::~AssimpTestExample()
{
    delete camera;
    delete floor;
}

bool AssimpTestExample::Init()
{
    shaderManager = &NGE::Media::MediaManager::GetInstance().GetShaderManager();
    textureManager = &NGE::Media::MediaManager::GetInstance().GetTextureManager();

    // Load shaders
    shaderManager->LoadProgram("floorShader", "floorShader.xml");

    camera = new Rendering::Camera::Camera(0.0f, 10.0f, 30.0f, 0.0f, 0.0f, 0.0f);

    floor = new Geometry::Basic::Floor();
    floor->Initialize(200.f, 4.f, shaderManager->GetProgram("floorShader"));
    floor->SetColor(Math::vec4f(131.f / 255.f, 139.f / 255.f, 131.f / 255.f, 1.0f));
    
    mesh = new Mesh();
    mesh->SetShader(shaderManager->GetProgram("floorShader"));
    //if (!mesh->LoadMesh("data/models/md2/phoenix_ugv.md2"))
    if (!mesh->LoadMesh("data/models/x/soldier.X"))
        log_error("Could not load mesh!");

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    return true;
}

void AssimpTestExample::Prepare(float dt)
{
    camera->Update(dt);
}

void AssimpTestExample::Render()
{
    Rendering::Renderer::GetInstance().ClearBuffers();
    Rendering::Renderer::GetInstance().ClearColor(0.9f, 0.95f, 1.0f, 1.0f);

    matrixStack.Identity();

    camera->Look();
    floor->Render();
    
    mesh->Render();
}

void AssimpTestExample::Shutdown() { }

void AssimpTestExample::OnKeyPressed(Events::KeyboardEvent& event)
{
    if (event.GetAction() == Events::PRESSED)
    {
        switch (event.GetKeyId())
        {
            case Events::KEY_Z:
                if (camera->IsMouseLocked())
                    window->EnableMouseCursor(true);
                else
                    window->EnableMouseCursor(false);
                camera->LockMouse(!camera->IsMouseLocked());
                break;
        }
    }

    camera->SetKeyboardInput(event);
}

void AssimpTestExample::OnMouse(Events::MouseEvent& event)
{
    camera->SetMouseInfo(event.GetX(), event.GetY());
}






#include "Examples/BigBallisticExample.h"
#include "Media/MediaManager.h"
#include "Physics/RigidBody/CollisionPlane.h"
using namespace NGE::Examples;

BigBallisticExample::BigBallisticExample() : resolver(maxContacts * 8)
{
    currentShotType = LASER;
    cData.contactArray = contacts;

    pause = false;
    simulationSpeed = 1.0f;
}

BigBallisticExample::~BigBallisticExample() { }

bool BigBallisticExample::Init()
{
    // Shortcuts
    Media::Shaders::ShaderManager* shaderManager = &Media::MediaManager::GetInstance().GetShaderManager();
    Media::Images::TextureManager* textureManager = &Media::MediaManager::GetInstance().GetTextureManager();

    // Load shaders
    shaderManager->LoadProgram("floorShader", "floorShader.xml");
    shaderManager->LoadProgram("colorShader", "colorShader.xml");
    shaderManager->LoadProgram("basicShader", "shaders.xml");

    // Load textures
    pugi::xml_document textureInfo;
    pugi::xml_parse_result textureResult = textureInfo.load_file("data/configs/textures.xml");
    pugi::xml_node currentTexture = textureInfo.child("Texture2D");
    textureManager->GetTexture(currentTexture);

    camera.Set(-35.0f, 10.0f, 0.0f, 50.0f, 10.0f, 22.0f, 0.0f, 1.0f, 0.0f);

    floor.Initialize(256.f, 4.0f, shaderManager->GetProgram("floorShader"));
    floor.SetColor(Math::vec4f(0.0f, 0.6f, 0.2f, 1.0f));

    box.Initialize(1.0f);
    box.SetShader(shaderManager->GetProgram("colorShader"));

    sphere.Initialize(20, 20, 1.0f, shaderManager->GetProgram("basicShader"));
    sphere.SetTexture(textureManager->GetTexture("wood"));

    Reset();

    testAmmo1.SetState(ARTILLERY);
    testAmmo1.body->SetAcceleration(0.f, -9.81f, 0.f);
    testAmmo1.body->SetVelocity(0, 0, 0);
    testAmmo1.body->SetPosition(0, 30, 0);
    testAmmo1.body->CalculateDerivedData();
    testAmmo1.body->SetCanSleep(true);
    testAmmo1.CalculateInternals();

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    return true;
}

void BigBallisticExample::Prepare(float dt)
{
    camera.Update(dt);

    dt = dt * simulationSpeed;

    if (!pause)
    {
        testAmmo1.body->Integrate(dt);
        testAmmo1.CalculateInternals();

        for (int i = 0; i < 2; ++i)
        {
            testSpheres1[i].body->Integrate(dt);
            testSpheres1[i].CalculateInternals();;
        }


        UpdateObjects(dt);
        GenerateContacts();
        resolver.ResolveContacts(cData.contactArray, cData.contactCount, dt);
    }
}

void BigBallisticExample::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.9f, 0.95f, 1.0f, 1.0f);
    Rendering::Renderer::GetInstance().GetMatrixStack().Identity();

    camera.Look();
    floor.Render();

    for (Box* box = boxData; box < boxData + boxes; box++)
    {
        box->Render(&this->box);
    }

    for (AmmoRound* shot = ammo; shot < ammo + ammoRounds; shot++)
    {
        if (shot->type != UNUSED)
            shot->Render(&this->sphere);
    }

    for (int i = 0; i < 2; ++i)
    {
        testSpheres1[i].Render(&this->sphere);
    }
}

void BigBallisticExample::Shutdown() { }

void BigBallisticExample::OnKeyPressed(NGE::Events::KeyboardEvent& event)
{
    if (event.GetAction() == Events::PRESSED)
    {
        switch (event.GetKeyId())
        {
            case Events::KEY_Z:
                if (camera.IsMouseLocked())
                    window->EnableMouseCursor(true);
                else
                    window->EnableMouseCursor(false);
                camera.LockMouse(!camera.IsMouseLocked());
                break;

            case Events::KEY_R:
                Reset();
                break;

            case Events::KEY_ONE:
                currentShotType = PISTOL;
                break;

            case Events::KEY_TWO:
                currentShotType = ARTILLERY;
                break;

            case Events::KEY_THREE:
                currentShotType = FIREBALL;
                break;

            case Events::KEY_FOUR:
                currentShotType = LASER;
                break;

            case Events::KEY_SPACE:
                pause = !pause;
                break;

            case Events::KEY_UP:
                simulationSpeed += 0.1f;
                break;

            case Events::KEY_DOWN:
                simulationSpeed -= 0.1f;
                break;

            case Events::KEY_NUM8:
                testAmmo1.body->AddVelocity(Math::vec3f(0, 0, 10));
                break;
        }
    }

    camera.SetKeyboardInput(event);
}

void BigBallisticExample::OnMouse(NGE::Events::MouseEvent& event)
{
    camera.SetMouseInfo(event.GetX(), event.GetY());

    if (event.GetAction() == Events::PRESSED && event.GetButtonId() == Events::BUTTON1)
    {
        Fire();
        testCollision();
    }
}

void BigBallisticExample::Reset()
{
    for (AmmoRound* shot = ammo; shot < ammo + ammoRounds; shot++)
        shot->type = UNUSED;

    float z = 20.0f;
    for (Box* box = boxData; box < boxData + boxes; box++)
    {
        box->SetState(z);
        z += 90.0f;
    }

    for (int i = 0; i < 2; ++i)
    {
        testSpheres1[i].SetState(ARTILLERY);
        testSpheres1[i].body->SetAcceleration(0.f, -9.81f, 0.f);
        testSpheres1[i].body->SetVelocity(0, 0, 0);
        testSpheres1[i].body->SetPosition(i * 20.f, 10.f, 0.f);
        testSpheres1[i].body->CalculateDerivedData();
        testSpheres1[i].body->SetCanSleep(false);
        testSpheres1[i].CalculateInternals();
    }
}

void BigBallisticExample::Fire()
{
    AmmoRound* shot;
    for (shot = ammo; shot < ammo + ammoRounds; shot++)
        if (shot->type == UNUSED)
            break;

    if (shot >= ammo + ammoRounds)
        return;

    shot->SetState(currentShotType);
}

void BigBallisticExample::UpdateObjects(float dt)
{
    for (AmmoRound* shot = ammo; shot < ammo + ammoRounds; shot++)
    {
        if (shot->type != UNUSED)
        {
            shot->body->Integrate(dt);
            shot->CalculateInternals();

            if (shot->body->GetPosition().z > 200.0f)
            {
                shot->type = UNUSED;
            }
        }
    }

    for (Box* box = boxData; box < boxData + boxes; box++)
    {
        box->body->Integrate(dt);
        box->CalculateInternals();
    }
}

void BigBallisticExample::GenerateContacts()
{
    Physics::RigidBody::CollisionPlane plane;
    plane.direction = Math::vec3f(0, 1, 0);
    plane.offset = 0;

    cData.Reset(maxContacts);
    cData.friction = 0.9f;
    cData.restitution = 0.1f;
    cData.tolerance = 0.1f;

    for (Box* box = boxData; box < boxData + boxes; box++)
    {
        if (!cData.HasMoreContacts())
            return;

        Physics::RigidBody::CollisionDetector::BoxAndHalfSpace(*box, plane, &cData);

        for (AmmoRound* shot = ammo; shot < ammo + ammoRounds; shot++)
        {
            if (shot->type != UNUSED)
            {
                if (!cData.HasMoreContacts()) return;

                if (Physics::RigidBody::CollisionDetector::BoxAndSphere(*box, *shot, &cData))
                {
                    //    shot->type = UNUSED;
                }

                Physics::RigidBody::CollisionDetector::SphereAndHalfSpace(*shot, plane, &cData);
            }
        }
    }

    Physics::RigidBody::CollisionDetector::SphereAndHalfSpace(testAmmo1, plane, &cData);

    for (int i = 0; i < 2; ++i)
    {
        Physics::RigidBody::CollisionDetector::SphereAndHalfSpace(testSpheres1[i], plane, &cData);
    }

    Physics::RigidBody::CollisionDetector::SphereAndSphere(testSpheres1[0], testSpheres1[1], &cData);
}

void BigBallisticExample::testCollision()
{
    testSpheres1[0].body->SetVelocity(10, 0, 0);
    testSpheres1[1].body->SetVelocity(-10, 0, 0);
}


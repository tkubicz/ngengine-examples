/* 
 * File:   BigBallisticExample.h
 * Author: tku
 *
 * Created on 15 styczeń 2014, 13:08
 */

#ifndef BIGBALLISTICEXAMPLE_H
#define	BIGBALLISTICEXAMPLE_H

#include "Windows/Windows.h"
#include "Events/Events.h"
#include "Physics/RigidBody/CollisionBox.h"
#include "Physics/RigidBody/CollisionSphere.h"
#include "Physics/RigidBody/CollisionDetector.h"
#include "Physics/RigidBody/Contact.h"
#include "Physics/RigidBody/ContactResolver.h"
#include "Physics/RigidBody/BVHNode.h"
#include "Physics/RigidBody/BoundingSphere.h"
#include "Rendering/Camera/Camera.h"
#include "Rendering/Renderer.h"
#include "Geometry/Basic/Floor.h"
#include "Geometry/Basic/Box.h"
#include "Geometry/Basic/Sphere.h"

namespace NGE
{
    namespace Examples
    {

        enum ShotType
        {
            UNUSED = 0,
            PISTOL,
            ARTILLERY,
            FIREBALL,
            LASER
        };

        class AmmoRound : public Physics::RigidBody::CollisionSphere
        {
          public:
            ShotType type;
            unsigned startTime;

            AmmoRound()
            {
                body = new Physics::RigidBody::RigidBody();
            }

            ~AmmoRound()
            {
                delete body;
            }

            void SetState(ShotType shotType)
            {
                type = shotType;

                switch (type)
                {
                    case PISTOL:
                        body->SetMass(1.5f);
                        body->SetVelocity(0.0f, 0.0f, 20.0f);
                        body->SetAcceleration(0.0f, -0.5f, 0.0f);
                        body->SetDamping(0.99f, 0.8f);
                        radius = 0.2f;
                        break;

                    case ARTILLERY:
                        body->SetMass(20.0f); // 200.0 kg
                        body->SetVelocity(0.0f, 30.0f, 40.0f);
                        body->SetAcceleration(0.0f, -21.0f, 0.0f);
                        body->SetDamping(0.99f, 0.8f);
                        radius = 0.4f;
                        break;

                    case FIREBALL:
                        body->SetMass(4.0f);
                        body->SetVelocity(0.0f, -0.5f, 10.0f); // 10 m/s
                        body->SetAcceleration(0.0f, 0.3f, 0.0f); // Unosi się
                        body->SetDamping(0.9f, 0.8f);
                        radius = 0.6f;
                        break;

                    case LASER:
                        body->SetMass(0.1f);
                        body->SetVelocity(0.0f, 0.0f, 100.0f);
                        body->SetAcceleration(0.0f, 0.0f, 0.0f);
                        body->SetDamping(0.99f, 0.8f);
                        radius = 0.2f;
                        break;
                }

                body->SetCanSleep(false);
                body->SetAwake();

                Math::mat3f tensor;
                float coeff = 0.4f * body->GetMass() * radius * radius;
                tensor.SetInertiaTensorCoeffs(coeff, coeff, coeff);
                body->SetInertiaTensor(tensor);

                body->SetPosition(0.0f, 1.5f, 0.0f);
                //startTime = 

                body->CalculateDerivedData();
                CalculateInternals();
            }

            void Render(Geometry::Basic::Sphere* sphere)
            {
                Rendering::Renderer::GetInstance().GetMatrixStack().PushMatrix();
                Rendering::Renderer::GetInstance().GetMatrixStack().Multiple(body->GetTransform());
                matrixStack.Scale(radius, radius, radius);
                sphere->Render();
                Rendering::Renderer::GetInstance().GetMatrixStack().PopMatrix();
            }
        };

        class Box : public Physics::RigidBody::CollisionBox
        {
          public:

            Box()
            {
                body = new Physics::RigidBody::RigidBody();
            }

            ~Box()
            {
                delete body;
            }

            void SetState(float z)
            {
                body->SetPosition(0, 3, z);
                body->SetOrientation(1, 0, 0, 0);
                body->SetVelocity(0, 0, 0);
                body->SetRotation(Math::vec3f(0, 0, 0));
                halfSize = Math::vec3f(1, 1, 1);

                float mass = halfSize.x * halfSize.y * halfSize.z * 8.0f;
                body->SetMass(mass);

                Math::mat3f tensor;
                tensor.SetBlockInertiaTensor(halfSize, mass);
                body->SetInertiaTensor(tensor);

                body->SetLinearDamping(0.95f);
                body->SetAngularDamping(0.8f);
                body->ClearAccumulators();
                body->SetAcceleration(0, -10.0f, 0);

                body->SetCanSleep(false);
                body->SetAwake();

                body->CalculateDerivedData();
                CalculateInternals();
            }

            void Render(Geometry::Basic::Box* box)
            {
                Rendering::Renderer::GetInstance().GetMatrixStack().PushMatrix();
                Rendering::Renderer::GetInstance().GetMatrixStack().Multiple(body->GetTransform());
                Rendering::Renderer::GetInstance().GetMatrixStack().Scale(halfSize.x * 2, halfSize.y * 2, halfSize.z * 2);
                box->Render();
                Rendering::Renderer::GetInstance().GetMatrixStack().PopMatrix();
            }
        };

        class BigBallisticExample : public Windows::Application
        {
          public:
            BigBallisticExample();
            virtual ~BigBallisticExample();

            bool Init();
            void Prepare(float dt);
            void Render();
            void Shutdown();

            void OnKeyPressed(Events::KeyboardEvent& event);
            void OnMouse(Events::MouseEvent& event);

          private:
            Rendering::Camera::Camera camera;
            Geometry::Basic::Floor floor;
            Geometry::Basic::Box box;
            Geometry::Basic::Sphere sphere;

            bool pause;
            float simulationSpeed;
            
            const static unsigned maxContacts = 256;
            const static unsigned ammoRounds = 256;
            const static unsigned boxes = 2;

            //Physics::RigidBody::BVHNode<Physics::RigidBody::BoundingSphere> bvhTree;
            Physics::RigidBody::Contact contacts[maxContacts];
            Physics::RigidBody::CollisionData cData;
            Physics::RigidBody::ContactResolver resolver;

            AmmoRound ammo[ammoRounds];
            Box boxData[boxes];
            ShotType currentShotType;

            void Reset();
            void Fire();
            void UpdateObjects(float dt);
            void GenerateContacts();
            
            AmmoRound testAmmo1;
            AmmoRound testSpheres1[2];
            
            void testCollision();
        };
    }
}

#endif	/* BIGBALLISTICEXAMPLE_H */


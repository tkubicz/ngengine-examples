#include "Examples/CubeMapExample.h"
#include "Media/MediaManager.h"
using namespace NGE::Examples;

CubeMapExample::CubeMapExample() { }

CubeMapExample::~CubeMapExample() { }

bool CubeMapExample::Init()
{
    Media::Shaders::ShaderManager* shaderManager = &Media::MediaManager::GetInstance().GetShaderManager();
    shaderManager->LoadProgram("floorShader", "floorShader.xml");
    shaderManager->LoadProgram("colorShader", "colorShader.xml");
    shaderManager->LoadProgram("cubeMapShader", "cubeMap.xml");

    //camera.Set(-35.0f, 10.0f, 0.0f, 50.0f, 10.0f, 22.0f, 0.0f, 1.0f, 0.0f);
    camera.Set(0.0f, 0.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0);
    
    floor.Initialize(64.f, 4.f, shaderManager->GetProgram("floorShader"));
    floor.SetColor(Math::vec4f(0.0f, 0.6f, 0.2f, 1.0f));

    pugi::xml_document textureInfo;
    pugi::xml_parse_result textureResult = textureInfo.load_file("data/configs/cubeMap.xml");

    for (pugi::xml_node currentTexture = textureInfo.child("Texture2D"); currentTexture != NULL; currentTexture = currentTexture.next_sibling())
        Media::MediaManager::GetInstance().GetTextureManager().LoadTexture(currentTexture);

    for (pugi::xml_node currentTexture = textureInfo.child("TextureCubeMap"); currentTexture != NULL; currentTexture = currentTexture.next_sibling())
        Media::MediaManager::GetInstance().GetTextureManager().LoadTexture(currentTexture);
    
    box.Initialize(2.0f);
    box.SetShader(shaderManager->GetProgram("cubeMapShader"));
    box.SetTexture(Media::MediaManager::GetInstance().GetTextureManager().GetTexture("cubeMap"));
    
    angle = 0.0f;

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    return true;
}

void CubeMapExample::Prepare(float dt)
{
    camera.Update(dt);
    
    angle += 20.0f * dt;
}

void CubeMapExample::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //glClearColor(0.9f, 0.95f, 1.0f, 1.0f);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    Rendering::Renderer::GetInstance().GetMatrixStack().Identity();

    camera.Look();
    //floor.Render();
    
    Rendering::Renderer::GetInstance().GetMatrixStack().PushMatrix();
    //Rendering::Renderer::GetInstance().GetMatrixStack().Translate(0.0f, 1.0f, 0.0f);
    Rendering::Renderer::GetInstance().GetMatrixStack().Rotate(angle, 0.0f, 1.0f, 0.0f);
    
    box.Render();
    
    Rendering::Renderer::GetInstance().GetMatrixStack().PopMatrix();
}

void CubeMapExample::Shutdown() { }

void CubeMapExample::OnKeyPressed(NGE::Events::KeyboardEvent& event)
{
    if (event.GetAction() == Events::PRESSED)
    {
        switch (event.GetKeyId())
        {
            case Events::KEY_Z:
                if (camera.IsMouseLocked())
                    window->EnableMouseCursor(true);
                else
                    window->EnableMouseCursor(false);
                camera.LockMouse(!camera.IsMouseLocked());
                break;
        }
    }

    camera.SetKeyboardInput(event);
}

void CubeMapExample::OnMouse(NGE::Events::MouseEvent& event)
{
    camera.SetMouseInfo(event.GetX(), event.GetY());
}

void CubeMapExample::OnResize(int width, int height)
{
    Application::OnResize(width, height);
}

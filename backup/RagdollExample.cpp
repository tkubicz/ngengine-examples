#include "Examples/RagdollExample.h"
#include "Media/MediaManager.h"
#include "Physics/RigidBody/CollisionPlane.h"
#include "Physics/RigidBody/CollisionDetector.h"
using namespace NGE::Examples;

RagdollExample::RagdollExample() : resolver(maxContacts * 8)
{

    cData.contactArray = contacts;

    // Prawe kolano
    joints[0].Set(bones[0].body, Math::vec3f(0, 1.07f, 0),
                  bones[1].body, Math::vec3f(0, -1.07f, 0),
                  0.15f);

    // Lewe kolano
    joints[1].Set(bones[2].body, Math::vec3f(0, 1.07f, 0),
                  bones[3].body, Math::vec3f(0, -1.07f, 0),
                  0.15f);

    // Prawy łokieć
    joints[2].Set(bones[9].body, Math::vec3f(0, 0.96f, 0),
                  bones[8].body, Math::vec3f(0, -0.96f, 0),
                  0.15f);

    // Lewy łokieć
    joints[3].Set(bones[11].body, Math::vec3f(0.f, 0.96f, 0.f),
                  bones[10].body, Math::vec3f(0.f, -0.96f, 0.f),
                  0.15f);

    // Brzuch
    joints[4].Set(bones[4].body, Math::vec3f(0.054f, 0.50f, 0.f),
                  bones[5].body, Math::vec3f(-0.043f, -0.45f, 0.f),
                  0.15f);

    joints[5].Set(bones[5].body, Math::vec3f(-0.043f, 0.411f, 0.f),
                  bones[6].body, Math::vec3f(0.f, -0.411f, 0.f),
                  0.15f);

    joints[6].Set(bones[6].body, Math::vec3f(0.f, 0.512f, 0.f),
                  bones[7].body, Math::vec3f(0.f, -0.752f, 0.f),
                  0.15f);

    // Prawe biodro
    joints[7].Set(bones[1].body, Math::vec3f(0.f, 1.066f, 0.f),
                  bones[4].body, Math::vec3f(0.f, -0.458f, -0.5f),
                  0.15f);

    // Lewe biodro
    joints[8].Set(bones[3].body, Math::vec3f(0.f, 1.066f, 0.f),
                  bones[4].body, Math::vec3f(0.f, -0.458f, 0.5f),
                  0.15f);

    // Prawe ramie
    joints[9].Set(bones[6].body, Math::vec3f(0.f, 0.367f, -0.8f),
                  bones[8].body, Math::vec3f(0.f, 0.888f, 0.32f),
                  0.15f);

    // Lewe ramie
    joints[10].Set(bones[6].body, Math::vec3f(0.f, 0.367f, 0.8f),
                   bones[10].body, Math::vec3f(0.f, 0.888f, -0.32f),
                   0.15f);
}

RagdollExample::~RagdollExample() { }

bool RagdollExample::Init()
{
    // Shortcuts
    Media::Shaders::ShaderManager* shaderManager = &Media::MediaManager::GetInstance().GetShaderManager();
    Media::Images::TextureManager* textureManager = &Media::MediaManager::GetInstance().GetTextureManager();

    // Load shaders
    shaderManager->LoadProgram("floorShader", "floorShader.xml");
    shaderManager->LoadProgram("colorShader", "colorShader.xml");

    camera.Set(-35.0f, 10.0f, 0.0f, 50.0f, 10.0f, 22.0f, 0.0f, 1.0f, 0.0f);

    floor.Initialize(256.f, 4.f, shaderManager->GetProgram("floorShader"));
    floor.SetColor(Math::vec4f(0.0f, 0.6f, 0.2f, 1.0f));

    box.Initialize(1.0f);
    box.SetShader(shaderManager->GetProgram("colorShader"));
    box.SetColor(Math::vec4f(0.8f, 0.1f, 0.3f, 1.0f));

    Reset();

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    return true;
}

void RagdollExample::Prepare(float dt)
{
    camera.Update(dt);

    for (Bone* bone = bones; bone < bones + numBones; ++bone)
    {
        bone->body->Integrate(dt);
        bone->CalculateInternals();
    }

    GenerateContacts();
    resolver.ResolveContacts(cData.contactArray, cData.contactCount, dt);
}

void RagdollExample::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.9f, 0.95f, 1.0f, 1.0f);
    matrixStack.Identity();

    camera.Look();
    floor.Render();

    for (unsigned i = 0; i < numBones; i++)
    {
        bones[i].Render(&this->box);
    }

}

void RagdollExample::Shutdown() { }

void RagdollExample::OnKeyPressed(NGE::Events::KeyboardEvent& event)
{
    if (event.GetAction() == Events::PRESSED)
    {
        switch (event.GetKeyId())
        {
            case Events::KEY_Z:
                if (camera.IsMouseLocked())
                    window->EnableMouseCursor(true);
                else
                    window->EnableMouseCursor(false);
                camera.LockMouse(!camera.IsMouseLocked());
                break;

            case Events::KEY_R:
                Reset();
                break;
        }
    }

    camera.SetKeyboardInput(event);
}

void RagdollExample::OnMouse(NGE::Events::MouseEvent& event)
{
    camera.SetMouseInfo(event.GetX(), event.GetY());
}

void RagdollExample::Reset()
{
    bones[0].SetState(Math::vec3f(0.f, 0.993f, -0.5f),
                      Math::vec3f(0.301f, 1.0f, 0.234f));
    bones[1].SetState(Math::vec3f(0.f, 3.159f, -0.56f),
                      Math::vec3f(0.301f, 1.0f, 0.234f));
    bones[2].SetState(Math::vec3f(0.f, 0.993f, 0.5f),
                      Math::vec3f(0.301f, 1.0f, 0.234f));
    bones[3].SetState(Math::vec3f(0.f, 3.15f, 0.56f),
                      Math::vec3f(0.301f, 1.0f, 0.234f));
    bones[4].SetState(Math::vec3f(-0.054f, 4.683f, 0.013f),
                      Math::vec3f(0.415f, 0.392f, 0.690f));
    bones[5].SetState(Math::vec3f(0.043f, 5.603f, 0.013f),
                      Math::vec3f(0.301f, 0.367f, 0.693f));
    bones[6].SetState(Math::vec3f(0.f, 6.485f, 0.013f),
                      Math::vec3f(0.435f, 0.367f, 0.786f));
    bones[7].SetState(Math::vec3f(0.f, 7.759f, 0.013f),
                      Math::vec3f(0.45f, 0.598f, 0.421f));
    bones[8].SetState(Math::vec3f(0.f, 5.946f, -1.066f),
                      Math::vec3f(0.267f, 0.888f, 0.207f));
    bones[9].SetState(Math::vec3f(0.f, 4.024f, -1.066f),
                      Math::vec3f(0.267f, 0.888f, 0.207f));
    bones[10].SetState(Math::vec3f(0.f, 5.946f, 1.066f),
                       Math::vec3f(0.267f, 0.888f, 0.207f));
    bones[11].SetState(Math::vec3f(0.f, 4.024f, 1.066f),
                       Math::vec3f(0.267f, 0.888f, 0.207f));

    float strength = -random.RandomFloat(500.0f, 1000.0f);
    //for (unsigned i = 0; i < numBones; ++i)
        //bones[i].body->AddForceAtBodyPoint(Math::vec3f(strength, 0, 0), Math::vec3f());

    //bones[6].body->AddForceAtBodyPoint(Math::vec3f(strength, strength, random.RandomBinomial(1000.0f)),
                                       //Math::vec3f(random.RandomBinomial(4.0f), random.RandomBinomial(3.0f), 0));

    cData.contactCount = 0;
}

void RagdollExample::GenerateContacts()
{
    Physics::RigidBody::CollisionPlane plane;
    plane.direction = Math::vec3f(0, 1, 0);
    plane.offset = 0;

    cData.Reset(maxContacts);
    cData.friction = 0.9f;
    cData.restitution = 0.6f;
    cData.tolerance = 0.1f;

    for (Bone* bone = bones; bone < bones + numBones; bone++)
    {
        if (!cData.HasMoreContacts()) return;
        Physics::RigidBody::CollisionDetector::BoxAndHalfSpace(*bone, plane, &cData);

        Physics::RigidBody::CollisionSphere boneSphere = bone->GetCollisionSphere();

        // Sprawdzenie kolizji z innymi pudełkami
        for (Bone* other = bone + 1; other < bones + numBones; other++)
        {
            if (!cData.HasMoreContacts()) return;

            Physics::RigidBody::CollisionSphere otherSphere = other->GetCollisionSphere();

            Physics::RigidBody::CollisionDetector::SphereAndSphere(boneSphere, otherSphere, &cData);
        }
    }

    // Sprawdzenie stawów
    for (Physics::RigidBody::Joint* joint = joints; joint < joints + numJoints; ++joint)
    {
        if (!cData.HasMoreContacts()) return;
        unsigned added = joint->AddContact(cData.contacts, cData.contactsLeft);
        cData.AddContacts(added);
    }
}





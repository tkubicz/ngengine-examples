/* 
 * File:   RagdollExample.h
 * Author: tku
 *
 * Created on 21 styczeń 2014, 18:13
 */

#ifndef RAGDOLLEXAMPLE_H
#define	RAGDOLLEXAMPLE_H

#include "Physics/RigidBody/CollisionBox.h"
#include "Physics/RigidBody/CollisionSphere.h"
#include "Physics/RigidBody/CollisionData.h"
#include "Physics/RigidBody/Joint.h"
#include "Physics/RigidBody/ContactResolver.h"
#include "Geometry/Basic/Box.h"
#include "Geometry/Basic/Floor.h"
#include "Rendering/Rendering.h"
#include "Windows/Windows.h"
#include "Tools/Random.h"

namespace NGE
{
    namespace Examples
    {

        class ColoredBox : public Geometry::Basic::Box
        {
          protected:
            Math::vec4f currentColor;

          public:

            ColoredBox()
            {
                currentColor = Math::vec4f(0.55f, 0.26f, 0.27f, 0.0f);
            }

            void SetColor(const Math::vec4f& color)
            {
                if (currentColor != color)
                {
                    currentColor = color;
                    shader->SendUniform("in_color", currentColor);
                }
            }

            Math::vec4f GetColor() const
            {
                return currentColor;
            }

            virtual void Render()
            {
                if (shader == NULL)
                    return;

                shader->BindShader();

                shader->SendUniform4x4("modelview_matrix", Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(MODELVIEW_MATRIX));
                shader->SendUniform4x4("projection_matrix", Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(PROJECTION_MATRIX));
                shader->SendUniform("in_color", currentColor);
                
                glBindVertexArray(vertexArray);
                glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
                glBindVertexArray(0);

                shader->UnbindShader();
            }
        };

        class Bone : public Physics::RigidBody::CollisionBox
        {
          protected:
            Math::vec4f awakeColor;
            Math::vec4f sleepColor;

          public:

            Bone()
            {
                body = new Physics::RigidBody::RigidBody();
                awakeColor = Math::vec4f(0.5f, 0.3f, 0.3f, 1.0f);
                sleepColor = Math::vec4f(0.3f, 0.3f, 0.5f, 1.0f);
            }

            ~Bone()
            {
                delete body;
            }

            Physics::RigidBody::CollisionSphere GetCollisionSphere() const
            {
                Physics::RigidBody::CollisionSphere sphere;
                sphere.body = body;
                sphere.radius = halfSize.x;
                sphere.offset = Math::mat4f();

                if (halfSize.y < sphere.radius) sphere.radius = halfSize.y;
                if (halfSize.z < sphere.radius) sphere.radius = halfSize.z;

                sphere.CalculateInternals();
                return sphere;
            }

            void Render(ColoredBox* box)
            {
                if (body->GetAwake())
                    box->SetColor(awakeColor);
                else
                    box->SetColor(sleepColor);

                matrixStack.PushMatrix();
                matrixStack.Multiple(body->GetTransform());
                matrixStack.Scale(halfSize.x * 2, halfSize.y * 2, halfSize.z * 2);
                box->Render();
                matrixStack.PopMatrix();
            }

            void SetState(const Math::vec3f& position, const Math::vec3f& extents)
            {
                body->SetPosition(position);
                body->SetOrientation(Math::quaternionf());
                body->SetVelocity(Math::vec3f());
                body->SetRotation(Math::vec3f());
                halfSize = extents;

                float mass = halfSize.x * halfSize.y * halfSize.z * 8.0f;
                body->SetMass(mass);

                Math::mat3f tensor;
                tensor.SetBlockInertiaTensor(halfSize, mass);
                body->SetInertiaTensor(tensor);
                
                body->SetLinearDamping(0.95f);
                body->SetAngularDamping(0.8f);
                body->ClearAccumulators();
                body->SetAcceleration(0, -9.81f, 0);

                body->SetCanSleep(false);
                body->SetAwake();

                body->CalculateDerivedData();
                CalculateInternals();
            }
        };

        class RagdollExample : public Windows::Application
        {
          public:
            RagdollExample();
            virtual ~RagdollExample();

            virtual bool Init();
            virtual void Prepare(float dt);
            virtual void Render();
            virtual void Shutdown();

            virtual void OnKeyPressed(Events::KeyboardEvent& event);
            virtual void OnMouse(Events::MouseEvent& event);

          protected:
            Rendering::Camera::Camera camera;
            Geometry::Basic::Floor floor;
            ColoredBox box;
            
            const static unsigned maxContacts = 256;
            const static unsigned numBones = 12;
            const static unsigned numJoints = 11;
            
            Tools::Random random;
            Bone bones[numBones];
            Physics::RigidBody::Joint joints[numJoints];
            Physics::RigidBody::Contact contacts[maxContacts];
            Physics::RigidBody::CollisionData cData;
            Physics::RigidBody::ContactResolver resolver;
            
            void GenerateContacts();
            void Reset();
        };
    }
}

#endif	/* RAGDOLLEXAMPLE_H */


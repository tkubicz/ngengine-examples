/* 
 * File:   BoundingVolumeExample.h
 * Author: tku
 *
 * Created on 27 styczeń 2014, 15:35
 */

#ifndef BOUNDINGVOLUMEEXAMPLE_H
#define	BOUNDINGVOLUMEEXAMPLE_H

#include "Windows/Application.h"
#include "Physics/RigidBody/CollisionSphere.h"
#include "Physics/RigidBody/ForceGenerator.h"
#include "Physics/RigidBody/Contact.h"
#include "Physics/RigidBody/ContactResolver.h"
#include "Physics/RigidBody/CollisionData.h"
#include "Physics/RigidBody/BVHNode.h"
#include "Physics/RigidBody/BoundingSphere.h"
#include "Geometry/Basic/Sphere.h"
#include "Geometry/Basic/Box.h"
#include "Geometry/Basic/Floor.h"
#include "Rendering/Rendering.h"
#include "Events/Events.h"
#include "Math/Objects3D/Sphere.h"
#include "Math/Objects3D/AABox.h"
#include "Rendering/Scene/Octree.h"
#include "Rendering/Camera/Frustum.h"
#include "Tools/Random.h"
#include "Media/MediaManager.h"

namespace NGE
{
    namespace Examples
    {

        class Sphere : public Physics::RigidBody::CollisionSphere
        {
          protected:
            Geometry::Basic::Sphere* mesh;

          public:

            Sphere()
            {
                body = new Physics::RigidBody::RigidBody();
            }

            Sphere(Geometry::Basic::Sphere& sphere)
            {
                SetMesh(sphere);
            }

            ~Sphere()
            {
                delete body;
            }

            void Initialize()
            {
                Tools::Random random;

                body->SetMass(10.0f);
                body->SetVelocity(random.RandomVector(Math::vec3f(-15, 0, -15), Math::vec3f(15, 0, 15)));
                body->SetAcceleration(0.0f, 0.0f, 0.0f);
                body->SetDamping(0.99f, 0.8f);

                body->SetCanSleep(false);
                body->SetAwake();

                Math::mat3f tensor;
                float coeff = 0.4f * body->GetMass() * radius * radius;
                tensor.SetInertiaTensorCoeffs(coeff, coeff, coeff);
                body->SetInertiaTensor(tensor);

                body->CalculateDerivedData();
                CalculateInternals();
            }

            void SetMesh(Geometry::Basic::Sphere& sphere)
            {
                this->mesh = &sphere;
            }

            Geometry::Basic::Sphere* GetMesh() const
            {
                return mesh;
            }

            void Render()
            {
                matrixStack.PushMatrix();
                matrixStack.Multiple(body->GetTransform());
                matrixStack.Scale(radius, radius, radius);
                mesh->Render();
                matrixStack.PopMatrix();
            }
        };

        class BoundingVolumeExample : public Windows::Application
        {
          public:
            BoundingVolumeExample();
            virtual ~BoundingVolumeExample();

            bool Init();
            void Prepare(float dt);
            void Render();
            void Shutdown();

            void OnKeyPressed(Events::KeyboardEvent& event);
            void OnMouse(Events::MouseEvent& event);

          protected:
            Rendering::Camera::Camera camera;
            Geometry::Basic::Floor floor;

            Rendering::Scene::Octree<Sphere>* octree;
            Rendering::Camera::Frustum<float> frustum;

            void RenderOctree(Rendering::Scene::Octree<Sphere>* octree);

            bool frustumFollow;

            /* Fields and methods used for box rendering */
            float boxHalfSize;
            Math::vec3f boxPosition;
            Geometry::Basic::Box box;

            /* Pola i metody służące do renderowania kul i hierarchi obiektów */
            const static unsigned maxContacts = 256;
            Geometry::Basic::Sphere sphere;
            std::vector<Sphere*> spheres;
            Physics::RigidBody::ForceRegistry forceRegistry;
            Physics::RigidBody::Gravity gravity;
            Physics::RigidBody::Contact contacts[maxContacts];
            Physics::RigidBody::CollisionData cData;
            Physics::RigidBody::ContactResolver resolver;

            void InitializeSpheres();
            void UpdateSpheres(float dt);
            void RenderSpheres();
            void GenerateContacts();

            /* Pola i metody służące do renderowania i liczenia minimalnej kuli otaczającej */
            Math::Objects3D::Sphere<float> boundingSphere;
            void InitializeMinSphere();

            /* Pola i metody służące do renderowania punktów */
            std::vector<Math::vec3f> points;
            Media::Shaders::GLSLProgram* pointsShader;
            GLuint pointsVertexBuffer;
            GLuint pointsVertexArray;
            void InitializePoints();
            void RenderPoints();

            // Shortcuts
            NGE::Media::Shaders::ShaderManager* shaderManager;
            NGE::Media::Images::TextureManager* textureManager;
        };
    }
}

#endif	/* BOUNDINGVOLUMEEXAMPLE_H */


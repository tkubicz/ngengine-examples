#include "Examples/NormalMappingExample.h"
#include "Media/MediaManager.h"
using namespace NGE::Examples;

void Plane::Update(float dt)
{
    static float angle = 0.0f;
    angle += dt * 5;

    Math::vec3f lightDirection(1.0f, 1.0f, 1.0f);

    lightDirection.x = 2.0f * cos(angle);
    lightDirection.Normalize();

    lightDirection = Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(MODELVIEW_MATRIX) * lightDirection;

    this->lightDirection = lightDirection;
}

void Plane::Render()
{
    if (shader == NULL || colorTexture == NULL || normalTexture == NULL)
    {
        Tools::Logger::WriteErrorLog("Plane --> shader/texture is null");
        return;
    }

    shader->BindShader();

    shader->SendUniform4x4("projectionMatrix", Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(PROJECTION_MATRIX));
    shader->SendUniform4x4("modelviewMatrix", Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(MODELVIEW_MATRIX));
    shader->SendUniform3x3("normalMatrix", Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(MODELVIEW_MATRIX).ExtractMatrix3());
    shader->SendUniform("lightDirection", lightDirection);

    colorTexture->Activate(0);
    shader->SendUniform("u_texture", 0);

    normalTexture->Activate(1);
    shader->SendUniform("u_normalMap", 1);

    glBindVertexArray(vertexArray);

    glClear(GL_COLOR_BUFFER_BIT);
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);

    glBindVertexArray(0);
}

NormalMappingExample::NormalMappingExample() { }

NormalMappingExample::~NormalMappingExample() { }

bool NormalMappingExample::Init()
{
    Media::Shaders::ShaderManager* shaderManager = &Media::MediaManager::GetInstance().GetShaderManager();
    Media::Images::TextureManager* textureManager = &Media::MediaManager::GetInstance().GetTextureManager();

    shaderManager->LoadProgram("floorShader", "floorShader.xml");
    shaderManager->LoadProgram("normalMappingShader", "normalMapping.xml");

    pugi::xml_document textureInfo;
    pugi::xml_parse_result textureResult = textureInfo.load_file("data/configs/normalMappingExample.xml");

    for (pugi::xml_node currentTexture = textureInfo.child("Texture2D"); currentTexture != NULL; currentTexture = currentTexture.next_sibling())
        textureManager->LoadTexture(currentTexture);

    //camera.Set(-35.0f, 10.0f, 0.0f, 50.0f, 10.0f, 22.0f, 0.0f, 1.0f, 0.0f);
    camera.Set(0.0f, 0.0f, -5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
    floor.Initialize(64.f, 4.f, shaderManager->GetProgram("floorShader"));
    floor.SetColor(Math::vec4f(0.803f, 0.584f, 0.047f, 1.0f));

    plane.Initialize();
    plane.SetShader(shaderManager->GetProgram("normalMappingShader"));
    plane.SetTextures(textureManager->GetTexture("rock_color"), textureManager->GetTexture("rock_normal"));

    //glEnable(GL_DEPTH_TEST);
    //glEnable(GL_CULL_FACE);

    return true;
}

void NormalMappingExample::Prepare(float dt)
{
    camera.Update(dt);
    plane.Update(dt);
}

void NormalMappingExample::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //glClearColor(0.9f, 0.95f, 1.0f, 1.0f);   
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    //Rendering::Renderer::GetInstance().GetMatrixStack().Identity();

    camera.Look();
    //floor.Render();
    plane.Render();
}

void NormalMappingExample::Shutdown() { }

void NormalMappingExample::OnKeyPressed(NGE::Events::KeyboardEvent& event)
{
    if (event.GetAction() == Events::PRESSED)
    {
        switch (event.GetKeyId())
        {
            case Events::KEY_Z:
                if (camera.IsMouseLocked())
                    window->EnableMouseCursor(true);
                else
                    window->EnableMouseCursor(false);
                camera.LockMouse(!camera.IsMouseLocked());
                break;
        }
    }

    camera.SetKeyboardInput(event);
}

void NormalMappingExample::OnMouse(NGE::Events::MouseEvent& event)
{
    camera.SetMouseInfo(event.GetX(), event.GetY());
}

void NormalMappingExample::OnResize(int width, int height)
{
    Application::OnResize(width, height);
}
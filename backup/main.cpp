#include <GL/glew.h>
#include <GL/glfw.h>

#include "Windows/Windows.h"
#include "Tools/Tools.h"
#include "Rendering/Rendering.h"
#include "Media/Media.h"
#include "Parsers/pugixml.hpp"

//#include "Examples/Bridge.h"
//#include "Examples/Blank.h"
//#include "Examples/LightingExample.h"
//#include "Examples/WaterExample.h"
//#include "Examples/CubeMapExample.h"
//#include "Examples/NormalMappingExample.h"
//#include "Examples/GPUParticles.h"
//#include "Examples/FrameBufferExample.h"
//#include "Examples/OctreeExample.h"
//#include "Examples/FlightismExample.h"
//#include "Examples/BigBallisticExample.h"
//#include "Examples/RagdollExample.h"
//#include "Examples/BoundingVolumeExample.h"
//#include "Examples/FrustumCullingExample.h"
//#include "Examples/MeshAnimationExample.h"
//#include "Examples/AssimpTestExample.h"

#ifdef _WIN32
#include <windows.h>
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
#else
int main()
#endif
{   
    NGE::Tools::Logger::Initialize("data/log.txt", true);

    if (!glfwInit())
    {
        NGE::Tools::Logger::WriteFatalErrorLog("Error starting GLFW");
        return 1;
    }

//    NGE::Examples::BridgeExample app;
//    NGE::Examples::LightingExample app;
//    NGE::Examples::WaterExample app;
//    NGE::Examples::CubeMapExample app;
//    NGE::Examples::NormalMappingExample app;
//    NGE::Examples::GPUParticles app;
//    NGE::Examples::FrameBufferExample app;
//    NGE::Examples::OctreeExample app;
//    NGE::Examples::FlightismExample app;
//    NGE::Examples::BigBallisticExample app;
//    NGE::Examples::RagdollExample app;
//    NGE::Examples::BoundingVolumeExample app;
//    NGE::Examples::FrustumCullingExample app;
//    NGE::Examples::MeshAnimationExample app;
//    NGE::Examples::AssimpTestExample app;
    
    NGE::Windows::GLFWWindow programWindow;
    programWindow.SetApplication(&app);
   
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file("data/configs/data.xml");
    pugi::xml_node window = doc.child("Window");
    if (!programWindow.LoadXMLSettings(window))
    {
        NGE::Tools::Logger::WriteFatalErrorLog("Unable to load window settings");
        return 1;
    }
    
    NGE::Media::MediaManager::GetInstance().GetMediaPathManager().LoadXMLSettings(doc.child("Config"));

    if (!programWindow.Create())
    {
        NGE::Tools::Logger::WriteFatalErrorLog("Unable to create OpenGL window");
        programWindow.Destroy();
        return 1;
    }
    
    if (!programWindow.Init())
    {
        NGE::Tools::Logger::WriteFatalErrorLog("Could not initialize GLEW");
        return 1;
    }

    if (!app.Init())
    {
        NGE::Tools::Logger::WriteFatalErrorLog("Could not intialize application");
        programWindow.Destroy();
        return 1;
    }

    NGE::Tools::Timing::Initialize();
    NGE::Rendering::Renderer::GetInstance().GetMatrixStack().Initialize();
    programWindow.SetInputCallbacks();
    
    while (programWindow.IsRunning())
    {
        NGE::Tools::Timing::Update();
        float elapsedTime = (float) NGE::Tools::Timing::Get().lastFrameDuration;

        app.Prepare(elapsedTime);
        app.Render();

        programWindow.SwapBuffers();
        programWindow.ProcessEvents();
    }

    NGE::Tools::Logger::Flush();
    app.Shutdown();
    NGE::Media::MediaManager::GetInstance().Deinitialize();
    NGE::Rendering::Renderer::GetInstance().GetMatrixStack().Deinitialize();
    programWindow.Destroy();

    return 0;
}

/* 
 * File:   FrustumCullingExample.h
 * Author: tku
 *
 * Created on 19 luty 2014, 01:44
 */

#ifndef FRUSTUMCULLINGEXAMPLE_H
#define	FRUSTUMCULLINGEXAMPLE_H

#include "Windows/Application.h"
#include "Events/KeyboardEvent.h"
#include "Events/MouseEvent.h"
#include "Rendering/Camera/Camera.h"
#include "Rendering/Camera/Frustum.h"
#include "Rendering/Scene/Octree.h"
#include "Geometry/Basic/Floor.h"
#include "Geometry/Basic/Box.h"
#include "Geometry/Basic/Sphere.h"
#include "Media/MediaManager.h"
#include "Tools/Random.h"
#include "Geometry/Models/XModel.h"
#include "Geometry/Basic/Plane.h"
#include "Media/Images/JpegImage.h"
#include "Media/Images/TargaImage.h"
#include "Media/Images/PngImage.h"

namespace NGE
{
    namespace Examples
    {

        class FrustumCullingExample : public Windows::Application
        {
          private:
            typedef float real;

            struct BoxStruct
            {
                Math::Vector3<real> position;
                Math::Vector3<real> halfSize;
            };
            
            struct Test
            {
                int value;
                std::string secret;
            };
            
            struct MaterialStartEnd
            {
                unsigned materialIndex;
                unsigned start;
                unsigned end;
                
                NGE::Media::Images::Texture* texture;
            };

            struct XMesh
            {
                GLuint buffers[4];
                GLuint vao;
                Geometry::Models::XModel* xmodel;
                Media::Shaders::GLSLProgram* shader;
                std::vector<unsigned int> indices;
                std::vector<MaterialStartEnd> materials;
                
                void Sort();
                void GenerateVBO();
                void GenerateVBA();
                void RenderX();
            };

            class Plane : public Geometry::Basic::Plane
            {
              public:

                virtual void Render()
                {
                    if (!shader || !texture)
                    {
                        //log_error("Plane -> No texture or shader.");
                        return;
                    }

                    shader->BindShader();
                    texture->Activate();

                    shader->SendUniform4x4("modelview_matrix", Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(MODELVIEW_MATRIX));
                    shader->SendUniform4x4("projection_matrix", Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(PROJECTION_MATRIX));

                    glBindVertexArray(vertexArray);
                    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
                    glBindVertexArray(0);

                    texture->Deactivate();
                    shader->UnbindShader();
                }

              public:
                Media::Shaders::GLSLProgram* shader;
                Media::Images::Texture* texture;
            };

          public:
            FrustumCullingExample();
            virtual ~FrustumCullingExample();

            virtual bool Init();
            virtual void Prepare(float dt);
            virtual void Render();
            virtual void Shutdown();

            virtual void OnResize(int width, int height);

            void OnKeyPressed(Events::KeyboardEvent& event);
            void OnMouse(Events::MouseEvent& event);

          protected:
            Rendering::Camera::Camera* camera;
            Rendering::Camera::Frustum<real>* frustum;
            Rendering::Scene::Octree<real>* octree;

            Media::Images::TargaImage tga;
            Media::Images::JpegImage jpeg;
            Media::Images::PngImage png;
            Media::Images::Texture texTga;
            Media::Images::Texture texJpeg;
            Media::Images::Texture texPng;
            Plane* planeTga;
            Plane* planeJpeg;
            Plane* planePng;
            
            Geometry::Basic::Floor* floor;
            Geometry::Basic::Box* box;
            Geometry::Basic::Sphere* sphere;
            XMesh xmesh;
            XMesh xmesh2;

            const static int boxesCount = 256;
            BoxStruct* boxes;

            Tools::Random* random;

            // Shortcuts
            NGE::Media::Shaders::ShaderManager* shaderManager;
            NGE::Media::Images::TextureManager* textureManager;

          protected:
            void CheckIfInFrustum();
            void GenerateBoxes();
        };
    }
}

#endif	/* FRUSTUMCULLINGEXAMPLE_H */


#include "Examples/OctreeExample.h"
#include "Media/MediaManager.h"
using namespace NGE::Examples;

OctreeExample::OctreeExample() { }

OctreeExample::~OctreeExample() { }

bool OctreeExample::Init()
{
    // Wczytanie shaderów
    Media::MediaManager::GetInstance().GetShaderManager().LoadProgram("floorShader", "floorShader.xml");
    Media::MediaManager::GetInstance().GetShaderManager().LoadProgram("basicShader", "shaders.xml");

    // Wczytanie teskstur
    pugi::xml_document textureInfo;
    pugi::xml_parse_result textureResult = textureInfo.load_file("data/configs/textures.xml");
    pugi::xml_node currentTexture = textureInfo.child("Texture2D");
    texture = Media::MediaManager::GetInstance().GetTextureManager().GetTexture(currentTexture);

    camera.Set(-35.0f, 10.0f, 0.0f, 50.0f, 10.0f, 22.0f, 0.0f, 1.0f, 0.0f);
    floor.Initialize(100.f, 4.f, Media::MediaManager::GetInstance().GetShaderManager().GetProgram("floorShader"));

    sphere.Initialize(20, 20, 2.0, Media::MediaManager::GetInstance().GetShaderManager().GetProgram("basicShader"));
    sphere.SetTexture(texture);

    octree = new Rendering::Scene::Octree<Math::vec4f>(Math::vec3f(0, 0, 0), 100.0f);

    // Wygenerowanie pozycji kul
    for (int i = 0; i < 1000; ++i)
    {
        spherePositions.push_back(Math::vec4f(random.RandomFloat(-100.0f, 100.0f),
                                              random.RandomFloat(1.0f, 20.0f),
                                              random.RandomFloat(-100.0f, 100.0f),
                                              random.RandomFloat(0.2f, 1.0f)));

        //octree->Insert(&spherePositions[i], Math::vec3f(spherePositions[i].x, spherePositions[i].y, spherePositions[i].z));
    }

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    return true;
}

void OctreeExample::Prepare(float dt)
{
    camera.Update(dt);
}

void OctreeExample::Render()
{
    glClearColor(0.9f, 0.95f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    Rendering::Renderer::GetInstance().GetMatrixStack().Identity();

    camera.Look();
    floor.Render();

    static int rendered;
    
    rendered = 0;
    for (unsigned i = 0; i < spherePositions.size(); ++i)
    {
        //if (!camera.GetFrustum().SphereInFrustum(Math::vec3f(spherePositions[i].x, spherePositions[i].y, spherePositions[i].z), 2.0f * spherePositions[i].w))
            //continue;

        Rendering::Renderer::GetInstance().GetMatrixStack().PushMatrix();
        Rendering::Renderer::GetInstance().GetMatrixStack().Translate(spherePositions[i].x, spherePositions[i].y, spherePositions[i].z);
        Rendering::Renderer::GetInstance().GetMatrixStack().Scale(spherePositions[i].w, spherePositions[i].w, spherePositions[i].w);

        sphere.Render();
        Rendering::Renderer::GetInstance().GetMatrixStack().PopMatrix();
        
        rendered += 1;
    }
    
    std::cout << "Rendered: " << rendered << "/" << spherePositions.size() << std::endl;
}

void OctreeExample::Shutdown()
{
    delete octree;
}

void OctreeExample::OnKeyPressed(NGE::Events::KeyboardEvent& event)
{
    camera.SetKeyboardInput(event);

    if (event.GetAction() == Events::PRESSED)
    {
        if (event.GetKeyId() == Events::KEY_Z)
        {
            if (camera.IsMouseLocked())
            {
                window->EnableMouseCursor(true);
            }
            else
            {
                window->EnableMouseCursor(false);
            }

            camera.LockMouse(!camera.IsMouseLocked());
        }
    }
}

void OctreeExample::OnMouse(NGE::Events::MouseEvent& event)
{
    camera.SetMouseInfo(event.GetX(), event.GetY());
}

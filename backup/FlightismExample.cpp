#include "Examples/FlightismExample.h"
#include "Media/MediaManager.h"
using namespace NGE::Examples;

FlightismExample::FlightismExample()
{
    rightWing = new Physics::RigidBody::AeroControl(Math::mat3f(0, -1, 0, 0, -0.5f, 0, 0, 0, 0),
                                                    Math::mat3f(0, -0.995f, 0, 0, -0.5f, 0, 0, 0, 0),
                                                    Math::mat3f(0, -1.005f, 0, 0, -0.5f, 0, 0, 0, 0),
                                                    Math::vec3f(-1.0f, 0.0f, 2.0f), &windspeed);

    leftWing = new Physics::RigidBody::AeroControl(Math::mat3f(0, -1, 0, 0, -0.5f, 0, 0, 0, 0),
                                                   Math::mat3f(0, -0.995f, 0, 0, -0.5f, 0, 0, 0, 0),
                                                   Math::mat3f(0, -1.005f, 0, 0, -0.5f, 0, 0, 0, 0),
                                                   Math::vec3f(-1.0f, 0.0f, -2.0f), &windspeed);

    rudder = new Physics::RigidBody::AeroControl(Math::mat3f(0, 0, 0, 0, 0, 0, 0, 0, 0),
                                                 Math::mat3f(0, 0, 0.01f, 0, 0, 0, 0, 0, 0),
                                                 Math::mat3f(0, 0, -0.01f, 0, 0, 0, 0, 0, 0),
                                                 Math::vec3f(2.0f, 0.5f, 0.0f), &windspeed);

    tail = new Physics::RigidBody::Aero(Math::mat3f(0, -1, 0, 0, -0.5f, 0, 0, 0, -0.1f),
                                        Math::vec3f(2.0f, 0, 0), &windspeed);

    leftWingControl = rightWingControl = rudderControl = 0.0f;

    windspeed.Set(0, 0, 0);

    // Ustawienie ciała sztywnego samolotu
    ResetPlane();

    aircraft.SetMass(2.5f);
    Math::mat3f it;
    it.SetBlockInertiaTensor(Math::vec3f(2, 1, 1), 1);
    aircraft.SetInertiaTensor(it);

    aircraft.SetDamping(0.8f, 0.8f);
    aircraft.SetAcceleration(Math::vec3f(0.f, -9.81f, 0.f));
    aircraft.CalculateDerivedData();

    aircraft.SetAwake();
    aircraft.SetCanSleep(false);

    registry.Add(&aircraft, leftWing);
    registry.Add(&aircraft, rightWing);
    registry.Add(&aircraft, rudder);
    registry.Add(&aircraft, tail);

}

FlightismExample::~FlightismExample()
{
    delete rightWing;
    delete leftWing;
    delete rudder;
    delete tail;
}

bool FlightismExample::Init()
{
    Media::Shaders::ShaderManager* shaderManager = &Media::MediaManager::GetInstance().GetShaderManager();
    shaderManager->LoadProgram("floorShader", "floorShader.xml");
    shaderManager->LoadProgram("colorShader", "colorShader.xml");

    camera.Set(-35.0f, 10.0f, 0.0f, 50.0f, 10.0f, 22.0f, 0.0f, 1.0f, 0.0f);

    floor.Initialize(256.f, 4.f, shaderManager->GetProgram("floorShader"));
    floor.SetColor(Math::vec4f(0.0f, 0.6f, 0.2f, 1.0f));

    box.Initialize(1.0f);
    box.SetShader(shaderManager->GetProgram("colorShader"));

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    return true;
}

void FlightismExample::Prepare(float dt)
{   
    camera.Update(dt);
    
    float duration = dt;
    if (duration <= 0.0f)
        return;
    
    // Starts with no forces or acceleration
    aircraft.ClearAccumulators();
    
    // Add the propeller force
    Math::vec3f propulsion(-10.0f, 0.0f, 0.0f);
    propulsion = aircraft.GetTransform().TransformDirection(propulsion);
    aircraft.AddForce(propulsion);
    
    // Add the forces acting on the aircraft
    registry.UpdateForces(duration);
    
    // Update the aircraft physics
    aircraft.Integrate(duration);
    
    // Do a very basic collision detection and response with the ground
    Math::vec3f pos = aircraft.GetPosition();
    if (pos.y < 0.0f)
    {
        pos.y = 0.0f;
        aircraft.SetPosition(pos);
        
        if (aircraft.GetVelocity().y < -10.0f)
        {
            ResetPlane();
        }
    }
}

void FlightismExample::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.9f, 0.95f, 1.0f, 1.0f);

    Rendering::Renderer::GetInstance().GetMatrixStack().Identity();
    
    Math::vec3f pos = aircraft.GetPosition();
    Math::vec3f offset(4.0f + aircraft.GetVelocity().Length(), 0, 0);
    offset = aircraft.GetTransform().TransformDirection(offset);
    
    // Change camera position
    camera.Set(pos.x + offset.x, pos.y + 5.0f, pos.z + offset.z, pos.x, pos.y, pos.z, 0.0f, 1.0f, 0.0f);
    
    camera.Look();
    
    Rendering::Renderer::GetInstance().GetMatrixStack().PushMatrix();
    //Rendering::Renderer::GetInstance().GetMatrixStack().Translate(pos.x, 0.0f, pos.z);
    floor.Render();
    Rendering::Renderer::GetInstance().GetMatrixStack().PopMatrix();
    
    // Set the transform matrix for the aircraft
    Math::mat4f transform = aircraft.GetTransform();
    
    Rendering::Renderer::GetInstance().GetMatrixStack().PushMatrix();
    Rendering::Renderer::GetInstance().GetMatrixStack().Multiple(transform);
    RenderPlane();
    Rendering::Renderer::GetInstance().GetMatrixStack().PopMatrix();
}

void FlightismExample::Shutdown() { }

void FlightismExample::OnKeyPressed(NGE::Events::KeyboardEvent& event)
{
    if (event.GetAction() == Events::PRESSED)
    {
        switch (event.GetKeyId())
        {
            case Events::KEY_Z:
                if (camera.IsMouseLocked())
                    window->EnableMouseCursor(true);
                else
                    window->EnableMouseCursor(false);
                camera.LockMouse(!camera.IsMouseLocked());
                break;
                
            case Events::KEY_Q:
                rudderControl += 0.1f;
                break;
                
            case Events::KEY_E:
                rudderControl -= 0.1f;
                break;
                
            case Events::KEY_W:
                leftWingControl -= 0.1f;
                rightWingControl -= 0.1f;
                break;
                
            case Events::KEY_S:
                leftWingControl += 0.1f;
                rightWingControl += 0.1f;
                break;
                
            case Events::KEY_D:
                leftWingControl -= 0.1f;
                rightWingControl += 0.1f;
                break;
                
            case Events::KEY_A:
                leftWingControl += 0.1f;
                rightWingControl -= 0.1f;
                break;
                
            case Events::KEY_X:
                leftWingControl = 0.0f;
                rightWingControl = 0.0f;
                rudderControl = 0.0f;
                break;
                
            case Events::KEY_R:
                ResetPlane();
                break;
        }
    }
    
    if (leftWingControl < -1.0f)
        leftWingControl = -1.0f;
    else if (leftWingControl > 1.0f)
        leftWingControl = 1.0f;
    
    // Update the control surface
    leftWing->SetControl(leftWingControl);
    rightWing->SetControl(rightWingControl);
    rudder->SetControl(rudderControl);

    camera.SetKeyboardInput(event);
}

void FlightismExample::OnMouse(NGE::Events::MouseEvent& event)
{
    camera.SetMouseInfo(event.GetX(), event.GetY());
}

void FlightismExample::ResetPlane()
{
    aircraft.SetPosition(0, 0, 0);
    aircraft.SetOrientation(1, 0, 0, 0);

    aircraft.SetVelocity(0, 0, 0);
    aircraft.SetRotation(0, 0, 0);
}

void FlightismExample::RenderPlane()
{
    // Fuselage
    Rendering::Renderer::GetInstance().GetMatrixStack().PushMatrix();
    Rendering::Renderer::GetInstance().GetMatrixStack().Translate(-0.5f, 0.0f, 0.0f);
    Rendering::Renderer::GetInstance().GetMatrixStack().Scale(2.0f, 0.8f, 1.0f);
    box.Render();
    Rendering::Renderer::GetInstance().GetMatrixStack().PopMatrix();

    // Rear Fuselage
    Rendering::Renderer::GetInstance().GetMatrixStack().PushMatrix();
    Rendering::Renderer::GetInstance().GetMatrixStack().Translate(1.0f, 0.15f, 0.0f);
    Rendering::Renderer::GetInstance().GetMatrixStack().Scale(2.75f, 0.5f, 0.5f);
    box.Render();
    Rendering::Renderer::GetInstance().GetMatrixStack().PopMatrix();
    
    // Wings
    Rendering::Renderer::GetInstance().GetMatrixStack().PushMatrix();
    Rendering::Renderer::GetInstance().GetMatrixStack().Translate(0.0f, 0.3f, 0.0f);
    Rendering::Renderer::GetInstance().GetMatrixStack().Scale(0.8f, 0.1f, 6.0f);
    box.Render();
    Rendering::Renderer::GetInstance().GetMatrixStack().PopMatrix();
    
    // Rudder
    Rendering::Renderer::GetInstance().GetMatrixStack().PushMatrix();
    Rendering::Renderer::GetInstance().GetMatrixStack().Translate(2.0f, 0.775f, 0.0f);
    Rendering::Renderer::GetInstance().GetMatrixStack().Scale(0.75f, 1.15f, 0.1f);
    box.Render();
    Rendering::Renderer::GetInstance().GetMatrixStack().PopMatrix();
    
    // Tail-plane
    Rendering::Renderer::GetInstance().GetMatrixStack().PushMatrix();
    Rendering::Renderer::GetInstance().GetMatrixStack().Translate(1.9f, 0.0f, 0.0f);
    Rendering::Renderer::GetInstance().GetMatrixStack().Scale(0.85f, 0.1f, 2.0f);
    box.Render();
    Rendering::Renderer::GetInstance().GetMatrixStack().PopMatrix();
}


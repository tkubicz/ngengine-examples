#ifndef BRIDGE_H
#define BIRDGE_H

#include <GL/gl.h>
#include <vector>
#include "Media/Media.h"
#include "Physics/MassAggregate/Particle.h"
#include "Physics/MassAggregate/ParticleWorld.h"
#include "Tools/Random.h"
#include "Geometry/Basic/Sphere.h"
#include "Geometry/Basic/Box.h"
#include "Rendering/Rendering.h"
#include "Events/MouseEvent.h"
#include "Events/KeyboardEvent.h"
#include "GUI/GUI.h"
#include "Geometry/Nature/Terrain.h"
#include "Windows/Application.h"

namespace NGE
{
    namespace Examples
    {
        class BridgeExample : public Windows::Application, public Events::GUIEventListener
        {
        public:
            BridgeExample();
            virtual ~BridgeExample();

            bool Init();
            void Prepare(float dt);
            void Render();
            void Shutdown();

            void OnKeyPressed(Events::KeyboardEvent& event);
            void OnMouse(Events::MouseEvent& event);
            void OnMouseDrag(int x, int y);

            void OnResize(int width, int height);
            
            void ActionPerformed(GUI::GUIEvent& event);

        private:
            Media::Shaders::GLSLProgram* shader;
            Media::Shaders::GLSLProgram* floorShader;
            Media::Shaders::GLSLProgram* bridgeShader;

            Rendering::Camera::Camera camera;
            
            Media::Images::Texture iceTexture;
            Media::Images::Texture* woodTexture;
            Media::Images::Texture* metalTexture;
            Media::Images::Texture fontTexture;
            
            GUI::GUIFrame* panel;
            GUI::GUIPanel* buttonPanel;
            
            //Media::Models::ObjModel objTest;
            Geometry::Nature::Terrain terrain;
            
            Math::vec2i dims;

            bool mouseLocked;

            GLuint floorVertexBuffer, floorColorBuffer, floorTextureBuffer;
            std::vector<Math::vec3f> floorVertices;
            void GenerateFloor();
            void RenderFloor();

            GLuint bridgeVertexBuffer, bridgeColorBuffer;
            std::vector<Math::vec3f> bridgeVertices;
            std::vector<Math::vec3f> bridgeColors;
            void GenerateBridge();
            void UpdateBridge();
            void RenderBridge();

            Physics::MassAggregate::ParticleWorld* world;
            unsigned int particleCount;
            Physics::MassAggregate::Particle* particleArray;
            Physics::MassAggregate::GroundContacts groundContactGenerator;
            
            Geometry::Basic::Sphere sphere;
            Geometry::Basic::Box box;

            Physics::MassAggregate::ParticleCableConstraint* supports;
            Physics::MassAggregate::ParticleCable *cables;
            Physics::MassAggregate::ParticleRod *rods;
            Math::vec3f massPos;
            Math::vec3f massDisplayPos;
            
            //int keyActive, keyNum;
            Events::KeyboardEvent keyEvent;
            bool pause;

            Math::vec3f GRAVITY;
            static const int ROD_COUNT = 6;
            static const int CABLE_COUNT = 10;
            static const int SUPPORT_COUNT = 12;
            static const int BASE_MASS = 1;
            static const int EXTRA_MASS = 10;

            void UpdateAdditionalMass();
        };
    }
}
#endif

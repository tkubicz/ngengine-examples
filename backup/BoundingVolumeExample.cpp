#include "Examples/BoundingVolumeExample.h"
#include "Math/MinSphere3.h"
#include "Physics/RigidBody/CollisionPlane.h"
#include "Physics/RigidBody/CollisionData.h"
#include "Physics/RigidBody/CollisionDetector.h"
#include "Geometry/Basic/Box.h"
#include "Math/Objects3D/Plane.h"
#include "Tools/Settings.h"
#include <GL/glfw.h>
using namespace NGE::Examples;

BoundingVolumeExample::BoundingVolumeExample() : gravity(Math::vec3f(0.0f, -9.81f, 0.0f)), resolver(maxContacts * 8)
{
    cData.contactArray = contacts;
    frustumFollow = false;
}

BoundingVolumeExample::~BoundingVolumeExample() { }

bool BoundingVolumeExample::Init()
{
    shaderManager = &NGE::Media::MediaManager::GetInstance().GetShaderManager();
    textureManager = &NGE::Media::MediaManager::GetInstance().GetTextureManager();

    // Load shaders
    shaderManager->LoadProgram("floorShader", "floorShader.xml");
    shaderManager->LoadProgram("colorShader", "colorShader.xml");
    shaderManager->LoadProgram("basicShader", "shaders.xml");
    shaderManager->LoadProgram("solidColorShader", "colorShader.xml");

    // Load textures
    pugi::xml_document textureInfo;
    pugi::xml_parse_result textureResult = textureInfo.load_file("data/configs/textures.xml");
    pugi::xml_node currentTexture = textureInfo.child("Texture2D");
    textureManager->GetTexture(currentTexture);

    camera.Set(0.0f, 10.0f, 30.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

    floor.Initialize(256.f, 4.0f, shaderManager->GetProgram("floorShader"));
    floor.SetColor(Math::vec4f(0.0f, 0.6f, 0.2f, 1.0f));

    pointsShader = shaderManager->GetProgram("solidColorShader");

    sphere.Initialize(20, 20, 3.0f, shaderManager->GetProgram("basicShader"));
    sphere.SetTexture(textureManager->GetTexture("wood"));

    boxHalfSize = 30.0f;
    boxPosition.Set(0, 0, 0);
    box.Initialize(boxHalfSize * 2.0);
    box.SetShader(shaderManager->GetProgram("colorShader"));

    InitializePoints();
    //InitializeMinSphere();
    //InitializeSpheres();

    frustum.SetCameraInternals(52.f, 800.f / 600.f, 0.1f, 200.f);
    frustum.SetCameraDefinition(Math::vec3f(0.f, 10.f, 30.f), Math::vec3f(0.f, 0.f, 0.f), Math::vec3f(0.f, 1.f, 0.f));

    //aabox.InitialiseVBO();

    glEnable(GL_DEPTH_TEST);
    //glEnable(GL_CULL_FACE);

    return true;
}

void BoundingVolumeExample::Prepare(float dt)
{
    camera.Update(dt);

    if (frustumFollow)
        frustum.SetCameraDefinition(camera.GetViewerPosition(), camera.GetFocusPosition(), camera.GetUpDirection());

    std::cout << "point: " << frustum.PointInFrustum(Math::vec3f(0, 0, 0)) << std::endl;
    std::cout << "sphere: " << frustum.SphereInFrustum(Math::vec3f(0, 0, -71), 3.0f) << std::endl;
    std::cout << "box: " << frustum.AABoxInFrustum(Math::Objects3D::aaboxf(boxPosition, Math::vec3f(boxHalfSize, boxHalfSize, boxHalfSize))) << std::endl;

    /*forceRegistry.UpdateForces(dt);
    UpdateSpheres(dt);
    GenerateContacts();
    resolver.ResolveContacts(cData.contactArray, cData.contactCount, dt);*/
}

void BoundingVolumeExample::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.9f, 0.95f, 1.0f, 1.0f);
    matrixStack.Identity();

    camera.Look();
    //floor.Render();

    //RenderSpheres();
    RenderPoints();
    RenderOctree(octree);

    matrixStack.PushMatrix();
    matrixStack.Translate(0, 0, -70);
    //sphere.Render();
    matrixStack.PopMatrix();

    matrixStack.PushMatrix();
    matrixStack.Translate(boxPosition);
    //matrixStack.Scale(2, 2, 2);
    //box.Render();
    matrixStack.PopMatrix();

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    frustum.Render();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    /*glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    matrixStack.PushMatrix();
    matrixStack.Translate(boundingSphere.GetCenter());
    matrixStack.Scale(boundingSphere.GetRadius(), boundingSphere.GetRadius(), boundingSphere.GetRadius());
    sphere.Render();
    matrixStack.PopMatrix();

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);*/
}

void BoundingVolumeExample::Shutdown() { }

void BoundingVolumeExample::OnKeyPressed(NGE::Events::KeyboardEvent& event)
{
    if (event.GetAction() == Events::PRESSED)
    {
        switch (event.GetKeyId())
        {
            case Events::KEY_Z:
                if (camera.IsMouseLocked())
                    window->EnableMouseCursor(true);
                else
                    window->EnableMouseCursor(false);
                camera.LockMouse(!camera.IsMouseLocked());
                break;

            case Events::KEY_R:
                InitializePoints();
                InitializeMinSphere();
                break;

            case Events::KEY_SPACE:
                for (unsigned i = 0; i < spheres.size(); ++i)
                {
                    spheres[i]->body->AddVelocity(Math::vec3f(0, 0, 10));
                }
                break;

            case Events::KEY_C:
                log_info("Focus: " + to_string(camera.GetFocusPosition()));
                break;

            case Events::KEY_F:
                frustumFollow = !frustumFollow;
                break;
        }
    }

    camera.SetKeyboardInput(event);
}

void BoundingVolumeExample::OnMouse(NGE::Events::MouseEvent& event)
{
    camera.SetMouseInfo(event.GetX(), event.GetY());
}

void BoundingVolumeExample::RenderOctree(Rendering::Scene::Octree<Sphere>* octree)
{
    if (octree != NULL)
    {
        //std::cout << "octree: " << frustum.AABoxInFrustum(Math::Objects3D::aaboxf(octree->GetOrigin(), octree->GetHalfSize()))
        //        << " origin: " << octree->GetOrigin() << " | halfSize: " << octree->GetHalfSize() << std::endl;
        octree->Render(frustum);
    }
}

void BoundingVolumeExample::InitializeSpheres()
{
    static const unsigned sphereCount = 100;
    static const float maxRadius = 2.0f;
    static const float minRadius = 0.2f;
    static const Math::vec3f minBound(0.0f, 0.0f, 0.0f);
    static const Math::vec3f maxBound(300.0f, 200.0f, 300.0f);
    Tools::Random random;

    spheres.resize(sphereCount, NULL);
    for (unsigned i = 0; i < sphereCount; ++i)
    {
        spheres[i] = new Sphere();
        spheres[i]->SetMesh(sphere);
        spheres[i]->body->SetPosition(random.RandomVector(minBound, maxBound));
        spheres[i]->radius = random.RandomFloat(minRadius, maxRadius);

        spheres[i]->Initialize();

        forceRegistry.Add(spheres[i]->body, &gravity);
    }
}

void BoundingVolumeExample::UpdateSpheres(float dt)
{
    for (int i = 0; i < spheres.size(); ++i)
    {
        spheres[i]->body->Integrate(dt);
        spheres[i]->CalculateInternals();
    }
}

void BoundingVolumeExample::RenderSpheres()
{
    for (unsigned i = 0; i < spheres.size(); ++i)
    {
        spheres[i]->Render();
    }
}

void BoundingVolumeExample::GenerateContacts()
{
    Physics::RigidBody::CollisionPlane plane;
    plane.direction = Math::vec3f(0, 1, 0);
    plane.offset = 0;

    cData.Reset(maxContacts);
    cData.friction = 0.9f;
    cData.restitution = 0.1f;
    cData.tolerance = 0.1f;

    for (unsigned i = 0; i < spheres.size(); ++i)
    {
        if (!cData.HasMoreContacts())
            return;

        Physics::RigidBody::CollisionDetector::SphereAndHalfSpace(*spheres[i], plane, &cData);
    }
}

void BoundingVolumeExample::InitializePoints()
{
    // Zainicjowanie punktów

    // Podstawowe informacje dla punktów
    static const unsigned pointsSize = 10;
    static const float minBound = -30;
    static const float maxBound = 30;

    Tools::Random random;
    points.resize(pointsSize, Math::vec3f());
    Sphere spTest;
    octree = new Rendering::Scene::Octree<Sphere>(Math::vec3f(0, 0, 0), Math::vec3f(30, 30, 30));

    for (unsigned i = 0; i < points.size(); ++i)
    {
        points[i] = random.RandomVector(Math::vec3f(minBound, minBound, minBound), Math::vec3f(maxBound, maxBound, maxBound));
        //log_info("points[" + to_string(i) + "] = Math::vec3f" + to_string(points[i]) + ";");

        if (
            (points[i].x < minBound || points[i].x > maxBound) ||
            (points[i].y < minBound || points[i].y > maxBound) ||
            (points[i].z < minBound || points[i].z > maxBound)
            )
            log_info("point " + to_string(points[i]) + " out of bound - index: " + to_string(i));

        octree->Insert(&spTest, points[i]);
    }

    glGenBuffers(1, &pointsVertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, pointsVertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof (GLfloat) * 3 * points.size(), &points[0], GL_STATIC_DRAW);

    glGenVertexArrays(1, &pointsVertexArray);
    glBindVertexArray(pointsVertexArray);

    glBindBuffer(GL_ARRAY_BUFFER, pointsVertexBuffer);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindVertexArray(0);

    glPointSize(4.0f);
}

void BoundingVolumeExample::RenderPoints()
{
    pointsShader->BindShader();

    pointsShader->SendUniform4x4("modelview_matrix", Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(MODELVIEW_MATRIX));
    pointsShader->SendUniform4x4("projection_matrix", Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(PROJECTION_MATRIX));
    pointsShader->SendUniform("color", Math::vec4f(1.0f, 0.0f, 0.0f));

    glBindVertexArray(pointsVertexArray);
    glDrawArrays(GL_POINTS, 0, points.size());
    glBindVertexArray(0);

    pointsShader->UnbindShader();
}

void BoundingVolumeExample::InitializeMinSphere()
{
    double timeBefore = glfwGetTime();
    Math::MinSphere3<float> minSphere(points.size(), &points[0], boundingSphere);
    double timeAfter = glfwGetTime();

    log_info("Sphere generation time: " + to_string(timeAfter - timeBefore));

    log_info("Sphere\t center: " + to_string(boundingSphere.GetCenter()) +
             "      \t radius: " + to_string(boundingSphere.GetRadius()));
}

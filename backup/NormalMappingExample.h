/* 
 * File:   NormalMappingExample.h
 * Author: tku
 *
 * Created on 25 listopad 2013, 13:35
 */

#ifndef NORMALMAPPINGEXAMPLE_H
#define	NORMALMAPPINGEXAMPLE_H

#include "Windows/Windows.h"
#include "Events/Events.h"
#include "Geometry/Basic/Floor.h"
#include "Rendering/Rendering.h"
#include "Media/Images/Texture.h"
#include "Geometry/Basic/Plane.h"

namespace NGE
{
    namespace Examples
    {

        class Plane : public Geometry::Basic::Plane
        {
          public:
            
            void Update(float dt);
            void Render();
            
            void SetShader(Media::Shaders::GLSLProgram* shader)
            {
                this->shader = shader;
            }

            void SetTextures(Media::Images::Texture* colorTexture, Media::Images::Texture* normalTexture)
            {
                this->colorTexture = colorTexture;
                this->normalTexture = normalTexture;
            }

          protected:
            Media::Shaders::GLSLProgram* shader;
            Media::Images::Texture* colorTexture;
            Media::Images::Texture* normalTexture;
            Math::vec3f lightDirection;
        };

        class NormalMappingExample : public Windows::Application
        {
          public:
            NormalMappingExample();
            ~NormalMappingExample();

            bool Init();
            void Prepare(float dt);
            void Render();
            void Shutdown();

            void OnKeyPressed(Events::KeyboardEvent& event);
            void OnMouse(Events::MouseEvent& event);

            void OnResize(int width, int height);

          private:
            Rendering::Camera::Camera camera;
            Geometry::Basic::Floor floor;
            Plane plane;
        };
    }
}

#endif	/* NORMALMAPPINGEXAMPLE_H */


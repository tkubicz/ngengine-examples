<Shader name="waterShader" autoinit="true" autobind="true">
    <VertexShader>
        <Attrib name="a_vertex" type="vec3" index="0" />
        <Raw>
            <![CDATA[
            #version 130
            #define NUMBERWAVES 4
            
            const float PI = 3.141592654;
            const float G = 9.81;
            
            uniform mat4 u_projectionMatrix;
            uniform mat4 u_viewMatrix;
            uniform mat3 u_inverseViewNormalMatrix;
            
            uniform float u_passedTime;
            uniform float u_waterPlaneLength;
            
            uniform vec4 u_waveParameters[NUMBERWAVES];
            uniform vec2 u_waveDirections[NUMBERWAVES];
            
            in vec4 a_vertex;
            
            out vec3 v_incident;
            out vec3 v_bitangent;
            out vec3 v_normal;
            out vec3 v_tangent;
            out vec2 v_texCoord;
            
            void main(void)
            {
                vec4 finalVertex = vec4(a_vertex.xyz, 1.0);
            
                vec3 finalBitangent = vec3(0.0, 0.0, 0.0);
                vec3 finalNormal = vec3(0.0, 0.0, 0.0);
                vec3 finalTangent = vec3(0.0, 0.0, 0.0);
            
                // GPU Gems: Chapter 1. Effective Water Simulation from Physical Models
                for (int i = 0; i < NUMBERWAVES; i++)
                {
                    vec2 direction = normalize(u_waveDirections[i]);
                    float speed = u_waveParameters[i].x;
                    float amplitude = u_waveParameters[i].y;
                    float wavelength = u_waveParameters[i].z;
                    float steepness = u_waveParameters[i].w;
                    
                    float frequency = sqrt(G * 2.0 * PI / wavelength);
                    float phase = speed * frequency;
                    float alpha = frequency * dot(direction, a_vertex.xz) + phase * u_passedTime;
                    
                    finalVertex.x += steepness * amplitude * direction.x * cos(alpha);
                    finalVertex.y += amplitude * sin(alpha);
                    finalVertex.z += steepness * amplitude * direction.y * cos(alpha);
                }
                
                for (int i = 0; i < NUMBERWAVES; i++)
                {
                    vec2 direction = normalize(u_waveDirections[i]);
                    float speed = u_waveParameters[i].x;
                    float amplitude = u_waveParameters[i].y;
                    float wavelength = u_waveParameters[i].z;
                    float steepness = u_waveParameters[i].w;
                    
                    float frequency = sqrt(G * 2.0 * PI / wavelength);
                    float phase = speed * frequency;
                    float alpha = frequency * dot(direction, finalVertex.xz) + phase * u_passedTime;
                    
                    // x direction
                    finalBitangent.x += steepness * direction.x * direction.x * wavelength * amplitude * sin(alpha);
                    finalBitangent.y += direction.x * wavelength * amplitude * cos(alpha);
                    finalBitangent.z += steepness * direction.x * direction.y * wavelength * amplitude * sin(alpha);
                    
                    // y direction
                    finalNormal.x += direction.x * wavelength * amplitude * cos(alpha);
                    finalNormal.y += steepness * wavelength * amplitude * sin(alpha);
                    finalNormal.z += direction.y * wavelength * amplitude * cos(alpha);
                    
                    // z direction
                    finalTangent.x += steepness * direction.x * direction.y * wavelength * amplitude * sin(alpha);
                    finalTangent.y += direction.y * wavelength * amplitude * cos(alpha);
                    finalTangent.z += steepness * direction.y * direction.y * wavelength * amplitude * sin(alpha);
                }
                
                finalTangent.x = -finalTangent.x;
                finalTangent.z = 1.0 - finalTangent.z;
                finalTangent = normalize(finalTangent);
                
                finalBitangent.x = 1.0 - finalBitangent.x;
                finalBitangent.z = -finalBitangent.z;
                finalBitangent = normalize(finalBitangent);
                
                finalNormal.x = -finalNormal.x;
                finalNormal.y = 1.0 - finalNormal.y;
                finalNormal.z = -finalNormal.z;
                finalNormal = normalize(finalNormal);
                
                v_bitangent = finalBitangent;
                v_normal = finalNormal;
                v_tangent = finalTangent;
                
                v_texCoord = vec2(clamp((finalVertex.x + u_waterPlaneLength * 0.5 - 0.5) / u_waterPlaneLength, 0.0, 1.0), clamp((-finalVertex.z + u_waterPlaneLength * 0.5 + 0.5) / u_waterPlaneLength, 0.0, 1.0));
                
                vec4 vertex = u_viewMatrix * finalVertex;
                
                // W przestrzeni swiata
                v_incident = u_inverseViewNormalMatrix * vec3(vertex);
                
                gl_Position = u_projectionMatrix * vertex;
            }
            ]]> 
        </Raw>
    </VertexShader>
    <FragmentShader>
        <Raw>
            #version 130
            
            const float Eta = 0.15; // Water
            
            uniform samplerCube u_cubemap;
            uniform sampler2D u_waterTexture;
            
            in vec3 v_incident;
            in vec3 v_bitangent;
            in vec3 v_normal;
            in vec3 v_tangent;
            in vec2 v_texCoord;
            
            out vec4 fragColor;
            
            vec3 textureToNormal(vec4 orgNormalColor)
            {
                return normalize(vec3(clamp(orgNormalColor.r * 2.0 - 1.0, -1.0, 1.0), clamp(orgNormalColor.g * 2.0 - 1.0, -1.0, 1.0), clamp(orgNormalColor.b * 2.0 - 1.0, -1.0, 1.0)));
            }
            
            void main(void)
            {
                // Normalne przechowywane w teksturze są w przestrzeni obiektu. Transformacja na przestrzeń świata nie jest wykonywana
                vec3 objectNormal = textureToNormal(texture(u_waterTexture, v_texCoord));
            
                mat3 objectToWorldMatrix = mat3(normalize(v_bitangent), normalize(v_normal), normalize(v_tangent));
            
                vec3 worldNormal = objectToWorldMatrix * objectNormal;
                vec3 worldIncident = normalize(v_incident);
            
                vec3 refraction = refract(worldIncident, worldNormal, Eta);
                vec3 reflection = reflect(worldIncident, worldNormal);
            
                vec4 refractionColor = texture(u_cubemap, refraction);
                vec4 reflectionColor = texture(u_cubemap, reflection);
            
                float fresnel = Eta + (1.0 - Eta) * pow(max(0.0, 1.0 - dot(-worldIncident, worldNormal)), 5.0);
            
                //fragColor = mix(refractionColor, reflectionColor, fresnel);
                fragColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
            }
        </Raw>
    </FragmentShader>
</Shader>

<Shader name="waterTextureShader" autoinit="true" autobind="true">
    <VertexShader>
        <Attrib name="a_vertex" type="vec3" index="0" />
        <Attrib name="a_texCoord" type="vec2" index="1" />
        <Raw>
            #version 130
            
            uniform mat4 u_projectionMatrix;
            uniform mat4 u_modelviewMatrix;
            
            in vec4 a_vertex;
            in vec2 a_texCoord;
            
            out vec2 v_texCoord;
            
            void main(void)
            {
                v_texCoord = a_texCoord;
                gl_Position = u_projectionMatrix * u_modelviewMatrix * a_vertex;
            }
        </Raw>
    </VertexShader>
    <FragmentShader>
        <Raw>
            <![CDATA[
            #version 130
            
            #define NUMBERWAVES 4
            
            const float Eta = 0.15; // Water
            const float PI = 3.141592654;
            const float G = 9.81;
            
            uniform float u_waterPlaneLength;
            uniform float u_passedTime;
            
            uniform vec4 u_waveParameters[NUMBERWAVES];
            uniform vec2 u_waveDirections[NUMBERWAVES];
            
            in vec2 v_texCoord;
            out vec4 fragColor;
            
            vec3 normalToTexture(vec3 orgNormal)
            {
                return vec3(clamp(orgNormal.x * 0.5 + 0.5, 0.0, 1.0), clamp(orgNormal.y * 0.5 + 0.5, 0.0, 1.0), clamp(orgNormal.z * 0.5 + 0.5, 0.0, 1.0));
            }
            
            void main(void)
            {
                vec3 vertex = vec3(v_texCoord.s * u_waterPlaneLength - u_waterPlaneLength / 2.0 + 0.5, 0.0, -v_texCoord.t * u_waterPlaneLength + u_waterPlaneLength / 2.0 + 0.5);
                vec4 finalVertex = vec4(vertex, 1.0);
                vec3 finalNormal = vec3(0.0, 0.0, 0.0);
            
                for (int i = 0; i < NUMBERWAVES; ++i)
                {
                    vec2 direction = normalize(u_waveDirections[i]);
                    float speed = u_waveParameters[i].x;
                    float amplitude = u_waveParameters[i].y;
                    float wavelength = u_waveParameters[i].z;
                    float steepness = u_waveParameters[i].w;
                    
                    float frequency = sqrt(G * 2.0 * PI / wavelength);
                    float phase = speed * frequency;
                    float alpha = frequency * dot(direction, vertex.xz) + phase * u_passedTime;
                    
                    finalVertex.x += steepness * amplitude * direction.x * cos(alpha);
                    finalVertex.y += amplitude * sin(alpha);
                    finalVertex.z += steepness * amplitude * direction.y * cos(alpha);
                }
                
                for (int i = 0; i < NUMBERWAVES; ++i)
                {
                    vec2 direction = normalize(u_waveDirections[i]);
                    float speed = u_waveParameters[i].x;
                    float amplitude = u_waveParameters[i].y;
                    float wavelength = u_waveParameters[i].z;
                    float steepness = u_waveParameters[i].w;
                    
                    float frequency = sqrt(G * 2.0 * PI / wavelength);
                    float phase = speed * frequency;
                    float alpha = frequency * dot(direction, finalVertex.xz) + phase * u_passedTime;
                    
                    finalNormal.x += direction.x * wavelength * amplitude * cos(alpha);
                    finalNormal.y += steepness * wavelength * amplitude * sin(alpha);
                    finalNormal.z += direction.y * wavelength * amplitude * cos(alpha);
                }
                
                finalNormal.x = -finalNormal.x;
                finalNormal.y = 1.0 - finalNormal.y;
                finalNormal.z = -finalNormal.z;
                finalNormal = normalize(finalNormal);
                
                fragColor = vec4(normalToTexture(finalNormal), 1.0);
            }
            ]]>
        </Raw>
    </FragmentShader>
</Shader>
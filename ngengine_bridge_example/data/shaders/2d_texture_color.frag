#version 130

precision highp float;

uniform sampler2D texture0;

in vec4 color;
in vec2 texCoord0;
out vec4 outColor;

void main(void)
{
    if (texCoord0 == vec2(0,0))
        outColor = color;
    else
        outColor = color * texture(texture0, texCoord0.st);
}

<Shader name="directionalLight" autoinit="true" autobind="true">
    <VertexShader>
        <Uniform name="modelview_matrix" type="mat4"/>
        <Uniform name="projection_matrix" type="mat4"/>
        <Uniform name="normal_matrix" type="mat3"/>
        <Uniform name="material_ambient" type="vec4"/>
        <Uniform name="material_diffuse" type="vec4"/>
        <Uniform name="material_specular" type="vec4"/>
        <Uniform name="material_emissive" type="vec4"/>
        <Uniform name="material_shininess" type="vec4"/>
        <Uniform name="light0" type="light"/>

        <Attrib name="a_Vertex" type="vec3" index="0"/>
        <Attrib name="a_TexCoord0" type="vec2" index="1"/>
        <Attrib name="a_Normal" type="vec3" index="2"/>
        <Raw>
            #version 130

            uniform mat4 projection_matrix;
            uniform mat4 modelview_matrix;
            uniform mat3 normal_matrix;

            uniform vec4 material_ambient;
            uniform vec4 material_diffuse;
            uniform vec4 material_specular;
            uniform vec4 material_emissive;
            uniform float material_shininess;

            struct light {
                vec4 position;
                vec4 diffuse;
                vec4 specular;
                vec4 ambient;
            };

            uniform light light0;

            in vec3 a_Vertex;
            in vec2 a_TexCoord0;
            in vec3 a_Normal;

            out vec4 color;
            out vec2 texCoord0;

            void main(void)
            {
            //Transform the normal into eye-space using the normal matrix
            vec3 N = normalize(normal_matrix * a_Normal);

            //Calculate the light direction, in a directional light the position is actually a direction vector
            //We transform the position into eyespace before normalizing the vector
            vec3 L = normalize(modelview_matrix * light0.position).xyz;

            //We calculate the angle between the normal and the light direction
            float NdotL = max(dot(N, L), 0.0);

            //The ambient color is fixed, so we add this as the initial color and
            //then build on this with the other lighting contributions
            vec4 finalColor = material_ambient * light0.ambient;

            //Do the standard vertex transformation into eye space
            vec4 pos = modelview_matrix * vec4(a_Vertex, 1.0);

            //Because we are in eye space (everything is relative to the camera)
            //The eye vector is simply the negated position.
            vec3 E = -pos.xyz;

            //If the surface normal is facing towards the light at all
            if (NdotL > 0.0)
            {
            //Add the diffuse color using Lambertian Reflection
            finalColor += material_diffuse * light0.diffuse * NdotL;

            //Calculate the half vector and make it unit length
            vec3 HV = normalize(L + E);

            //Find the angle between the normal and the half vector
            float NdotHV = max(dot(N, HV), 0.0);

            //Calculate the specular using Blinn-Phong
            finalColor += material_specular * light0.specular * pow(NdotHV, material_shininess);
            }

            //Output the final color, texture coordinate and position
            color = finalColor;
            texCoord0 = a_TexCoord0;
            gl_Position = projection_matrix * pos;
            }
        </Raw>
    </VertexShader>
    <GeometryShader/>
    <FragmentShader>
        <Uniform name="texture0" type="sampler2D"/>
        <Raw>
            #version 130

            precision highp float;

            uniform sampler2D texture0;

            in vec4 color;
            in vec2 texCoord0;

            out vec4 outColor;

            void main(void) {
            outColor = color * texture(texture0, texCoord0.st);
            //outColor = texture(texture0, texCoord0.st);
            }
        </Raw>
    </FragmentShader>
</Shader>
<Shader name="spotLight" autoinit="true" autobind="true">
<VertexShader>
    <Attrib name="a_Vertex" type="vec3" index="0"/>
    <Attrib name="a_TexCoord0" type="vec2" index="1"/>
    <Attrib name="a_Normal" type="vec3" index="2"/>
    <Raw>
        #version 130

        struct material
        {
        vec4 ambient;
        vec4 diffuse;
        vec4 specular;
        vec4 emissive;
        float shininess;
        };

        struct light
        {
        vec4 position;
        vec4 diffuse;
        vec4 specular;
        vec4 ambient;

        float constant_attenuation;
        float linear_attenuation;
        float quadric_attenuation;

        float spotCutOff;
        vec3 spotDirection;
        float spotExponent;
        };

        uniform mat4 projection_matrix;
        uniform mat4 modelview_matrix;
        uniform mat3 normal_matrix;

        uniform light light0;
        uniform material material0;

        in vec3 a_Vertex;
        in vec2 a_TexCoord0;
        in vec3 a_Normal;

        out vec4 color;
        out vec2 texCoord0;

        void main(void)
        {
        vec3 N = normalize(normal_matrix * a_Normal);
        vec4 pos = modelview_matrix * vec4(a_Vertex, 1.0);

        // Obliczenie pozycji swiatla
        vec3 lightPos = (modelview_matrix * light0.position).xyz;

        // Pobranie wektora kierunku swiatla przez znalezienie wektora pomiedzy wierzcholkiem i polozeniem swiatla
        vec3 lightDir = (lightPos - pos.xyz).xyz;

        // Znalezienie kata pomiedzy normalna i kierunkiem swiatla
        float NdotL = max(dot(N, lightDir.xyz), 0.0);

        // Odleglosc miedzy wierzcholkiem i swiatlem
        float dist = length(lightDir);

        // Znalezienie wektora obserwatora
        vec3 E = -(pos.xyz);

        // Przypisanie koloru 'ambient'
        vec4 finalColor = material0.ambient * light0.ambient;

        float attenuation = 1.0;

        if (NdotL > 0.0)
        {
        float spotEffect = dot(normalize(light0.spotDirection), normalize(-lightDir));

        if (spotEffect > cos(light0.spotCutOff))
        {
        vec3 HV = normalize(lightPos + E);
        float NdotHV = max(dot(N, HV), 0.0);
        finalColor += material0.specular * light0.specular * pow(NdotHV, material0.shininess);

        spotEffect = pow(spotEffect, light0.spotExponent);
        attenuation = spotEffect / (light0.constant_attenuation + light0.linear_attenuation * dist +
        light0.quadric_attenuation * dist * dist);
        finalColor += material0.diffuse * light0.diffuse * NdotL;
        }
        }

        color = material0.emissive + (finalColor * attenuation);

        texCoord0 = a_TexCoord0;
        gl_Position = projection_matrix * pos;
        }
    </Raw>
</VertexShader>
<GeometryShader/>
<FragmentShader>
    <Uniform name="texture0" type="sampler2D"/>
    <Raw>
        #version 130

        precision highp float;

        uniform sampler2D texture0;

        in vec4 color;
        in vec2 texCoord0;

        out vec4 outColor;

        void main(void) {
        outColor = color * texture(texture0, texCoord0.st);
        }
    </Raw>
</FragmentShader>
</Shader>
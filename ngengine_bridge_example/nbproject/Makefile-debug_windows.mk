#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=debug_windows
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/Bridge.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lglfw -lopengl32 -lglu32 -lglew32 -lfreetype -lz -ljpeg -lpng D\:/Documents/Projects/ngengine/projects/netbeans/dist/debug_dynamic_windows/GNU-Linux-x86/libnge.dll

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ngengine_bridge_example

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ngengine_bridge_example: /D/Documents/Projects/ngengine/projects/netbeans/dist/debug_dynamic_windows/GNU-Linux-x86/libnge.dll

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ngengine_bridge_example: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ngengine_bridge_example ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/Bridge.o: Bridge.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/include/freetype2 -I../../ngengine/source/include -I/C/Software/Programming/Libraries/boost_1_53_0 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Bridge.o Bridge.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/include/freetype2 -I../../ngengine/source/include -I/C/Software/Programming/Libraries/boost_1_53_0 -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:
	cd D\:/Documents/Projects/ngengine/projects/netbeans && ${MAKE}  -f Makefile CONF=debug_dynamic_windows

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ngengine_bridge_example

# Subprojects
.clean-subprojects:
	cd D\:/Documents/Projects/ngengine/projects/netbeans && ${MAKE}  -f Makefile CONF=debug_dynamic_windows clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc

#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux-x86
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-Linux-x86
CND_ARTIFACT_NAME_Debug=ngengine_bridge_example
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-Linux-x86/ngengine_bridge_example
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debug=ngenginebridgeexample.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux-x86/package/ngenginebridgeexample.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux-x86
CND_ARTIFACT_DIR_Release=dist/Release/GNU-Linux-x86
CND_ARTIFACT_NAME_Release=ngengine_bridge_example
CND_ARTIFACT_PATH_Release=dist/Release/GNU-Linux-x86/ngengine_bridge_example
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux-x86/package
CND_PACKAGE_NAME_Release=ngenginebridgeexample.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux-x86/package/ngenginebridgeexample.tar
# debug_windows configuration
CND_PLATFORM_debug_windows=GNU-Linux-x86
CND_ARTIFACT_DIR_debug_windows=dist/debug_windows/GNU-Linux-x86
CND_ARTIFACT_NAME_debug_windows=ngengine_bridge_example
CND_ARTIFACT_PATH_debug_windows=dist/debug_windows/GNU-Linux-x86/ngengine_bridge_example
CND_PACKAGE_DIR_debug_windows=dist/debug_windows/GNU-Linux-x86/package
CND_PACKAGE_NAME_debug_windows=ngenginebridgeexample.tar
CND_PACKAGE_PATH_debug_windows=dist/debug_windows/GNU-Linux-x86/package/ngenginebridgeexample.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk

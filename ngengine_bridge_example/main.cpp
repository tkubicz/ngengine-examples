#include <GL/glew.h>
#include <GL/glfw.h>

#include <Tools/Tools.h>
#include <Windows/Windows.h>
#include <Rendering/Rendering.h>

#include "Bridge.h"

using namespace NGE::Tools;
using namespace NGE::Examples;
using namespace NGE::Windows;
using namespace NGE::Media;
using namespace NGE::Rendering;

#ifdef _WIN32
#include <windows.h>
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
#else 
int main()
#endif
{
    Logger::Initialize("data/log.txt", true);
    
    if (glfwInit() != GL_TRUE)
    {
        Logger::WriteFatalErrorLog("Error starting GLFW");
        return 1;
    }
    
    BridgeExample app;
    
    GLFWWindow programWindow;
    programWindow.SetApplication(&app);
    
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file("data/configs/data.xml");
    pugi::xml_node window = doc.child("Window");
    
    if (!programWindow.LoadXMLSettings(window))
    {
        Logger::WriteFatalErrorLog("Unable to load window settings");
        return 1;
    }
    
    MediaManager::GetInstance().GetMediaPathManager().LoadXMLSettings(doc.child("Config"));
    
    if (!programWindow.Create())
    {
        Logger::WriteFatalErrorLog("Unable to create OpenGL window");
        programWindow.Destroy();
        return 1;
    }
    
    if (!programWindow.Init())
    {
        Logger::WriteFatalErrorLog("Could not initialize GLEW");
        programWindow.Destroy();
        return 1;
    }
    
    if (!app.Init())
    {
        Logger::WriteFatalErrorLog("Could not initialise application");
        programWindow.Destroy();
        return 1;
    }
    
    Timing::Initialize();
    Renderer::GetInstance().GetMatrixStack().Initialize();
    programWindow.SetInputCallbacks();
    
    while (programWindow.IsRunning())
    {
        Timing::Update();
        float elapsedTime = static_cast<float>(Timing::Get().lastFrameDuration);
        
        app.Prepare(elapsedTime);
        app.Render();
        
        programWindow.SwapBuffers();
        programWindow.ProcessEvents();
    }
    
    Logger::Flush();
    app.Shutdown();
    
    MediaManager::GetInstance().Deinitialize();
    Renderer::GetInstance().GetMatrixStack().Deinitialize();
    
    programWindow.Destroy();
    
    return 0;
}
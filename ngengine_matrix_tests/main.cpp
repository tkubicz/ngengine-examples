#include <iostream>
#include <Math/Math.h>
#include <Rendering/Rendering.h>
#include <vector>

using namespace std;
using namespace NGE::Math;
using namespace NGE::Rendering;

int main()
{
    mat4f mat(1, 3, 2, 5, 2, 2, 1, 5, 3, 7, 3, 5, 5, 5, 5, 5);
    
    //std::vector<float> normalMatrix = Renderer::GetInstance().CalculateNormalMatrix(mat);
    //mat3f mat2(&normalMatrix[0]);
    
    mat4f mat3 = mat;
    mat3.SetInverseRigidBody();
    
    std::vector<unsigned int> test;
    test.reserve(25);
    std::cout << test.size() << std::endl;
    
    //cout << mat2 << endl;
    cout << mat.GetNormalMatrix() << endl;
    cout << mat3.ExtractMatrix3() << endl;
    cout << mat.Transpose() << endl;
    
    return 0;
}
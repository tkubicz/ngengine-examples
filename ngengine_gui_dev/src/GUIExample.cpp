#include <boost/thread.hpp>

#include "GUIExample.h"
#include "Media/MediaManager.h"
#include "Media/Shaders/GLSLProgram.h"
#include "Geometry/Models/Obj/ObjFileImporter.h"
#include "Core/NGEException.hpp"

GUIExample::GUIExample() { }

GUIExample::~GUIExample() { }

bool GUIExample::Init()
{
    window->CreateSlave(false);
    boost::thread t1(&GUIExample::threadFunc, this);

    Media::MediaManager::GetInstance().GetShaderManager().LoadProgram("floorShader", "floorShader.xml");
    Media::MediaManager::GetInstance().GetShaderManager().LoadProgram("testRectangleShader", "testRectangle.xml");
    Media::MediaManager::GetInstance().GetShaderManager().LoadProgram("skyDomeShader", "skydome.xml");
    Media::MediaManager::GetInstance().GetShaderManager().LoadProgram("diamondSquareShader", "diamondSquare.xml");

    floor.Initialize(100, 4, Media::MediaManager::GetInstance().GetShaderManager().GetProgram("floorShader"));
    camera.Set(0.0f, 150.0f, 10.0f, 2.0f, 8.0f, 2.0f, 0.0f, 1.0f, 0.0f);

    rect.SetPosition(20, 20);
    rect.SetDimensions(200, 200);
    rect.SetPercentageSize(false);
    //    rect.SetDimensions(200, 90);
    //    rect.SetPercentageSize(false);
    rect.SetShader(Media::MediaManager::GetInstance().GetShaderManager().GetProgram("testRectangleShader"));

    rect.SetDraggable(true);
    rect.SetResizeable(false);

    //rect.GetDragAreas().push_back(Math::vec4f(rect.Get))

    rect.SetAnchorPoint(NGE::GUI::GUITypes::AnchorPoint::BOTTOM_RIGHT);
    rect.SetOnActivateFunc(&GUIExample::testActivate, this);
    rect.SetOnDeactivateFunc(&GUIExample::testDeactivate, this);
    rect.SetOnDragFunc(&GUIExample::testOnDrag, this);
    rect.SetOnResizeFunc(&GUIExample::beeingResize, this);
    rect.SetOnBeginResizeFunc(&GUIExample::beginResize, this);
    rect.SetOnEndResizeFunc(&GUIExample::endResize, this);

    rect.UpdateRectangle(true);
    rect.Initialize();
    rect.color = vec4f(1, 0, 0, 1);

    //    rect2.SetPosition(100, 50);
    //    rect2.SetDimensions(100, 100);
    //    rect2.SetShader(Media::MediaManager::GetInstance().GetShaderManager().GetProgram("testRectangleShader"));
    //    rect2.SetParent(&rect);
    //    rect2.SetAnchorPoint(GUITypes::AnchorPoint::TOP_RIGHT);
    //    rect2.UpdateRectangle(true);
    //    rect2.Initialize();
    //    rect2.color = vec4f(0, 1, 0, 1);

    /*guiWindow.SetPercentageSize(true);
    guiWindow.SetPosition(50, 50);
    guiWindow.SetDimensions(15, 15);
    guiWindow.SetAnchorPoint(GUITypes::AnchorPoint::TOP_LEFT);
    guiWindow.SetShader(Media::MediaManager::GetInstance().GetShaderManager().GetProgram("testRectangleShader"));
    guiWindow.UpdateRectangle(true);
    guiWindow.Initialize();*/

    pugi::xml_document terrainInfo;
    pugi::xml_parse_result terrainResult = terrainInfo.load_file("config/terrain.xml");
    pugi::xml_node terrrainNode = terrainInfo.child("Terrain");

    if (!terrain.LoadXMLSettings(terrrainNode))
    {
        Tools::Logger::WriteErrorLog("Could not load terrain information from xml file");
        return false;
    }

    float latitude = 51.12f;
    float longitude = 17.31f;
    float time = 18.0f;
    float day = 275.0f;
    float turbidity = 2.0f;

    skyDome.Initialize(1600, 96, 64, false, 0.8);
    skyDome.SetShader(Media::MediaManager::GetInstance().GetShaderManager().GetProgram("skyDomeShader"));
    skyDome.SetCamera(&camera);
    skyDome.SetCoordinates(latitude, longitude, time, day, turbidity);
    skyDome.Update(0);

    try
    {
        nge_throw_exception("This is sample exception");
    }
    catch (std::exception& ex)
    {
        log_info(ex.what());
    }

    roughness = 0.01f;
    diamond = new NGE::Geometry::Nature::DiamonSquareTerrain(10);
    diamond->shader = Media::MediaManager::GetInstance().GetShaderManager().GetProgram("diamondSquareShader");
    diamond->Generate(roughness);
    diamond->GenerateVertices();
    diamond->GenerateIndices();
    diamond->GenerateVBA();

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    return true;
}

void GUIExample::Shutdown() { }

void GUIExample::Prepare(float dt)
{
    camera.Update(dt);
    floor.Update(dt);
    skyDome.Update(dt);
}

void GUIExample::Render()
{
    Rendering::Renderer::GetInstance().ClearColor(0.9f, 0.95f, 1.0f, 1.0f);
    Rendering::Renderer::GetInstance().ClearBuffers();

    Rendering::Renderer::GetInstance().GetMatrixStack().Identity();

    camera.Look();
    floor.Render();
    //terrain.Render();
    diamond->Render();
    //skyDome.Render();

    /*Rendering::Renderer::GetInstance().Enter2DMode();
    {
        rect.Render();
        //rect2.Render();
        //guiWindow.Render();
    }
    Rendering::Renderer::GetInstance().Exit2DMode();*/
}

void GUIExample::OnKeyPressed(NGE::Events::KeyboardEvent& event)
{
    camera.SetKeyboardInput(event);

    if (event.GetAction() == Events::PRESSED)
    {
        if (event.GetKeyId() == Events::KEY_Z)
        {
            if (camera.IsMouseLocked())
            {
                window->EnableMouseCursor(true);
            }
            else
            {
                window->EnableMouseCursor(false);
            }

            camera.LockMouse(!camera.IsMouseLocked());
        }

        if (event.GetKeyId() == Events::KEY_X)
        {
            terrain.SetWireframe(!terrain.GetWireframe());
        }

        if (event.GetKeyId() == Events::KEY_R)
        {
            GenerateNewTerrain();
        }
        
        if (event.GetKeyId() == Events::KEY_UP)
        {
            if (roughness >= 1.0f)
                roughness = 1.0f;
            else
                roughness += 0.1f;
        }
        else if (event.GetKeyId() == Events::KEY_DOWN)
        {
            if (roughness <= 0.0f)
                roughness = 0.0f;
            else
                roughness -= 0.1f;
        }
        
        if (event.GetKeyId() == Events::KEY_P) {
            log_info(to_string(roughness));
        }

    }
}

void GUIExample::OnMouse(NGE::Events::MouseEvent& event)
{
    camera.SetMouseInfo(event.GetX(), event.GetY());
    rect.CheckMouseEvent(event);
    //    rect2.CheckMouseEvent(event);
    guiWindow.CheckMouseEvent(event);
}

void GUIExample::OnMouseDrag(int x, int y) { }

void GUIExample::OnResize(int width, int height)
{
    Application::OnResize(width, height);

    rect.UpdateRectangle(true);
    rect.Initialize();

    //    rect2.UpdateRectangle(true);
    //    rect2.Initialize();

    //guiWindow.UpdateRectangle(true);
    //guiWindow.Initialize();
}

void GUIExample::testActivate(NGE::Events::MouseEvent* ev)
{
    //std::cout << "activate" << std::endl;
    rect.OnActivate(ev);
}

void GUIExample::testDeactivate(NGE::Events::MouseEvent* event)
{
    //std::cout << "deactivate" << std::endl;
    rect.OnDeactivate(event);
}

void GUIExample::testOnDrag(NGE::Events::MouseEvent* event)
{
    rect.OnDrag(event);
}

void GUIExample::beeingResize(NGE::Events::MouseEvent* event) { }

void GUIExample::beginResize(NGE::Events::MouseEvent* event)
{
    rect.OnBeginResize(event);
    log_info("Start resizing...");
}

void GUIExample::endResize(NGE::Events::MouseEvent* event)
{
    rect.OnEndResize(event);
    log_info("End resizing...");
}

void GUIExample::threadFunc()
{
    printf("Thread start...\n");
    boost::posix_time::milliseconds sleepTime(5000);
    boost::this_thread::sleep(sleepTime);

    pugi::xml_document textureInfo;
    pugi::xml_parse_result textureResult = textureInfo.load_file("../data/configs/textures.xml");

    if (textureResult.status != pugi::xml_parse_status::status_ok)
    {
        log_error("Could not load texture file.");
    }
    else
    {
        for (pugi::xml_node current = textureInfo.child("Texture2D"); current != NULL; current = current.next_sibling())
        {
            window->SetSlaveWindowContext();
            Media::MediaManager::GetInstance().GetTextureManager().LoadTexture(current);
            window->SetMainWindowContext();
        }
    }

    printf("Thread stop...\n");
}

void GUIExample::GenerateNewTerrain()
{
    diamond->DestroyGeometry();

    diamond->Generate(roughness);
    diamond->GenerateVertices();
    diamond->GenerateIndices();
    diamond->GenerateVBA();
}


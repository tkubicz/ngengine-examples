#include "GUIWindow.h"
#include <Rendering/Renderer.h>

using namespace NGE::GUI;

GUIWindow::GUIWindow() : GUIRectangle()
{
    //::GUIRectangle();
    
    titlebarHeight = 10;
}

GUIWindow::GUIWindow(const vec2i& position, const vec2i& dimensions, const GUITypes::AnchorPoint anchorPoint)
{
    GUIWindow();

    SetPosition(position);
    SetDimensions(dimensions);
    SetAnchorPoint(anchorPoint);
}

bool GUIWindow::Initialize()
{
    CreateRectangleGeometry();
    CreateRectangleVBO();
    CreateRectangleVBA();
}

void GUIWindow::Render()
{
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);

    shader->BindShader();
    {
        shader->SendUniform4x4("modelviewMatrix", NGE::Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(NGE::MODELVIEW_MATRIX));
        shader->SendUniform4x4("projectionMatrix", NGE::Rendering::Renderer::GetInstance().GetMatrixStack().GetMatrix(NGE::PROJECTION_MATRIX));
        shader->SendUniform("color", color);

        glBindVertexArray(vertexArray);
        {
            glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, 0);
        }
        glBindVertexArray(0);
    }
    shader->UnbindShader();

    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
}

bool GUIWindow::CreateRectangleGeometry()
{
    vertices.reserve(6);
    // Rectangle geometry.
    vertices.push_back(NGE::Math::vec3f(realDimensions.x, realDimensions.y, 0.0f));
    vertices.push_back(NGE::Math::vec3f(realDimensions.z, realDimensions.y, 0.0f));
    vertices.push_back(NGE::Math::vec3f(realDimensions.z, realDimensions.w, 0.0f));
    vertices.push_back(NGE::Math::vec3f(realDimensions.x, realDimensions.w, 0.0f));

    // Titlebar geometry.
    vertices.push_back(NGE::Math::vec3f(realDimensions.z, realDimensions.w + titlebarHeight, 0.0f));
    vertices.push_back(NGE::Math::vec3f(realDimensions.x, realDimensions.w + titlebarHeight, 0.0f));

    colors.reserve(6);
    colors.push_back(vec4f(1, 0, 0, 1));
    colors.push_back(vec4f(0, 1, 0, 1));
    colors.push_back(vec4f(0, 0, 1, 1));
    colors.push_back(vec4f(1, 1, 0, 1));
    colors.push_back(vec4f(1, 1, 1, 1));
    colors.push_back(vec4f(1, 1, 1, 1));

    indices.reserve(12);
    // Rectangle indices.
    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(2);
    indices.push_back(3);
    indices.push_back(0);
    indices.push_back(2);

    // Titlebar indices.
    indices.push_back(3);
    indices.push_back(2);
    indices.push_back(4);
    indices.push_back(5);
    indices.push_back(3);
    indices.push_back(4);

    return true;
}

bool GUIWindow::CreateRectangleVBO()
{
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof (float) * 3 * vertices.size(), &vertices[0], GL_STATIC_DRAW);

    glGenBuffers(1, &colorBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof (float) * 4 * colors.size(), &colors[0], GL_STATIC_DRAW);

    glGenBuffers(1, &indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof (GLuint) * indices.size(), &indices[0], GL_STATIC_DRAW);

    vertices.clear();
    colors.clear();
    indices.clear();
}

bool GUIWindow::CreateRectangleVBA()
{
    glGenVertexArrays(1, &vertexArray);
    glBindVertexArray(vertexArray);

    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

    glBindVertexArray(0);
}

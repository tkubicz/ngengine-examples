/* 
 * File:   GUIExample.h
 * Author: tku
 *
 * Created on 3 lipiec 2014, 00:24
 */

#ifndef GUIEXAMPLE_H
#define	GUIEXAMPLE_H

#include <Windows/Application.h>
#include <Rendering/Camera/Camera.h>
#include <Geometry/Basic/Floor.h>
#include <Events/GUIEventListener.h>
#include <Geometry/Nature/Terrain.h>
#include <Geometry/Nature/SkyDome.h>
#include <Geometry/Nature/DiamonSquareTerrain.hpp>
#include "GUIRectangle.h"
#include "GUIWindow.h"

using namespace NGE;

class GUIExample : public NGE::Windows::Application
{
  public:
    GUIExample();
    virtual ~GUIExample();

    bool Init();
    void Prepare(float dt);
    void Render();
    void Shutdown();

    virtual void OnResize(int width, int height);

    void OnKeyPressed(NGE::Events::KeyboardEvent& event);
    void OnMouse(NGE::Events::MouseEvent& event);
    void OnMouseDrag(int x, int y);

    void testActivate(NGE::Events::MouseEvent* event);
    void testDeactivate(NGE::Events::MouseEvent* event);

  protected:
    NGE::Rendering::Camera::Camera camera;
    NGE::Geometry::Basic::Floor floor;
    
    NGE::Geometry::Nature::SkyDome skyDome;
    NGE::Geometry::Nature::Terrain terrain;
    
    float roughness;
    NGE::Geometry::Nature::DiamonSquareTerrain* diamond;

    NGE::GUI::GUIRectangle rect;
    NGE::GUI::GUIRectangle rect2;
    NGE::GUI::GUIWindow guiWindow;

    void testOnDrag(NGE::Events::MouseEvent* event);

    void beginResize(NGE::Events::MouseEvent* event);
    void endResize(NGE::Events::MouseEvent* event);
    void beeingResize(NGE::Events::MouseEvent* event);

    void threadFunc();
    
    void GenerateNewTerrain();
};

#endif	/* GUIEXAMPLE_H */


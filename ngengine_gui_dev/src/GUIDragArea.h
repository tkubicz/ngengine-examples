/* 
 * File:   GUIDragArea.h
 * Author: tku
 *
 * Created on 3 październik 2014, 15:16
 */

#ifndef GUIDRAGAREA_H
#define	GUIDRAGAREA_H

namespace NGE
{
    namespace GUI
    {
        class GUIDragArea
        {
          public:
            GUIDragArea(const NGE::Math::vec2i& position, const NGE::Math::vec2i& size);
            
            NGE::Math::vec2i GetAbsolutePosition() const;
            NGE::Math::vec2i GetAbsoluteSize() const;
            
            NGE::Math::vec2i GetRelativePosition() const;
            NGE::Math::vec2i GetRelativeSize() const;
            
          protected:
            NGE::Math::vec2i absolutePosition;
            NGE::Math::vec2i absoluteSize;
            
            NGE::Math::vec2i relativePosition;
            NGE::Math::vec2i relativeSize;
        };
    }
}

#endif	/* GUIDRAGAREA_H */


/* 
 * File:   GUITypes.h
 * Author: tku
 *
 * Created on 3 październik 2014, 14:22
 */

#ifndef GUITYPES_H
#define	GUITYPES_H

namespace NGE
{
    namespace GUI
    {

        /**
         * @class GUITypes class.
         * @brief Class that keeps types used in GUI system in one place.
         */
        class GUITypes
        {
          public:

            /**
             * @enum Type used to represents anchor point of GUI widget.
             */
            enum AnchorPoint
            {
                BOTTOM_LEFT = 0,
                BOTTOM_RIGHT,
                TOP_LEFT,
                TOP_RIGHT
            };
        };
    }
}

#endif	/* GUITYPES_H */


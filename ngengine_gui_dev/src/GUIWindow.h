/* 
 * File:   GUIWindow.h
 * Author: tku
 *
 * Created on 15 lipiec 2014, 15:12
 */

#ifndef GUIWINDOW_H
#define	GUIWINDOW_H

#include "GUIRectangle.h"

namespace NGE
{
    namespace GUI
    {

        class GUIWindow : public GUIRectangle
        {
          public:

            GUIWindow();
            GUIWindow(const vec2i& position, const vec2i& dimensions, const GUITypes::AnchorPoint anchorPoint);

            bool Initialize();

            void Render();

          protected:

            bool CreateRectangleGeometry();
            bool CreateRectangleVBO();
            bool CreateRectangleVBA();

          protected:
            /**
             * Holds the height of the titlebar.
             */
            int titlebarHeight;

            /**
             * Holds the background color.
             */
            vec4i backgroundColor;

            /**
             * Holds the border color.
             */
            vec4i borderColor;
        };
    }
}
#endif	/* GUIWINDOW_H */


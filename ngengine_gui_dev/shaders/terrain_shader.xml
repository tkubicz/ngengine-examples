<Shader name="terrain_shader" autoinit="true" autobind="true">
    <VertexShader>
        <Attrib name="a_vertex" type="vec3" index="0" />
        <Attrib name="a_texcoord" type="vec2" index="1" />
        <Attrib name="a_normal" type="vec3" index="2" />
        <Attrib name="a_color" type="vec3" index="3" />
        <Raw>
            #version 130
            
            struct material
            {
                vec4 ambient;
                vec4 diffuse;
                vec4 specular;
                vec4 emissive;
                float shininess;
            };
            
            struct light
            {
                vec4 position;
                vec4 diffuse;
                vec4 specular;
                vec4 ambient;
            
                float constant_attenuation;
                float linear_attenuation;
                float quadratic_attenuation;
            };
            
            uniform mat4 projection_matrix;
            uniform mat4 modelview_matrix;
            uniform mat3 normal_matrix;
            
            uniform light light0;
            uniform material material0;
            
            in vec3 a_vertex;
            in vec2 a_texcoord;
            in vec3 a_normal;
            in vec3 a_color;
            
            out float height;
            out vec2 texcoord;
            
            void main()
            {
                vec3 N = normalize(normal_matrix * a_normal);
                vec3 L = normalize(modelview_matrix * light0.position).xyz;
            
                float NdotL = max(dot(N, L), 0.0);
                vec4 finalColor = material0.ambient * light0.ambient;
                vec4 pos = modelview_matrix * vec4(a_vertex, 1.0);
                vec3 E = -pos.xyz;
            
                if (NdotL > 0.0)
                {
                    finalColor += material0.diffuse * light0.diffuse * NdotL;
                    vec3 HV = normalize(L + E);
                    float NdotHV = max(dot(N, HV), 0.0);
                    finalColor += material0.specular * light0.specular * pow(NdotHV, material0.shininess);
                }
            
                //color = finalColor;
                height = a_vertex.y;
                texcoord = a_texcoord;
                gl_Position = projection_matrix * pos;
            }
        </Raw>
    </VertexShader>
    <FragmentShader>
        <Raw>
            <![CDATA[
            #version 130
            
            uniform sampler2D texture0;
            
            in float height;
            in vec2 texcoord;
            
            out vec4 outColor;
            
            void main()
            {
                //outColor = color * texture(texture0, texcoord.st);

                vec4 newColor;

                if (height < 20)
                {
                    newColor = vec4(0, 0, 1, 1);
                }
                else if (height < 40)
                {
                    newColor = vec4(0, height / 500, 0, 1);
                }
                else if (height < 60)
                {
                    newColor = vec4(height / 500 - 1, 1, 0, 1);
                }
                else if (height < 80)
                {
                    newColor = vec4(1, 2 - height / 1000, 0, 1);
                }
                else
                {
                    newColor = vec4(1, 1, 1, 1);
                }

                outColor = newColor;
            }
            ]]>
        </Raw>
    </FragmentShader>
</Shader>
<Shader name="point_light_per_vertex" autoinit="true" autobind="true">
    <VertexShader>
        <Attrib name="a_vertex" type="vec3" index="0" />
        <Attrib name="a_texcoord" type="vec2" index="1" />
        <Attrib name="a_normal" type="vec3" index="2" />
        <Raw>
            #version 130
            
            struct material
            {
                vec4 ambient;
                vec4 diffuse;
                vec4 specular;
                vec4 emissive;
                float shininess;
            };
            
            struct light
            {
                vec4 position;
                vec4 diffuse;
                vec4 specular;
                vec4 ambient;
            
                float constant_attenuation;
                float linear_attenuation;
                float quadratic_attenuation;
            };
            
            uniform mat4 projection_matrix;
            uniform mat4 modelview_matrix;
            uniform mat3 normal_matrix;
            
            uniform light light0;
            uniform material material0;
            
            in vec3 a_vertex;
            in vec2 a_texcoord;
            in vec3 a_normal;
            
            out vec4 color;
            out vec2 texcoord;
            
            void main()
            {
                vec3 N = normalize(normal_matrix * a_normal);
                vec4 pos = modelview_matrix * vec4(a_vertex, 1.0);
                
                // Pozycja swiatla
                vec3 lightPos = (modelview_matrix * light0.position).xyz;
                
                vec3 lightDir = lightPos - pos.xyz;
            
                float NdotL = max(dot(N, lightDir.xyz), 0.0);
            
                float dist = length(lightDir);
            
                vec3 E = -(pos.xyz);
            
                vec4 finalColor = material0.ambient * light0.ambient;
                
                if (NdotL > 0.0)
                {
                    vec3 HV = normalize(lightPos + E);
                    finalColor += material0.diffuse * light0.diffuse * NdotL;
            
                    float NdotHV = max(dot(N, HV), 0.0);
                    finalColor += material0.specular * light0.specular * pow(NdotHV, material0.shininess);
                }
            
                float attenuation = 1.0 / (light0.constant_attenuation + light0.linear_attenuation * dist + light0.quadratic_attenuation * dist * dist);
                
                color = material0.emissive + (finalColor * attenuation);
            
                texcoord = a_texcoord;
                gl_Position = projection_matrix * pos;
            }
        </Raw>
    </VertexShader>
    <FragmentShader>
        <Raw>
            #version 130
        
            uniform sampler2D texture0;
            
            in vec4 color;
            in vec2 texcoord;
            
            out vec4 outColor;
        
            void main()
            {
                outColor = color * texture(texture0, texcoord.st);
            }
        </Raw>
    </FragmentShader>
</Shader>
<Shader name="point_light_per_pixel" autoinit="true" autobind="true">
    <VertexShader>
        <Attrib name="a_vertex" type="vec3" index="0" />
        <Attrib name="a_texcoord" type="vec2" index="1" />
        <Attrib name="a_normal" type="vec3" index="2" />
        <Raw>
            <![CDATA[
            #version 130
            
            struct material
            {
                vec4 ambient;
                vec4 diffuse;
                vec4 specular;
                vec4 emissive;
                float shininess;
            };
            
            struct light
            {
                vec4 position;
                vec4 diffuse;
                vec4 specular;
                vec4 ambient;
            
                float constant_attenuation;
                float linear_attenuation;
                float quadratic_attenuation;
            };
            
            uniform mat4 projection_matrix;
            uniform mat4 modelview_matrix;
            uniform mat3 normal_matrix;
            
            uniform light light0;
            uniform material material0;
            
            in vec3 a_vertex;
            in vec2 a_texcoord;
            in vec3 a_normal;
            
            out vec2 texcoord;
            out vec3 normal;
            out vec3 lightDir;
            out vec3 halfVector;
            
            out vec4 vertDiffuse;
            out vec4 vertAmbient;
            out float dist;
            
            void main()
            {
                normal = normalize(normal_matrix * a_normal);
                vec4 pos = modelview_matrix * vec4(a_vertex, 1.0);
            
                vec3 lightPos = (modelview_matrix * light0.position).xyz;
                lightDir = lightPos - pos.xyz;
                vec3 E = -(pos.xyz);
            
                dist = length(lightDir);
                lightDir = normalize(lightDir);
                halfVector = normalize(lightPos + E);
            
                vertDiffuse = material0.diffuse * light0.diffuse;
                vertAmbient = material0.ambient * light0.ambient;
                texcoord = a_texcoord;
            
                gl_Position = projection_matrix * pos;
            }
            ]]>
        </Raw>
    </VertexShader>
    <FragmentShader>
        <Raw>
            #version 130
            
            struct material
            {
                vec4 ambient;
                vec4 diffuse;
                vec4 specular;
                vec4 emissive;
                float shininess;
            };
            
            struct light
            {
                vec4 position;
                vec4 diffuse;
                vec4 specular;
                vec4 ambient;
            
                float constant_attenuation;
                float linear_attenuation;
                float quadratic_attenuation;
            };
            
            uniform light light0;
            uniform material material0;
            uniform sampler2D texture0;
            
            in vec2 texcoord;
            in vec3 normal;
            in vec3 lightDir;
            in vec3 halfVector;
            in vec4 vertDiffuse;
            in vec4 vertAmbient;
            in float dist;
            
            out vec4 outColor;
            
            void main()
            {
                vec3 N = normalize(normal);
                
                float NdotL = max(dot(N, normalize(lightDir)), 0.0);
                vec4 color = vec4(0.0);
                
                if (NdotL > 0.0)
                {
                    color += vertDiffuse * NdotL;
                    vec3 HV = normalize(halfVector);
                    float NdotHV = max(dot(N, HV), 0.0);
                    color += material0.specular * light0.specular * pow(NdotHV, material0.shininess);
                }
            
                float attenuation = 1.0 / (light0.constant_attenuation + light0.linear_attenuation * dist + light0.quadratic_attenuation * dist * dist);
            
                outColor = ((color * attenuation) + vertAmbient) * texture(texture0, texcoord.st);
            }
        </Raw>
    </FragmentShader>
</Shader>
<Shader name="spot_light_per_vertex" autoinit="true" autobind="true">
    <VertexShader>
        <Attrib name="a_vertex" type="vec3" index="0" />
        <Attrib name="a_texcoord" type="vec2" index="1" />
        <Attrib name="a_normal" type="vec3" index="2" />
        <Raw>
            #version 130
            
            struct material
            {
                vec4 ambient;
                vec4 diffuse;
                vec4 specular;
                vec4 emissive;
                float shininess;
            };
            
            struct light
            {
                vec4 position;
                vec4 diffuse;
                vec4 specular;
                vec4 ambient;
            
                float constant_attenuation;
                float linear_attenuation;
                float quadratic_attenuation;
            
                float spot_cut_off;
                vec3 spot_direction;
                float spot_exponent;
            };
            
            uniform mat4 projection_matrix;
            uniform mat4 modelview_matrix;
            uniform mat3 normal_matrix;
            
            uniform light light0;
            uniform material material0;
            
            in vec3 a_vertex;
            in vec2 a_texcoord;
            in vec3 a_normal;
            
            out vec4 color;
            out vec2 texcoord;
            
            void main()
            {
                vec3 N = normalize(normal_matrix * a_normal);
                vec4 pos = modelview_matrix * vec4(a_vertex, 1.0);
                
                vec3 lightPos = (modelview_matrix * light0.position).xyz;
                vec3 lightDir = (lightPos - pos.xyz).xyz;
                
                float NdotL = max(dot(N, lightDir.xyz), 0.0);
                float dist = length(lightDir);
                
                vec3 E = -(pos.xyz);
            
                vec4 finalColor = material0.ambient * light0.ambient;
                float attenuation = 1.0;
            
                if (NdotL > 0.0)
                {
                    float spotEffect = dot(normalize(light0.spot_direction), normalize(-lightDir));
                    
                    if (spotEffect > cos(light0.spot_cut_off))
                    {
                        vec3 HV = normalize(lightPos + E);
                        float NdotHV = max(dot(N, HV), 0.0);
                        finalColor += material0.specular * light0.specular * pow(NdotHV, material0.shininess);
                        
                        spotEffect = pow(spotEffect, light0.spot_exponent);
                        attenuation = spotEffect / (light0.constant_attenuation + light0.linear_attenuation * dist + light0.quadratic_attenuation * dist * dist);
                        finalColor += material0.diffuse * light0.diffuse * NdotL;
                    }
                }
            
                color = material0.emissive + (finalColor * attenuation);
                texcoord = a_texcoord;
                gl_Position = projection_matrix * pos;
            }
        </Raw>
    </VertexShader>
    <FragmentShader>
        <Raw>
            #version 130
            
            uniform sampler2D texture0;
            
            in vec4 color;
            in vec2 texcoord;
            
            out vec4 outColor;
            
            void main()
            {
                outColor = color * texture(texture0, texcoord.st);
            }
        </Raw>
    </FragmentShader>
</Shader>
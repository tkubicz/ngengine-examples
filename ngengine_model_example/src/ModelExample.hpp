/* 
 * File:   ModelExample.hpp
 * Author: tku
 *
 * Created on 12 listopad 2014, 15:56
 */

#ifndef MODELEXAMPLE_HPP
#define	MODELEXAMPLE_HPP

#include <Windows/Application.h>
#include <Rendering/Camera/Camera.h>
#include <Geometry/Basic/Floor.h>
#include <Geometry/Models/Obj/Obj.hpp>

using namespace NGE;

class ModelExample : public NGE::Windows::Application
{
  public:
    ModelExample();
    virtual ~ModelExample();

    bool Init();
    void Prepare(float dt);
    void Render();
    void Shutdown();

    virtual void OnResize(int width, int height);

    void OnKeyPressed(NGE::Events::KeyboardEvent& event);
    void OnMouse(NGE::Events::MouseEvent& event);
    void OnMouseDrag(int x, int y);

  protected:
    NGE::Rendering::Camera::Camera camera;
    NGE::Geometry::Basic::Floor floor;
};

#endif	/* MODELEXAMPLE_HPP */


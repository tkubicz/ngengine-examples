#include "ModelExample.hpp"

ModelExample::ModelExample() { }

ModelExample::~ModelExample() { }

bool ModelExample::Init()
{
    Media::MediaManager::GetInstance().GetShaderManager().LoadProgram("floorShader", "floorShader.xml");
    floor.Initialize(100, 4, Media::MediaManager::GetInstance().GetShaderManager().GetProgram("floorShader"));
    camera.Set(0.0f, 10.0f, 10.0f, 2.0f, 8.0f, 2.0f, 0.0f, 1.0f, 0.0f);

    /* Load OBJ model */
    std::string modelFileName = "../data/models/DefenderLingerie00.obj";
    Appearance::Scene::Scene* scene = new Appearance::Scene::Scene();
    
    Geometry::Models::Obj::ObjFileImporter objFileImporter;

    if (objFileImporter.CanRead(modelFileName, true))
    {
        log_info("Can read");
    }
    else
    {
        log_info("Can't read");
    }
    
    objFileImporter.InternReadFile(modelFileName, scene);
    
   
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    return true;
}

void ModelExample::Shutdown()
{
}

void ModelExample::Prepare(float dt)
{
    camera.Update(dt);
    floor.Update(dt);
}

void ModelExample::Render()
{
    Rendering::Renderer::GetInstance().ClearColor(0.9f, 0.95f, 1.0f, 1.0f);
    Rendering::Renderer::GetInstance().ClearBuffers();

    Rendering::Renderer::GetInstance().GetMatrixStack().Identity();

    camera.Look();
    floor.Render();
}

void ModelExample::OnKeyPressed(NGE::Events::KeyboardEvent& event)
{
    camera.SetKeyboardInput(event);

    if (event.GetAction() == Events::PRESSED)
    {
        if (event.GetKeyId() == Events::KEY_Z)
        {
            if (camera.IsMouseLocked())
            {
                window->EnableMouseCursor(true);
            }
            else
            {
                window->EnableMouseCursor(false);
            }

            camera.LockMouse(!camera.IsMouseLocked());
        }
    }
}

void ModelExample::OnMouse(NGE::Events::MouseEvent& event)
{
    camera.SetMouseInfo(event.GetX(), event.GetY());
}

void ModelExample::OnMouseDrag(int x, int y) { }

void ModelExample::OnResize(int width, int height)
{
    Application::OnResize(width, height);
}







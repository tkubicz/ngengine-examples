#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/ModelExample.o \
	${OBJECTDIR}/src/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lGL -lGLEW -lglfw -Wl,-rpath,../../ngengine/projects/netbeans/dist/debug_dynamic_linux/GNU-Linux-x86 -L../../ngengine/projects/netbeans/dist/debug_dynamic_linux/GNU-Linux-x86 -lnge

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ngengine_model_example

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ngengine_model_example: ../../ngengine/projects/netbeans/dist/debug_dynamic_linux/GNU-Linux-x86/libnge.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ngengine_model_example: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ngengine_model_example ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/src/ModelExample.o: src/ModelExample.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/include/freetype2 -I../../ngengine/source/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ModelExample.o src/ModelExample.cpp

${OBJECTDIR}/src/main.o: src/main.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -I/usr/include/freetype2 -I../../ngengine/source/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/main.o src/main.cpp

# Subprojects
.build-subprojects:
	cd ../../ngengine/projects/netbeans && ${MAKE}  -f Makefile CONF=debug_dynamic_linux

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ngengine_model_example

# Subprojects
.clean-subprojects:
	cd ../../ngengine/projects/netbeans && ${MAKE}  -f Makefile CONF=debug_dynamic_linux clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
